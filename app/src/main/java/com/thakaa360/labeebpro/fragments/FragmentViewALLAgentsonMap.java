package com.thakaa360.labeebpro.fragments;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.SubscriptionEventListener;
import com.thakaa360.labeebpro.R;
import com.thakaa360.labeebpro.Tracker.LatLngInterpolator;
import com.thakaa360.labeebpro.Tracker.MarkerAnimation;
import com.thakaa360.labeebpro.activities.AgentRegistration;
import com.thakaa360.labeebpro.activities.CompanyDashboard;
import com.thakaa360.labeebpro.activities.TrackAllAgents;
import com.thakaa360.labeebpro.modals.PUSHER_MODAL;
import com.thakaa360.labeebpro.server.client.ApiClient;
import com.thakaa360.labeebpro.server.interfaces.Assign_Agent_Interface;
import com.thakaa360.labeebpro.server.interfaces.GetAllAgents;
import com.thakaa360.labeebpro.server.modals.AGENT_MODAL_FOR_ASSIGNING;
import com.thakaa360.labeebpro.server.modals.ALL_AGENTS_MODAL;
import com.thakaa360.labeebpro.server.modals.MSG_MODAL;
import com.thakaa360.labeebpro.utilities.Constants;
import com.thakaa360.labeebpro.utilities.Tools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import io.paperdb.Paper;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentViewALLAgentsonMap extends Fragment implements OnMapReadyCallback,GoogleMap.OnMarkerClickListener {
    View view;
    Context context;
    private GoogleMap mMap;
    private HashMap<Marker,Integer> markerhashMap=new HashMap<>();
    private ArrayList<Marker> markers = new ArrayList<>();
    private ArrayList<Integer> idArrayList = new ArrayList<>();
    Toolbar toolbar;
    SQLiteDatabase sqLiteDatabase;
    int company_id;
    private  HashMap<Marker,Integer> idHAshMap = new HashMap<>();


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_view_agents_on_map, container, false);

        context = getContext();
        Paper.init(context);

        sqLiteDatabase = context.openOrCreateDatabase(Constants.DB_NAME,Context.MODE_PRIVATE,null);
        Cursor c = sqLiteDatabase.rawQuery(Constants.GET_PROFILE,null);
        c.moveToFirst();
        company_id =  c.getInt(c.getColumnIndex(Constants.DB_USER_ID));



        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);



        return view;
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getContext(), R.raw.custom_map));

            if (!success) {
                Log.e("MapsActivityRaw", "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e("MapsActivityRaw", "Can't find style.", e);
        }

        LatLng dubai = new LatLng(25.2048, 55.2708);
        //mMap.addMarker(new MarkerOptions().position(dubai).title("Your Current Location"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(dubai,8));
        // Add a marker in Sydney and move the camera
        //LatLng sydney = new LatLng(-34, 151);
        //Marker marker = mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        GetAllAgents getAllAgents = ApiClient.getApiClient().create(GetAllAgents.class);
        Call<List<ALL_AGENTS_MODAL>> call = getAllAgents.getAgents(company_id);
        call.enqueue(new Callback<List<ALL_AGENTS_MODAL>>() {
            @Override
            public void onResponse(Call<List<ALL_AGENTS_MODAL>> call, Response<List<ALL_AGENTS_MODAL>> response) {
                if (response.isSuccessful()) {
                    //   Toast.makeText(getApplicationContext(),response.body().get(0).getUser_name(),Toast.LENGTH_SHORT).show();
                    for (int i = 0; i < response.body().size(); i++) {
                        Marker marker = mMap.addMarker(new MarkerOptions().
                                title(response.body().get(i).getUser_name()).
                                position(new LatLng(
                                        response.body().get(i).getLat(),
                                        response.body().get(i).getLon()
                                )).
                                icon(BitmapDescriptorFactory.fromBitmap(Tools.getBitmapFromVectorDrawable(context,R.drawable.ic_marker_03))));
                        markers.add(marker);
                        markerhashMap.put(marker, i);

                        idArrayList.add(response.body().get(i).getId());
                        idHAshMap.put(marker,response.body().get(i).getId());
                    }

                }
                else {
                    Toast.makeText(context,response.message(),Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<ALL_AGENTS_MODAL>> call, Throwable t) {
                Toast.makeText(context,t.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });


        PusherOptions options = new PusherOptions();
        options.setCluster("ap2");
        Pusher pusher = new Pusher("e5353a3054d6415263d0", options);
        pusher.connect();
        Channel channel = pusher.subscribe("test-channel");
        try {
            channel.bind("test-event", new SubscriptionEventListener() {
                @Override
                public void onEvent(String channelName, String eventName, final String data) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            Gson gson = new Gson();
                            PUSHER_MODAL msg = gson.fromJson(data,PUSHER_MODAL.class);
                            double lat = msg.getLat();
                            double lng = msg.getLon();
                            int id = msg.getId();
                            for (int i=0;i<markers.size();i++){
                                if(idArrayList.get(i)==id){
                                    LatLngInterpolator latLngInterpolator = new LatLngInterpolator.Linear();
                                    MarkerAnimation.animateMarkerToGB(markers.get(i), new LatLng(lat,lng), latLngInterpolator);
                                }
                            }







                        }
                    });
                }
            });

        }
        catch (NullPointerException ex){
            Toast.makeText(context,ex.getMessage(),Toast.LENGTH_LONG).show();
        }




    }

    @Override
    public boolean onMarkerClick(Marker marker) {


        return false;
    }


    public static Fragment newInstance() {
        return new FragmentViewALLAgentsonMap();
    }
}





