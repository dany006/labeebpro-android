package com.thakaa360.labeebpro.fragments;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.thakaa360.labeebpro.R;
import com.thakaa360.labeebpro.activities.CompanyDashboard;
import com.thakaa360.labeebpro.server.client.ApiClient;
import com.thakaa360.labeebpro.server.interfaces.Assign_Agent_Interface;
import com.thakaa360.labeebpro.server.interfaces.GetAllAgents;
import com.thakaa360.labeebpro.server.modals.AGENT_MODAL_FOR_ASSIGNING;
import com.thakaa360.labeebpro.server.modals.MSG_MODAL;
import com.thakaa360.labeebpro.utilities.Constants;
import com.thakaa360.labeebpro.utilities.Tools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import io.paperdb.Paper;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentViewAgentsonMap extends Fragment implements OnMapReadyCallback,GoogleMap.OnMarkerClickListener {
    View view;
    static Context context;

    private GoogleMap mMap;
    String agent_id;
    private Toolbar toolbar;
    private String
            request_lat,
            request_lon,
            company_id,
            request_subcategory;
    private int status;
    private static int request_id;
    private HashMap<Marker, Integer> mHashMap = new HashMap<Marker, Integer>();
    private ArrayList<LatLng> latLngArrayList = new ArrayList<>();
    private static List<AGENT_MODAL_FOR_ASSIGNING> agentData;
    private HashMap<Marker, Integer> idHAshMap = new HashMap<>();
    static ProgressDialog progressDoalog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_view_agents_on_map, container, false);

        context = getContext();
        Paper.init(context);

        progressDoalog = new ProgressDialog(context);
        progressDoalog.setMax(100);
        progressDoalog.setMessage("Assigning Please Wait");
        progressDoalog.setTitle("Labeeb");
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);


        request_lat = getArguments().getDouble("request_lat") + "";
        request_lon = getArguments().getDouble("request_lon") + "";
        company_id = getArguments().getInt("company_id") + "";
        request_subcategory = getArguments().getInt("request_subcategory") + "";
        status = getArguments().getInt("status");

        request_id = getArguments().getInt("request_id");


        ////Toast.makeText(getApplicationContext(),request_lat+"",Toast.LENGTH_SHORT).show();
        //Toast.makeText(getApplicationContext(),request_lon+"",Toast.LENGTH_SHORT).show();
        //Toast.makeText(getApplicationContext(),request_subcategory+"",Toast.LENGTH_SHORT).show();
        // Toast.makeText(getApplicationContext(),company_id+"",Toast.LENGTH_SHORT).show();

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        return view;
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        progressDoalog.show();

        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getContext(), R.raw.custom_map));

            if (!success) {
                Log.e("MapsActivityRaw", "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e("MapsActivityRaw", "Can't find style.", e);
        }

        RequestBody lat_part = RequestBody.create(MultipartBody.FORM, request_lat);
        RequestBody lng_part = RequestBody.create(MultipartBody.FORM, request_lon);
        RequestBody company_id_part = RequestBody.create(MultipartBody.FORM, company_id);
        RequestBody subCat_part = RequestBody.create(MultipartBody.FORM, request_subcategory);

        GetAllAgents get_agents = ApiClient.getApiClient().create(GetAllAgents.class);
        Call<List<AGENT_MODAL_FOR_ASSIGNING>> call = get_agents.getAgents(
                lat_part,
                lng_part,
                company_id_part,
                subCat_part
        );
        call.enqueue(new Callback<List<AGENT_MODAL_FOR_ASSIGNING>>() {
            @Override
            public void onResponse(Call<List<AGENT_MODAL_FOR_ASSIGNING>> call, Response<List<AGENT_MODAL_FOR_ASSIGNING>> response) {
                progressDoalog.dismiss();
                if (response.isSuccessful()) {
                    agentData = response.body();

                    //Toast.makeText(getApplicationContext(),response.body().get(0).getLat()+"",Toast.LENGTH_SHORT).show();

                    for (int i = 0; i < response.body().size(); i++) {
                        latLngArrayList.add(new LatLng(
                                response.body().get(i).getLat(),
                                response.body().get(i).getLon()));
                        Marker marker = mMap.addMarker(
                                new MarkerOptions().
                                        position(latLngArrayList.get(i)).title(String.format("%.2f", agentData.get(i).getDistance()) + " Km").icon(BitmapDescriptorFactory.fromBitmap(Tools.getBitmapFromVectorDrawable(context, R.drawable.marker_icon))));
                        mHashMap.put(marker, i);
                        idHAshMap.put(marker, response.body().get(i).getAgent_id());

                        //       Toast.makeText(getApplicationContext(),idHAshMap.get(marker)+"",Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<List<AGENT_MODAL_FOR_ASSIGNING>> call, Throwable t) {
                progressDoalog.dismiss();
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        /*

        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {


                View view = getLayoutInflater().inflate(R.layout.agent_infowindow_layout,null);
                LinearLayout upper_window = view.findViewById(R.id.upper_window);
                LinearLayout bottom_window  = view.findViewById(R.id.bottom_window);
                CircleImageView circleImageView = view.findViewById(R.id.agent_profile_img);
                Button btn = view.findViewById(R.id.agent_Assign_btn);

                String ServiceTitle = marker.getTitle();
                if(ServiceTitle.equals("RequestLocation")){
                    btn.setVisibility(View.GONE);
                    bottom_window.setVisibility(View.VISIBLE);
                    upper_window.setVisibility(View.GONE);
                    //TextView markerTitle = view.findViewById(R.id.service_location_marker_title);
                    //markerTitle.setText();
                }else{
                    bottom_window.setVisibility(View.GONE);
                    upper_window.setVisibility(View.VISIBLE);
                    TextView agentName = view.findViewById(R.id.agent_name_infowindow);
                    TextView agentDistance = view.findViewById(R.id.agentDistance_fromservice);
                    RatingBar agentRating = view.findViewById(R.id.viewagentRating);
                    agentRating.setMax(5);
                    agentRating.setRating(agentData.get(mHashMap.get(marker)).getRating());
                    agentName.setText(agentData.get(mHashMap.get(marker)).getUser_name());
                    String[] AgentAvatarPath = agentData.get(mHashMap.get(marker)).getAvatar().split("avatars");
                    Toast.makeText(getApplicationContext(),AgentAvatarPath[1]+"",Toast.LENGTH_SHORT).show();
                    Glide.with(context)
                            .load("http://52.221.159.42:3000/api/avatars"+AgentAvatarPath[1])
                            .override(200, 200)
                            .into(circleImageView);
                    agent_id = agentData.get(mHashMap.get(marker)).getAgent_id()+"";
                    //final String request_id = agentData.get(mHashMap.get(marker)).getId()+"";
                    if(status == 1){
                        btn.setVisibility(View.VISIBLE);
                        if(agentData.get(mHashMap.get(marker)).getAvailble_status() == 1){
                            btn.setBackgroundColor(Color.RED);
                            btn.setText("Busy");
                        }
                        else if(agentData.get(mHashMap.get(marker)).getAvailble_status() == 0){
                            btn.setBackgroundColor(Color.GREEN);
                            btn.setText("Available");
                        }
                    }

                    LatLng latLng = marker.getPosition();
                }

                return view;
            }
        });

        */
        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(Double.parseDouble(request_lat), Double.parseDouble(request_lon));
        mMap.addMarker(new MarkerOptions().position(sydney).title("RequestLocation"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, 10));
        // mMap.setOnInfoWindowClickListener(this);
        mMap.setOnMarkerClickListener(this);
    }
/*
    @Override
    public void onInfoWindowClick(Marker marker) {
        String ServiceTitle = marker.getTitle();
        if(ServiceTitle.equals("RequestLocation")){}
        else {
             showDialog();}

        }

        */

    @Override
    public boolean onMarkerClick(Marker marker) {

        if (!marker.getTitle().equals("RequestLocation")) {
            progressDoalog.show();
            DialogFragment newFragment = new FragmentViewAgentsonMap.UpdatingDialogue(
                    idHAshMap.get(marker),
                    mHashMap.get(marker)
            );
            newFragment.show(getActivity().getFragmentManager(), "missiles");
        }
        return false;
    }

    /*
    @SuppressLint("ValidFragment")
    public  class MyAlertDialogFragment extends DialogFragment {



        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
//            int title = getArguments().getInt("title");

            return new AlertDialog.Builder(getActivity())
                    .setTitle("Are You Sure to Want Assign that Agent")
                    .setPositiveButton("Assign",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    doPositiveClick();
                                }
                            }
                    )
                    .setNegativeButton("Cancel",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    doNegativeClick();
                                }
                            }
                    )
                    .create();
        }
    }

    void showDialog() {
        DialogFragment newFragment = new MyAlertDialogFragment();
        newFragment.show(getFragmentManager(), "dialog");
    }

    public void doPositiveClick() {
        // Do stuff here.
        RequestBody request_id_part = RequestBody.create(MultipartBody.FORM,request_id+"");
        RequestBody status_part = RequestBody.create(MultipartBody.FORM,+2+"");
        RequestBody agent_id_part = RequestBody.create(MultipartBody.FORM,agent_id);

        Toast.makeText(getApplicationContext(),request_id+" "+agent_id,Toast.LENGTH_LONG).show();
        AssignAgent_Interface assignAgent_interface = ApiClient.getApiClient().create(AssignAgent_Interface.class);
        Call<MSG_MODAL> call1 = assignAgent_interface.assignAgent(
                request_id_part,
                status_part,
                agent_id_part
        );
        call1.enqueue(new Callback<MSG_MODAL>() {
            @Override
            public void onResponse(Call<MSG_MODAL> call, Response<MSG_MODAL> response) {
                if(response.isSuccessful()){
                if (response.code() == 200) {
                    if (response.body().getStatus() == 507) {
                            Toast.makeText(getApplicationContext(),response.body().getMsg(),Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                else {
                    Toast.makeText(getApplicationContext(),response.message(),Toast.LENGTH_SHORT).show();
                }





            }

            @Override
            public void onFailure(Call<MSG_MODAL> call, Throwable t) {
                Toast.makeText(getApplicationContext(),t.getMessage(),Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void doNegativeClick() {
        // Do stuff here.
    }

*/
    @SuppressLint("ValidFragment")
    public static class UpdatingDialogue extends DialogFragment {

        int index = 0;
        CircleImageView circleImageView;
        TextView name, contact_number, completed_request;
        int agent_id;
        RatingBar customer_company_ratingbar;
        TextView agent_Distance;

        public UpdatingDialogue(int agent_id, int index) {
            this.agent_id = agent_id;
            this.index = index;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            progressDoalog.dismiss();


            // Use the Builder class for convenient dialog construction
            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            final LayoutInflater inflater = getActivity().getLayoutInflater();

            // Inflate and set the layout for the dialog
            // Pass null as the parent view because its going in the dialog layout
            final View view = inflater.inflate(R.layout.agent_profile_dialogue, null);

            builder.setView(view);
            circleImageView = view.findViewById(R.id.agent_profile_img);
            name = view.findViewById(R.id.agent_profile_name);
            customer_company_ratingbar = view.findViewById(R.id.customer_company_ratingbar);
            contact_number = view.findViewById(R.id.agent_contact_number);
            completed_request = view.findViewById(R.id.agent_profile_completed_requests);
            agent_Distance = view.findViewById(R.id.agent_distance_txt);

            name.setText(agentData.get(index).getUser_name());
            customer_company_ratingbar.setRating(agentData.get(index).getRating());
            String[] agentAvatarPath = agentData.get(index).getAvatar().split("avatars");

            try {
                Glide.with(context).
                        load(Constants.PROFILE_Url + agentData.get(index).getAvatar()).
                        apply(new RequestOptions().placeholder(R.drawable.ic_placeholders_05).error(R.drawable.ic_placeholders_03).diskCacheStrategy(DiskCacheStrategy.RESOURCE))
                        .into(circleImageView);
            } catch (IllegalArgumentException ex) {

            } catch (NullPointerException e) {

            }


            /*Glide.with(context)
                    .load("http://52.221.159.42:5000/api/avatars" + agentAvatarPath[1])
                    .override(200, 200)
                    .into(circleImageView);*/
            contact_number.setText(agentData.get(index).getNumber() + "");
            completed_request.setText(agentData.get(index).getCompleted_req() + "");
            agent_Distance.setVisibility(View.VISIBLE);
            agent_Distance.setText(String.format("%.2f", agentData.get(index).getDistance()) + " km");

            builder.setPositiveButton("Assign", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    progressDoalog.show();
                    RequestBody request_id_part = RequestBody.create(MultipartBody.FORM, request_id + "");
                    RequestBody status_part = RequestBody.create(MultipartBody.FORM, +3 + "");
                    RequestBody agent_id_part = RequestBody.create(MultipartBody.FORM, agent_id + "");

                    // Toast.makeText(ViewAgentsOnMap.this,request_id+" "+agent_id,Toast.LENGTH_LONG).show();
                    Assign_Agent_Interface assignAgent_interface = ApiClient.getApiClient().create(Assign_Agent_Interface.class);
                    Call<MSG_MODAL> call1 = assignAgent_interface.assignAgent(
                            request_id_part,
                            status_part,
                            agent_id_part
                    );
                    call1.enqueue(new Callback<MSG_MODAL>() {
                        @Override
                        public void onResponse(Call<MSG_MODAL> call, Response<MSG_MODAL> response) {
                            progressDoalog.dismiss();
                            if (response.isSuccessful()) {
                                Toast.makeText(context, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                                if (isAdded()) {
                                    startActivity(new Intent(context, CompanyDashboard.class));
                                }

                            } else {
                                Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                            }


                        }

                        @Override
                        public void onFailure(Call<MSG_MODAL> call, Throwable t) {
                            progressDoalog.dismiss();
                            Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    });
                }
            }).setNegativeButton("Call", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" +
                            agentData.get(index).getNumber()));
                    startActivity(intent);
                }
            });


            return builder.create();

        }
    }

    public static Fragment newInstance() {
        return new FragmentViewAgentsonMap();
    }
}

