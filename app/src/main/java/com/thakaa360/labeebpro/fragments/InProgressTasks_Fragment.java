package com.thakaa360.labeebpro.fragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.github.marlonlom.utilities.timeago.TimeAgo;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.thakaa360.labeebpro.R;
import com.thakaa360.labeebpro.activities.AgentOnMap;
import com.thakaa360.labeebpro.activities.FullMapsActivity;
import com.thakaa360.labeebpro.activities.ViewAgentsOnMap;
import com.thakaa360.labeebpro.helpers.AnimationItem;
import com.thakaa360.labeebpro.helpers.Full_Image;
import com.thakaa360.labeebpro.helpers.TimeConverter;
import com.thakaa360.labeebpro.server.client.ApiClient;
import com.thakaa360.labeebpro.server.interfaces.Cancel_Request_Interface;
import com.thakaa360.labeebpro.server.interfaces.Requests_Interface;
import com.thakaa360.labeebpro.server.modals.INPROGRESS_TASKS_MODAL;
import com.thakaa360.labeebpro.server.modals.MSG_MODAL;
import com.thakaa360.labeebpro.server.modals.PENDING_TASKS_MODAL;
import com.thakaa360.labeebpro.utilities.Constants;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InProgressTasks_Fragment extends Fragment {
    static View view;
    ArrayList<INPROGRESS_TASKS_MODAL> modalArrayList = new ArrayList();
    RecyclerView recyclerView;

    private AlertDialog.Builder alertDialog;
    private EditText et_country;
    private int edit_position;
    private boolean add = false;
    private Paint p = new Paint();

    private SQLiteDatabase sqLiteDatabase;
    private Cursor cursor;
    private static int user_id;
    private static Context context;
    private TextView
            service_name_txt,
            service_subname_txt;
    ArrayList<PENDING_TASKS_MODAL> data = new ArrayList<>();

   // ProgressDialog progressDoalog;
    private AnimationItem[] mAnimationItems;
    private AnimationItem mSelectedItem;
    private SwipeRefreshLayout
            swipeRefreshLayout;
    private ProgressBar
            spinner;
    Requests_Interface requests_interface;
    private static Animator mCurrentAnimator;

    // The system "short" animation time duration, in milliseconds. This
    // duration is ideal for subtle animations or animations that occur
    // very frequently.
    private static int mShortAnimationDuration;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.inprogress_tasks_fragment,container,false);

        spinner = view.findViewById(R.id.progressBar1);
        spinner.setVisibility(View.GONE);

        recyclerView = view.findViewById(R.id.taskList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mAnimationItems = getAnimationItems();
        mSelectedItem = mAnimationItems[1];

        context = getContext();

        swipeRefreshLayout = view.findViewById(R.id.refresh_layout);



        sqLiteDatabase = context.openOrCreateDatabase(Constants.DB_NAME,Context.MODE_PRIVATE,null);
        cursor = sqLiteDatabase.rawQuery(Constants.GET_PROFILE,null);
        cursor.moveToFirst();

        user_id = cursor.getInt(cursor.getColumnIndex(Constants.DB_USER_ID));
        sqLiteDatabase.close();

        spinner.setVisibility(View.VISIBLE);

        requests_interface = ApiClient.getApiClient().create(Requests_Interface.class);
        Call<List<INPROGRESS_TASKS_MODAL>>call = requests_interface.get_CompanyInProgressTasks(user_id);
        call.enqueue(new Callback<List<INPROGRESS_TASKS_MODAL>>() {
            @Override
            public void onResponse(Call<List<INPROGRESS_TASKS_MODAL>> call, Response<List<INPROGRESS_TASKS_MODAL>> response) {
                spinner.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    modalArrayList.clear();
                    for (int i = 0;i < response.body().size();i++){
                        INPROGRESS_TASKS_MODAL modal = response.body().get(i);
                        modalArrayList.add(modal);
                     }

                    SwipeAdapter adapter = new SwipeAdapter(R.layout.inprogress_task_item,modalArrayList);
                    recyclerView.setAdapter(adapter);
                    runLayoutAnimation(recyclerView,mSelectedItem);
                }
                else{
                    Toast.makeText(getContext(), response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<INPROGRESS_TASKS_MODAL>> call, Throwable t) {
                spinner.setVisibility(View.GONE);
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                requests_interface = ApiClient.getApiClient().create(Requests_Interface.class);
                Call<List<INPROGRESS_TASKS_MODAL>>call1 = requests_interface.get_CompanyInProgressTasks(user_id);
                call1.enqueue(new Callback<List<INPROGRESS_TASKS_MODAL>>() {
                    @Override
                    public void onResponse(Call<List<INPROGRESS_TASKS_MODAL>> call, Response<List<INPROGRESS_TASKS_MODAL>> response) {
                        spinner.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                        if (response.isSuccessful()){
                            modalArrayList.clear();
                            for (int i = 0;i < response.body().size();i++){
                                INPROGRESS_TASKS_MODAL modal = response.body().get(i);
                                modalArrayList.add(modal);
                            }

                            SwipeAdapter adapter = new SwipeAdapter(R.layout.inprogress_task_item,modalArrayList);
                            recyclerView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                            runLayoutAnimation(recyclerView,mSelectedItem);
                        }
                        else{
                            Toast.makeText(getContext(), response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<INPROGRESS_TASKS_MODAL>> call, Throwable t) {
                        spinner.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                        Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });

        return view;
    }

    public static Fragment newInstance() {
        return new InProgressTasks_Fragment();
    }


    private void initDialog(){
        alertDialog = new AlertDialog.Builder(getContext());
       alertDialog.setTitle("Are you Sure want to delete ");
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                    //   adapter.addItem(et_country.getText().toString());
                  //  inProgressTaskAdapter.notifyDataSetChanged();
                    dialog.dismiss();

            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
               // inProgressTaskAdapter.notifyDataSetChanged();
                dialog.dismiss();
            }
        });

        alertDialog.setCancelable(false);
    }
    private void initCancelDialog(){
        alertDialog = new AlertDialog.Builder(getContext());
        alertDialog.setTitle("Are you Sure want to cancel ");
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                //   adapter.addItem(et_country.getText().toString());
          //      inProgressTaskAdapter.notifyDataSetChanged();
                dialog.dismiss();

            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
           //     inProgressTaskAdapter.notifyDataSetChanged();
                dialog.dismiss();
            }
        });

        alertDialog.setCancelable(false);
    }
    protected AnimationItem[] getAnimationItems() {
        return new AnimationItem[] {
                new AnimationItem("Fall down", R.anim.layout_animation_fall_down),
                new AnimationItem("Slide from right", R.anim.layout_animation_from_right),
                new AnimationItem("Slide from bottom", R.anim.layout_animation_from_bottom)
        };
    }
    private void runLayoutAnimation(final RecyclerView recyclerView, final AnimationItem item) {
        final Context context = recyclerView.getContext();

        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(context, item.getResourceId());

        recyclerView.setLayoutAnimation(controller);
        recyclerView.getAdapter().notifyDataSetChanged();
        recyclerView.scheduleLayoutAnimation();
    }


    private class SwipeAdapter extends BaseQuickAdapter<INPROGRESS_TASKS_MODAL,BaseViewHolder>{
        View customView;
        ImageView serviceIcon;
        EditText bidAmount;
        ImageView status_badge ;
        ImageView track_agent;
        TextView trackTxt;
        TextView task_time;
        List<INPROGRESS_TASKS_MODAL> listdata ;
        public SwipeAdapter(int layoutResId, @Nullable List<INPROGRESS_TASKS_MODAL> data) {
            super(layoutResId, data);
            listdata = data;
        }

        @Override
        protected void convert(final BaseViewHolder helper, final INPROGRESS_TASKS_MODAL item) {
            serviceIcon  = helper.getView(R.id.task_icon);
            service_name_txt = helper.getView(R.id.task_serviceName);
            service_subname_txt = helper.getView(R.id.task_subDetail);
            status_badge = helper.getView(R.id.status_badge);
            track_agent = helper.getView(R.id.task_sound);
            trackTxt = helper.getView(R.id.trackAgent);
            task_time = helper.getView(R.id.task_time);

            task_time.setText(TimeAgo.using(TimeConverter.StringToMilliSecs(item.getUpdated_at())));

            try {
                Glide.with(mContext).
                        load(Constants.PROFILE_Url+item.getCat_avatar()).
                        apply(new RequestOptions().placeholder(R.drawable.ic_placeholders_05).error(R.drawable.ic_placeholders_03).diskCacheStrategy(DiskCacheStrategy.RESOURCE))
                        .into(serviceIcon);
            }catch (IllegalArgumentException ex){

            }
            catch (NullPointerException e){

            }

            service_name_txt.setText(item.getThiscat_title());
            service_subname_txt.setText(item.getSubcat_title());

            track_agent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle1 = new Bundle();
                    bundle1.putInt(Constants.AGENT_ID,listdata.get(helper.getAdapterPosition()).getAgent_id());
                    bundle1.putDouble(Constants.REQ_LAT,listdata.get(helper.getAdapterPosition()).getLat());
                    bundle1.putDouble(Constants.REQ_LNG,listdata.get(helper.getAdapterPosition()).getLon());

                    Intent intent = new Intent(context, AgentOnMap.class);
                    intent.putExtras(bundle1);
                    startActivity(intent);
                }
            });

         /*   if (item.getStatus() == 2){
                status_badge.setImageResource(R.drawable.inprogress_company_assigned_badge
                );
                track_agent.setVisibility(View.GONE);
                trackTxt.setVisibility(View.GONE);
            }
            if (item.getStatus() == 3){

            }*/

            switch (listdata.get(helper.getAdapterPosition()).getStatus()){

                case 2:
                    status_badge.setImageResource(R.drawable.inprogress_company_assigned_badge
                    );
                    track_agent.setVisibility(View.GONE);
                    trackTxt.setVisibility(View.GONE);
                    break;
                case 3:
                    track_agent.setVisibility(View.VISIBLE);
                    trackTxt.setVisibility(View.VISIBLE);
                    status_badge.setImageResource(R.drawable.inprogress_agent_assigned_badge);
                    break;
                case 4:
                    track_agent.setVisibility(View.VISIBLE);
                    trackTxt.setVisibility(View.VISIBLE);
                    status_badge.setImageResource(R.drawable.inprogress_agent_moving_badge);
                    break;
                case 5:
                    track_agent.setVisibility(View.VISIBLE);
                    trackTxt.setVisibility(View.VISIBLE);
                    status_badge.setImageResource(R.drawable.inprogress_agent_arrived_badge);
                    break;
                case 6:
                    track_agent.setVisibility(View.VISIBLE);
                    trackTxt.setVisibility(View.VISIBLE);
                    status_badge.setImageResource(R.drawable.inprogress_task_inprogress_badge);
                    break;

            }



            helper.getView(R.id.cancel_menu).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Cancel_Request_Interface cancel_request_interface = ApiClient.getApiClient().create(Cancel_Request_Interface.class);
                        Call<MSG_MODAL> call = cancel_request_interface.cancelReq(
                                RequestBody.create(MultipartBody.FORM,user_id+""),
                                        RequestBody.create(MultipartBody.FORM,item.getId()+"")
                        );

//                        progressDoalog.show();
                        call.enqueue(new Callback<MSG_MODAL>() {
                            @Override
                            public void onResponse(Call<MSG_MODAL> call, Response<MSG_MODAL> response) {
                            //    progressDoalog.dismiss();
                                if (response.isSuccessful()){
                                   Toast.makeText(getContext(),response.body().getMsg(),Toast.LENGTH_LONG).show();
                                }
                                else{
                                    Toast.makeText(context,response.message(),Toast.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<MSG_MODAL> call, Throwable t) {
                              //  progressDoalog.dismiss();
                                Toast.makeText(context,t.getMessage(),Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                });
            helper.getView(R.id.assign_menu).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   // Toast.makeText(getContext(),helper.getAdapterPosition()+" Assign",Toast.LENGTH_LONG).show();
                    Bundle b =new Bundle();
                    b.putDouble("request_lat",item.getLat());
                    b.putDouble("request_lon",item.getLon());
                    b.putInt("request_id",item.getId());
                    b.putInt("company_id",item.getSp_id());
                    b.putInt("request_subcategory",item.getThisubcat_id());
                    b.putInt("status",3);
                    Intent i = new Intent(getContext(),ViewAgentsOnMap.class);
                    i.putExtras(b);
                    startActivity(i);
                }
            });

            helper.getView(R.id.view_detail).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    RequestDetailInternalBottomSheet fragment = new RequestDetailInternalBottomSheet(item);
                    fragment.show(((FragmentActivity) context).getSupportFragmentManager(), "ServiceCategoriesBottomSheet" + item.getId());
                }
            });
        }
    }

    @SuppressLint("ValidFragment")
    public static   class RequestDetailInternalBottomSheet extends BottomSheetDialogFragment {

        private BottomSheetBehavior mBehavior;
        private AppBarLayout app_bar_layout;
        private View
                company_info_layout,
                agent_info_layout;
        /* Company Section */
        private CircleImageView
                company_info_thumbnail,
                company_dialler;
        private TextView
                company_info_name;
        private MaterialRatingBar
                company_info_rating;

        /*Agent Section*/
        private CircleImageView
                agent_info_thumbnail,
                agent_dialler,
                agent_map_launcher;
        private TextView
                agent_info_name;
        private MaterialRatingBar
                agent_info_rating;

        ImageView
                task_image,
                task_image_full;
        TextView
                serviceNameTxt,
                serviceDetailTxt,
                task_time,
                request_address;
        ImageButton
                callButton,
                audioPlayButton;

        /*Audio*/

        ConstraintLayout
                play_layout;
        Button
                play_btn,
                pause_btn;
        SeekBar
                seekBar;
        TextView
                audio_duration_txt;




        private RecyclerView recyclerView;
        private Bundle bundle;


        private int  requestStatus;
        private INPROGRESS_TASKS_MODAL modal;

        private MapView mapView;
        /*private LinearLayout lyt_profile;

        public void setPeople(People people) {
          this.people = people;
        }*/
        ImageView sound_icon;
        String AUDIO_PATH ;
        private MediaPlayer mediaPlayer;
        private int playbackPosition=0;
        TextView name_toolbar;
        ImageView
                full_map_event;


        public RequestDetailInternalBottomSheet(INPROGRESS_TASKS_MODAL modal) {
            this.modal = modal;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
            final View view = View.inflate(getContext(), R.layout.request_detail_bottomsheet, null);

            dialog.setContentView(view);



            requestStatus = modal.getStatus();

            //mediaPlayer = new MediaPlayer();



            mBehavior = BottomSheetBehavior.from((View) view.getParent());
            mBehavior.setPeekHeight(BottomSheetBehavior.PEEK_HEIGHT_AUTO);



            app_bar_layout = (AppBarLayout) view.findViewById(R.id.app_bar_layout);


            name_toolbar = view.findViewById(R.id.name_toolbar);

            /*Request Section*/

            task_image = view.findViewById(R.id.task_image);
            serviceNameTxt  = view.findViewById(R.id.task_serviceName);
            serviceDetailTxt = view.findViewById(R.id.task_subDetail);
            task_image_full = view.findViewById(R.id.task_image_full);

            task_time = view.findViewById(R.id.task_time);
            request_address = view.findViewById(R.id.request_address);

            sound_icon = view.findViewById(R.id.sound_icon);


            /*Audio Configuration*/

            play_layout = view.findViewById(R.id.play_layout);
            play_btn = view.findViewById(R.id.play_btn);
            audio_duration_txt = view.findViewById(R.id.audio_time);
            seekBar = view.findViewById(R.id.audio_seekbar);
            pause_btn = view.findViewById(R.id.pause_btn);

            audioPlayButton = view.findViewById(R.id.audio_play_btn);
            callButton = view.findViewById(R.id.call_btn);

            full_map_event = view.findViewById(R.id.full_map_event);

            mapView = view.findViewById(R.id.request_location);
            mapView.onCreate(savedInstanceState);
            mapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {

                    try {
                        // Customise the styling of the base map using a JSON object defined
                        // in a raw resource file.
                        boolean success = googleMap.setMapStyle(
                                MapStyleOptions.loadRawResourceStyle(
                                        getContext(), R.raw.custom_map));

                        if (!success) {
                            Log.e("MapsActivityRaw", "Style parsing failed.");
                        }
                    } catch (Resources.NotFoundException e) {
                        Log.e("MapsActivityRaw", "Can't find style.", e);
                    }

                    googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                    // map.setMyLocationEnabled(true);

// Needs to call MapsInitializer before doing any CameraUpdateFactory calls
//                double lat =  Double.parseDouble(Paper.book().read(Constants.LATITUDE).toString());
                    //            double lng = Double.parseDouble(Paper.book().read(Constants.LONGITUDE).toString());
                    MapsInitializer.initialize(getContext());

                    googleMap.addMarker(new MarkerOptions().position(new LatLng(
                            modal.getLat(),
                            modal.getLon())).title("Your Location"));
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(
                            modal.getLat(),
                            modal.getLon()), 16);
                    googleMap.animateCamera(cameraUpdate);
                }
            });

            seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                        mediaPlayer.seekTo(seekBar.getProgress());
                    }
                }
            });

            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            try {
                mediaPlayer.setDataSource(Constants.REQ_Audio_BASE_URL+modal.getVoice());
                mediaPlayer.prepare();
                int dur =  mediaPlayer.getDuration()/1000;
                String duration = null;

                duration = "00:"+String.format("%02d", dur);


                audio_duration_txt.setText(duration);
            } catch (IOException e) {
                e.printStackTrace();
            }

            play_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    pause_btn.setVisibility(View.VISIBLE);
                    play_btn.setVisibility(View.GONE);
                    try {
                        playAudio(Constants.REQ_Audio_BASE_URL+modal.getVoice());
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(context,e.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }
            });

            pause_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (mediaPlayer != null && mediaPlayer.isPlaying()){
                        mediaPlayer.stop();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            seekBar.setProgress(0,true);
                        }
                        else{
                            seekBar.setProgress(0);
                        }
                        play_btn.setVisibility(View.VISIBLE);
                        pause_btn.setVisibility(View.GONE);
                    }
                }
            });


            try {
                if (modal.getFinal_bid() == 0) {
                    task_time.setText("Quote");
                } else {
                    task_time.setText(modal.getFinal_bid() + " AED");
                }
            }
            catch (NullPointerException e){
                task_time.setText("");
            }

            callButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:"+modal.getCus_number()));
                    startActivity(intent);
                }
            });

            audioPlayButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    play_layout.setVisibility(View.VISIBLE);
                }
            });

            request_address.setText(modal.getAddress());
            try{

                /*String[] image_Array = modal.getAvatar().split("app/");
                String req_img = image_Array[1];
                Log.v("req_img",req_img);*/
                Glide.with(getContext()).
                        load(Constants.REQ_IMAGE_BASE_URL+modal.getCus_avatar()).
                        apply(new RequestOptions().placeholder(R.drawable.ic_placeholders_05).error(R.drawable.ic_placeholders_03).diskCacheStrategy(DiskCacheStrategy.RESOURCE))
                        .into(task_image);
                Glide.with(getContext()).
                        load(Constants.PROFILE_Url+modal.getCat_avatar()).
                        apply(new RequestOptions().placeholder(R.drawable.ic_placeholders_05).error(R.drawable.ic_placeholders_03).diskCacheStrategy(DiskCacheStrategy.RESOURCE))
                        .into(task_image_full);
            }catch (IllegalArgumentException ex){
                Toast.makeText(getContext(),ex.getMessage(),Toast.LENGTH_LONG).show();
            }
            catch (NullPointerException ex){
                Toast.makeText(getContext(),ex.getMessage(),Toast.LENGTH_LONG).show();
            }

            task_image_full.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, Full_Image.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.FULL_SCREEN_IMAGE,Constants.REQ_IMAGE_BASE_URL+modal.getAvatar());
                    intent.putExtras(bundle);
                    startActivity(intent);
                    getActivity().overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                }
            });

            full_map_event.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, FullMapsActivity.class);
                    Bundle bundle  = new Bundle();
                    bundle.putDouble(Constants.REQ_LAT,modal.getLat());
                    bundle.putDouble(Constants.REQ_LNG,modal.getLon());
                    intent.putExtras(bundle);
                    startActivity(intent);
                    getActivity().overridePendingTransition(android.R.anim.fade_in,android.R.anim.slide_out_right);
                }
            });


            serviceNameTxt.setText(modal.getCus_name());
            serviceDetailTxt.setText(modal.getCus_number());
            serviceDetailTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:"+modal.getCus_number()));
                    startActivity(intent);
                }
            });












            Log.v("dataBottom",modal.getStatus()+"");

            //hideView(recyclerView);

            mBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    if (BottomSheetBehavior.STATE_EXPANDED == newState) {
                       // showView(app_bar_layout, getActionBarSize());
                        // showView(recyclerView,recyclerView.getScrollBarSize());
//                    hideView(lyt_profile);
                    }
                    if (BottomSheetBehavior.STATE_COLLAPSED == newState) {
                      //  hideView(app_bar_layout);
                        //  showView(lyt_profile, getActionBarSize());
                    }

                    if (BottomSheetBehavior.STATE_HIDDEN == newState) {
                        dismiss();
                    }
                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                }
            });

            ((ImageButton) view.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });

            return dialog;
        }

        @Override
        public void onStart() {
            super.onStart();
            mapView.onStart();
            mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

        }

        private void hideView(View view) {
            ViewGroup.LayoutParams params = view.getLayoutParams();
            params.height = 0;
            view.setLayoutParams(params);
        }

        private void showView(View view, int size) {
            ViewGroup.LayoutParams params = view.getLayoutParams();
            params.height = size;
            view.setLayoutParams(params);
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
            mapView.onDestroy();
        }

        @Override
        public void onLowMemory() {
            super.onLowMemory();
            mapView.onLowMemory();
        }

        private int getActionBarSize() {
            final TypedArray styledAttributes = getContext().getTheme().obtainStyledAttributes(new int[]{android.R.attr.actionBarSize});
            int size = (int) styledAttributes.getDimension(0, 0);
            return size;
        }

        private void playAudio(String url)
        {
            killMediaPlayer();


            try {
                mediaPlayer = new MediaPlayer();
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mediaPlayer.setDataSource(url);

                mediaPlayer.prepare(); // might take long! (for buffering, etc)
                mediaPlayer.start();
                seekBar.setMax(mediaPlayer.getDuration());
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        pause_btn.setVisibility(View.GONE);
                        play_btn.setVisibility(View.VISIBLE);
                    }
                });
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        int currentPosition = mediaPlayer.getCurrentPosition();
                        int total = mediaPlayer.getDuration();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            seekBar.setProgress(0,true);
                        }
                        else{
                            seekBar.setProgress(0);
                        }

                        while (mediaPlayer != null && mediaPlayer.isPlaying() && currentPosition < total) {
                            try {
                                Thread.sleep(1000);
                                currentPosition = mediaPlayer.getCurrentPosition();
                            } catch (InterruptedException e) {
                                return;
                            } catch (Exception e) {
                                return;
                            }


                            if (!mediaPlayer.isPlaying()){
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    seekBar.setProgress(0,true);
                                }
                                else{
                                    seekBar.setProgress(0);
                                }

                            }
                            else{
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    seekBar.setProgress(currentPosition,true);
                                }
                                else{
                                    seekBar.setProgress(currentPosition);
                                }
                            }
                            if (seekBar.getProgress() == total){
                                play_btn.setVisibility(View.VISIBLE);
                                pause_btn.setVisibility(View.GONE);
                            }
                        }
                    }
                }).start();

            } catch (IOException e) {
                e.printStackTrace();
                Log.v("erro",e.getMessage());
            }

        }


        private void killMediaPlayer() {
            if(mediaPlayer!=null) {
                try {
                    mediaPlayer.release();
                    seekBar.setProgress(0);
                }
                catch(Exception e) {
                    e.printStackTrace();
                }
            }
        }


    }
    private static void zoomImageFromThumb(final View thumbView, String imageResId) {
        // If there's an animation in progress, cancel it
        // immediately and proceed with this one.
        if (mCurrentAnimator != null) {
            mCurrentAnimator.cancel();
        }

        // Load the high-resolution "zoomed-in" image.
        final ImageView expandedImageView = (ImageView) view.findViewById(
                R.id.expanded_image);
        try{


            Glide.with(context).
                    load(imageResId).
                    apply(new RequestOptions().placeholder(R.drawable.ic_placeholders_05).error(R.drawable.ic_placeholders_03).diskCacheStrategy(DiskCacheStrategy.RESOURCE))
                    .into(expandedImageView);
        }catch (IllegalArgumentException ex){
            Toast.makeText(context,ex.getMessage(),Toast.LENGTH_LONG).show();
        }
        catch (NullPointerException ex){
            Toast.makeText(context,ex.getMessage(),Toast.LENGTH_LONG).show();
        }

        // Calculate the starting and ending bounds for the zoomed-in image.
        // This step involves lots of math. Yay, math.
        final Rect startBounds = new Rect();
        final Rect finalBounds = new Rect();
        final Point globalOffset = new Point();

        // The start bounds are the global visible rectangle of the thumbnail,
        // and the final bounds are the global visible rectangle of the container
        // view. Also set the container view's offset as the origin for the
        // bounds, since that's the origin for the positioning animation
        // properties (X, Y).
        thumbView.getGlobalVisibleRect(startBounds);
        view.findViewById(R.id.container)
                .getGlobalVisibleRect(finalBounds, globalOffset);
        startBounds.offset(-globalOffset.x, -globalOffset.y);
        finalBounds.offset(-globalOffset.x, -globalOffset.y);

        // Adjust the start bounds to be the same aspect ratio as the final
        // bounds using the "center crop" technique. This prevents undesirable
        // stretching during the animation. Also calculate the start scaling
        // factor (the end scaling factor is always 1.0).
        float startScale;
        if ((float) finalBounds.width() / finalBounds.height()
                > (float) startBounds.width() / startBounds.height()) {
            // Extend start bounds horizontally
            startScale = (float) startBounds.height() / finalBounds.height();
            float startWidth = startScale * finalBounds.width();
            float deltaWidth = (startWidth - startBounds.width()) / 2;
            startBounds.left -= deltaWidth;
            startBounds.right += deltaWidth;
        } else {
            // Extend start bounds vertically
            startScale = (float) startBounds.width() / finalBounds.width();
            float startHeight = startScale * finalBounds.height();
            float deltaHeight = (startHeight - startBounds.height()) / 2;
            startBounds.top -= deltaHeight;
            startBounds.bottom += deltaHeight;
        }

        // Hide the thumbnail and show the zoomed-in view. When the animation
        // begins, it will position the zoomed-in view in the place of the
        // thumbnail.
        thumbView.setAlpha(0f);
        expandedImageView.setVisibility(View.VISIBLE);

        // Set the pivot point for SCALE_X and SCALE_Y transformations
        // to the top-left corner of the zoomed-in view (the default
        // is the center of the view).
        expandedImageView.setPivotX(0f);
        expandedImageView.setPivotY(0f);

        // Construct and run the parallel animation of the four translation and
        // scale properties (X, Y, SCALE_X, and SCALE_Y).
        AnimatorSet set = new AnimatorSet();
        set
                .play(ObjectAnimator.ofFloat(expandedImageView, View.X,
                        startBounds.left, finalBounds.left))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
                        startBounds.top, finalBounds.top))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X,
                        startScale, 1f))
                .with(ObjectAnimator.ofFloat(expandedImageView,
                        View.SCALE_Y, startScale, 1f));
        set.setDuration(mShortAnimationDuration);
        set.setInterpolator(new DecelerateInterpolator());
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mCurrentAnimator = null;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                mCurrentAnimator = null;
            }
        });
        set.start();
        mCurrentAnimator = set;

        // Upon clicking the zoomed-in image, it should zoom back down
        // to the original bounds and show the thumbnail instead of
        // the expanded image.
        final float startScaleFinal = startScale;
        expandedImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCurrentAnimator != null) {
                    mCurrentAnimator.cancel();
                }

                // Animate the four positioning/sizing properties in parallel,
                // back to their original values.
                AnimatorSet set = new AnimatorSet();
                set.play(ObjectAnimator
                        .ofFloat(expandedImageView, View.X, startBounds.left))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.Y,startBounds.top))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.SCALE_X, startScaleFinal))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.SCALE_Y, startScaleFinal));
                set.setDuration(mShortAnimationDuration);
                set.setInterpolator(new DecelerateInterpolator());
                set.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        thumbView.setAlpha(1f);
                        expandedImageView.setVisibility(View.GONE);
                        mCurrentAnimator = null;
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        thumbView.setAlpha(1f);
                        expandedImageView.setVisibility(View.GONE);
                        mCurrentAnimator = null;
                    }
                });
                set.start();
                mCurrentAnimator = set;
            }
        });
    }
}
