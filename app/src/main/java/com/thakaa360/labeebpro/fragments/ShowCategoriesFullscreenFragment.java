package com.thakaa360.labeebpro.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;


import com.github.vivchar.rendererrecyclerviewadapter.CompositeViewRenderer;
import com.github.vivchar.rendererrecyclerviewadapter.DefaultCompositeViewModel;
import com.github.vivchar.rendererrecyclerviewadapter.RendererRecyclerViewAdapter;
import com.github.vivchar.rendererrecyclerviewadapter.ViewModel;
import com.github.vivchar.rendererrecyclerviewadapter.ViewRenderer;
import com.github.vivchar.rendererrecyclerviewadapter.binder.CompositeViewBinder;
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewBinder;
import com.thakaa360.labeebpro.R;
import com.thakaa360.labeebpro.activities.AgentRegistration;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ShowCategoriesFullscreenFragment extends DialogFragment {


    private int request_code = 0;
    private View root_view;
    RecyclerView recyclerView;
    Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        root_view = inflater.inflate(R.layout.show_all_categories_fulllayout, container, false);

        context = getContext();

        recyclerView = root_view.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        ((ImageButton) root_view.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        ((Button) root_view.findViewById(R.id.bt_save)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        RendererRecyclerViewAdapter adapter = new RendererRecyclerViewAdapter();
        adapter.registerRenderer(new MyCompositeViewBinder(
                context,
                R.layout.parent_itemview_layout,
                R.id.parerecyclerview,
                MyCompositeModel.class,
                new ViewBinder<MyCompositeModel>(
                        R.layout.parent_itemview_layout,
                        MyCompositeModel.class,
                        (model, finder, payloads) ->finder.setText(R.id.catTitle,model.getName())
                )/* imported from library */
                //new ViewBinder.Binder(){...} /* if you need bind something else */
        ).registerRenderer(new ViewBinder<>(
                R.layout.chip_item_selector_layout,
                ChildItem.class,
                (model, finder, payloads) -> finder
                        .setText(R.id.item_txt, model.getName())
        )));
        adapter.setItems(getParentItems());

        recyclerView.setAdapter(adapter);

        return  root_view;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    private List<MyCompositeModel> getParentItems() {
        ArrayList<MyCompositeModel> parents = new ArrayList();
        for (int i = 0;i < 5;i++) {
            ArrayList<ChildItem> children = new ArrayList();
            for (int j = 0;j < 15;j++) {
                ChildItem childItem = new ChildItem();
                childItem.setName("SubCategory "+j);
                children.add(childItem);
            }
            MyCompositeModel myCompositeModel = new MyCompositeModel(children);
            myCompositeModel.setName("Category "+i);
            parents.add(myCompositeModel);
        }
        return parents;
    }
    public class ChildItem implements ViewModel {
        String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public class MyCompositeModel extends DefaultCompositeViewModel {

        String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @NonNull
        protected final List<? extends ViewModel> mItems;

        public MyCompositeModel(@NonNull final List<? extends ViewModel> items) {
            super(items);
            mItems = items;
        }

        @NonNull
        @Override
        public List<? extends ViewModel> getItems() {
            return mItems;
        }

    }
    public class MyCompositeViewBinder extends CompositeViewBinder<MyCompositeModel> {
        Context context;
        public MyCompositeViewBinder(Context context, int layoutID, int recyclerViewID, @NonNull Class<MyCompositeModel> type, ViewBinder<MyCompositeModel> myCompositeModelViewBinder) {
            super(layoutID, recyclerViewID, type);
            this.context = context;
        }

        @NonNull
        @Override
        public CompositeViewRenderer registerRenderer(@NonNull ViewRenderer renderer) {
            return super.registerRenderer(renderer);
        }

        @Override
        protected RecyclerView.LayoutManager createLayoutManager() {
            return new GridLayoutManager(context,3);
        }
    }


}