package com.thakaa360.labeebpro.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.thakaa360.labeebpro.R;
import com.thakaa360.labeebpro.activities.CompanyDashboard;
import com.thakaa360.labeebpro.server.client.ApiClient;
import com.thakaa360.labeebpro.server.interfaces.Assign_Agent_Interface;
import com.thakaa360.labeebpro.server.interfaces.GetAllAgents;
import com.thakaa360.labeebpro.server.modals.AGENT_MODAL_FOR_ASSIGNING;
import com.thakaa360.labeebpro.server.modals.MSG_MODAL;
import com.thakaa360.labeebpro.utilities.Constants;
import com.thakaa360.labeebpro.utilities.Tools;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import io.paperdb.Paper;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentViewAgentsAsList extends Fragment{
    View view;
    Context context;
    ProgressBar progressBar;
    RecyclerView recyclerView;

    private String
            request_lat,
            request_lon,
            company_id,
            request_subcategory;
    private int status;
    private static int request_id;
    private ArrayList<AGENT_MODAL_FOR_ASSIGNING> agentData= new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_viewagentaslist,container,false);

        progressBar = view.findViewById(R.id.progressBar1);

        context = getContext();
        Paper.init(context);

        recyclerView = view.findViewById(R.id.taskList);
        request_lat = getArguments().getDouble("request_lat") + "";
        request_lon = getArguments().getDouble("request_lon") + "";
        company_id = getArguments().getInt("company_id") + "";
        request_subcategory = getArguments().getInt("request_subcategory") + "";
        status = getArguments().getInt("status");

        request_id = getArguments().getInt("request_id");

        RequestBody lat_part = RequestBody.create(MultipartBody.FORM, request_lat);
        RequestBody lng_part = RequestBody.create(MultipartBody.FORM, request_lon);
        RequestBody company_id_part = RequestBody.create(MultipartBody.FORM, company_id);
        RequestBody subCat_part = RequestBody.create(MultipartBody.FORM, request_subcategory);

        GetAllAgents get_agents = ApiClient.getApiClient().create(GetAllAgents.class);
        Call<List<AGENT_MODAL_FOR_ASSIGNING>> call = get_agents.getAgents(
                lat_part,
                lng_part,
                company_id_part,
                subCat_part
        );
        call.enqueue(new Callback<List<AGENT_MODAL_FOR_ASSIGNING>>() {
            @Override
            public void onResponse(Call<List<AGENT_MODAL_FOR_ASSIGNING>> call, Response<List<AGENT_MODAL_FOR_ASSIGNING>> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    for (int i = 0; i < response.body().size(); i++) {
                        AGENT_MODAL_FOR_ASSIGNING modal = response.body().get(i);
                        agentData.add(modal);
                      }

                        recyclerView.setLayoutManager(new LinearLayoutManager(context));
                        recyclerView.setAdapter(new MyAdapter(agentData));

                } else {
                    Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<List<AGENT_MODAL_FOR_ASSIGNING>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }
    public static Fragment newInstance() {
        return new FragmentViewAgentsAsList();
    }

    public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder>{

        ArrayList<AGENT_MODAL_FOR_ASSIGNING> data;

        public MyAdapter(ArrayList<AGENT_MODAL_FOR_ASSIGNING> data) {
            this.data = data;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view  = LayoutInflater.from(context).inflate(R.layout.agentaslist_item,parent,false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
            try {
                Glide.with(context).
                        load(Constants.PROFILE_Url+data.get(position).getAvatar()).
                        apply(new RequestOptions().placeholder(R.drawable.ic_placeholders_05).error(R.drawable.ic_placeholders_03).diskCacheStrategy(DiskCacheStrategy.RESOURCE))
                        .into(holder.agent_profile_img);
            }catch (IllegalArgumentException ex){

            }
            catch (NullPointerException e){

            }

            holder.agent_profile_name.setText(data.get(position).getUser_name());
            holder.agent_distance.setText(String.format("%.2f", data.get(position).getDistance())+" km");
            holder.agent_rating.setRating(data.get(position).getRating());

            holder.agent_assign_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    progressBar.setVisibility(View.VISIBLE);
                    RequestBody request_id_part = RequestBody.create(MultipartBody.FORM, request_id + "");
                    RequestBody status_part = RequestBody.create(MultipartBody.FORM, +3 + "");
                    RequestBody agent_id_part = RequestBody.create(MultipartBody.FORM, data.get(holder.getAdapterPosition()).getAgent_id() + "");

                    // Toast.makeText(ViewAgentsOnMap.this,request_id+" "+agent_id,Toast.LENGTH_LONG).show();
                    Assign_Agent_Interface assignAgent_interface = ApiClient.getApiClient().create(Assign_Agent_Interface.class);
                    Call<MSG_MODAL> call1 = assignAgent_interface.assignAgent(
                            request_id_part,
                            status_part,
                            agent_id_part
                    );
                    call1.enqueue(new Callback<MSG_MODAL>() {
                        @Override
                        public void onResponse(Call<MSG_MODAL> call, Response<MSG_MODAL> response) {
                          progressBar.setVisibility(View.GONE);
                            if (response.isSuccessful()) {
                                Toast.makeText(context, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                                if (isAdded()) {
                                    startActivity(new Intent(context, CompanyDashboard.class));
                                }

                            } else {
                                Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                            }


                        }

                        @Override
                        public void onFailure(Call<MSG_MODAL> call, Throwable t) {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    });
                }
            });

        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder{

            CircleImageView
                    agent_profile_img;
            TextView
                    agent_profile_name,
                    agent_distance;
            RatingBar
                    agent_rating;
            Button
                    agent_assign_btn;

            public ViewHolder(View itemView) {
                super(itemView);

                agent_profile_img = itemView.findViewById(R.id.agent_profile_img);
                agent_distance = itemView.findViewById(R.id.agent_distance_txt);
                agent_profile_name = itemView.findViewById(R.id.agent_profile_name);
                agent_rating = itemView.findViewById(R.id.agent_profile_rating);
                agent_assign_btn = itemView.findViewById(R.id.agent_assign_btn);

            }
        }

    }

}
