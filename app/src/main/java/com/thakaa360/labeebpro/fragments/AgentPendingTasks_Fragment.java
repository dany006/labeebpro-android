package com.thakaa360.labeebpro.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Paint;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.github.marlonlom.utilities.timeago.TimeAgo;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.thakaa360.labeebpro.R;
import com.thakaa360.labeebpro.activities.FullMapsActivity;
import com.thakaa360.labeebpro.activities.RequestOnMap;
import com.thakaa360.labeebpro.helpers.AnimationItem;
import com.thakaa360.labeebpro.helpers.Full_Image;
import com.thakaa360.labeebpro.helpers.TimeConverter;
import com.thakaa360.labeebpro.server.client.ApiClient;
import com.thakaa360.labeebpro.server.interfaces.Agent_Job_Interface;
import com.thakaa360.labeebpro.server.interfaces.Requests_Interface;
import com.thakaa360.labeebpro.server.interfaces.RespondCustomer;
import com.thakaa360.labeebpro.server.modals.AGENTS_TASKS;
import com.thakaa360.labeebpro.server.modals.INPROGRESS_TASKS_MODAL;
import com.thakaa360.labeebpro.server.modals.MSG_MODAL;
import com.thakaa360.labeebpro.server.modals.AGENTS_TASKS;
import com.thakaa360.labeebpro.utilities.Constants;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import io.paperdb.Paper;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AgentPendingTasks_Fragment extends Fragment {
    View view;
    ArrayList<AGENTS_TASKS> modalArrayList = new ArrayList();
    RecyclerView recyclerView;

    private AlertDialog.Builder alertDialog;
    private EditText et_country;
    private int edit_position;
    private boolean add = false;
    private Paint p = new Paint();

    ArrayList<AGENTS_TASKS> data = new ArrayList<>();

    static ProgressDialog progressDoalog;
    private AnimationItem[] mAnimationItems;
    private AnimationItem mSelectedItem;

    private SQLiteDatabase sqLiteDatabase;
    private Cursor cursor;
    private static int user_id;
    private static Context context;

    private TextView
                                service_name_txt,
                                service_subname_txt;
    ProgressBar
                        progressBar;
    SwipeRefreshLayout
                        swipeRefreshLayout;
    Requests_Interface requests_interface;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.pending_tasks_fragment,container,false);

        context = getContext();
        Paper.init(context);
        recyclerView = view.findViewById(R.id.taskList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mAnimationItems = getAnimationItems();
        mSelectedItem = mAnimationItems[1];


        progressBar = view.findViewById(R.id.progressBar1);

        swipeRefreshLayout = view.findViewById(R.id.refresh_layout);

        sqLiteDatabase = context.openOrCreateDatabase(Constants.DB_NAME,Context.MODE_PRIVATE,null);
        cursor = sqLiteDatabase.rawQuery(Constants.GET_PROFILE,null);
        cursor.moveToFirst();

        user_id = cursor.getInt(cursor.getColumnIndex(Constants.DB_USER_ID));
        sqLiteDatabase.close();
        requests_interface = ApiClient.getApiClient().create(Requests_Interface.class);
        Call<List<AGENTS_TASKS>> call = requests_interface.get_AgentPendingTasks(user_id);
        call.enqueue(new Callback<List<AGENTS_TASKS>>() {
            @Override
            public void onResponse(Call<List<AGENTS_TASKS>> call, Response<List<AGENTS_TASKS>> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    modalArrayList.clear();
                    for (int i = 0;i < response.body().size();i++){
                        AGENTS_TASKS modal = response.body().get(i);
                        modalArrayList.add(modal);
                    }

                    SwipeAdapter adapter = new SwipeAdapter(R.layout.agentpending_task_item,modalArrayList);
                    recyclerView.setAdapter(adapter);
                    runLayoutAnimation(recyclerView,mSelectedItem);
                }
                else{

                }
            }

            @Override
            public void onFailure(Call<List<AGENTS_TASKS>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);

            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Call<List<AGENTS_TASKS>> call = requests_interface.get_AgentPendingTasks(user_id);
                call.enqueue(new Callback<List<AGENTS_TASKS>>() {
                    @Override
                    public void onResponse(Call<List<AGENTS_TASKS>> call, Response<List<AGENTS_TASKS>> response) {
                        progressBar.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                        if (response.isSuccessful()){
                            modalArrayList.clear();
                            for (int i = 0;i < response.body().size();i++){
                                AGENTS_TASKS modal = response.body().get(i);
                                modalArrayList.add(modal);
                            }

                            SwipeAdapter adapter = new SwipeAdapter(R.layout.agentpending_task_item,modalArrayList);
                            recyclerView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                            runLayoutAnimation(recyclerView,mSelectedItem);
                        }
                        else{

                        }
                    }

                    @Override
                    public void onFailure(Call<List<AGENTS_TASKS>> call, Throwable t) {
                        swipeRefreshLayout.setRefreshing(false);
                        progressBar.setVisibility(View.GONE);

                    }
                });
            }
        });

        return view;
    }

    public static Fragment newInstance() {
        return new AgentPendingTasks_Fragment();
    }


    private void initDialog(){
        alertDialog = new AlertDialog.Builder(getContext());
       alertDialog.setTitle("Are you Sure want to delete ");
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                    //   adapter.addItem(et_country.getText().toString());
                  //  inProgressTaskAdapter.notifyDataSetChanged();
                    dialog.dismiss();

            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
               // inProgressTaskAdapter.notifyDataSetChanged();
                dialog.dismiss();
            }
        });

        alertDialog.setCancelable(false);
    }
    private void initCancelDialog(){
        alertDialog = new AlertDialog.Builder(getContext());
        alertDialog.setTitle("Are you Sure want to cancel ");
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                //   adapter.addItem(et_country.getText().toString());
          //      inProgressTaskAdapter.notifyDataSetChanged();
                dialog.dismiss();

            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
           //     inProgressTaskAdapter.notifyDataSetChanged();
                dialog.dismiss();
            }
        });

        alertDialog.setCancelable(false);
    }
    protected AnimationItem[] getAnimationItems() {
        return new AnimationItem[] {
                new AnimationItem("Fall down", R.anim.layout_animation_fall_down),
                new AnimationItem("Slide from right", R.anim.layout_animation_from_right),
                new AnimationItem("Slide from bottom", R.anim.layout_animation_from_bottom)
        };
    }
    private void runLayoutAnimation(final RecyclerView recyclerView, final AnimationItem item) {
        final Context context = recyclerView.getContext();

        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(context, item.getResourceId());

        recyclerView.setLayoutAnimation(controller);
        recyclerView.getAdapter().notifyDataSetChanged();
        recyclerView.scheduleLayoutAnimation();
    }


    private class SwipeAdapter extends BaseQuickAdapter<AGENTS_TASKS,BaseViewHolder>{
        View customView;
        ImageView serviceIcon;
        EditText bidAmount;
        MaterialStyledDialog dialog;
        TextView task_time;
        public SwipeAdapter(int layoutResId, @Nullable List<AGENTS_TASKS> data) {
            super(layoutResId, data);
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            customView = inflater.inflate(R.layout.create_bid_dialogue, null);
            bidAmount = customView.findViewById(R.id.bid_amount_field);

        }

        @Override
        protected void convert(final BaseViewHolder helper, final AGENTS_TASKS item) {

             serviceIcon  = helper.getView(R.id.service_icon);
            service_name_txt = helper.getView(R.id.task_serviceName);
            service_subname_txt = helper.getView(R.id.task_subDetail);
            task_time = helper.getView(R.id.task_time);

            task_time.setText(TimeAgo.using(TimeConverter.StringToMilliSecs(item.getUpdated_at())));

            try {
                Glide.with(mContext).
                        load(Constants.PROFILE_Url+item.getCat_avatar()).
                        apply(new RequestOptions().placeholder(R.drawable.profile_dummy).error(R.drawable.profile_dummy).diskCacheStrategy(DiskCacheStrategy.RESOURCE))
                        .into(serviceIcon);
            }catch (IllegalArgumentException ex){

            }
            catch (NullPointerException e){

            }

            service_name_txt.setText(item.getThiscat_title());
            service_subname_txt.setText(item.getSubcat_title());


            helper.getView(R.id.viewtask).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AGENTS_TASKS modal = item;
                    modal.setStatus(4);
                    RequestDetailInternalBottomSheet fragment = new RequestDetailInternalBottomSheet(modal);
                    fragment.show(((FragmentActivity) context).getSupportFragmentManager(), "ServiceCategoriesBottomSheet" + item.getId());
                }
            });




            helper.getView(R.id.start).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Agent_Job_Interface agent_job_interface = ApiClient.getApiClient().create(Agent_Job_Interface.class);
                    Call<MSG_MODAL> call = agent_job_interface.update_agent_status(
                            RequestBody.create(MultipartBody.FORM,item.getId()+""),
                            RequestBody.create(MultipartBody.FORM,4+""),

                            RequestBody.create(MultipartBody.FORM,0+"")
                    );

//                    progressDoalog.show();
                    call.enqueue(new Callback<MSG_MODAL>() {
                        @Override
                        public void onResponse(Call<MSG_MODAL> call, Response<MSG_MODAL> response) {
                            if (response.isSuccessful()){
                         //       progressDoalog.dismiss();
                                AGENTS_TASKS modal = item;
                                modal.setStatus(4);
                                Paper.book().write(Constants.REQUEST_MODAL_DATA,modal);

                                getActivity().startActivity(new Intent(context, RequestOnMap.class));


                            }
                            else{
                                Toast.makeText(context,response.message(),Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<MSG_MODAL> call, Throwable t) {
                    //        progressDoalog.dismiss();
                            Toast.makeText(context,t.getMessage(),Toast.LENGTH_LONG).show();

                        }
                    });

                }
            });

        }

    }

    @SuppressLint("ValidFragment")
    public static   class RequestDetailInternalBottomSheet extends BottomSheetDialogFragment {

        private BottomSheetBehavior mBehavior;
        private AppBarLayout app_bar_layout;
        private View
                company_info_layout,
                agent_info_layout;
        /* Company Section */
        private CircleImageView
                company_info_thumbnail,
                company_dialler;
        private TextView
                company_info_name;
        private MaterialRatingBar
                company_info_rating;

        /*Agent Section*/
        private CircleImageView
                agent_info_thumbnail,
                agent_dialler,
                agent_map_launcher;
        private TextView
                agent_info_name;
        private MaterialRatingBar
                agent_info_rating;

        ImageView
                task_image,
                task_image_full;
        TextView
                serviceNameTxt,
                serviceDetailTxt,
                task_time,
                request_address;

        /*Audio*/

        ConstraintLayout
                play_layout;
        Button
                play_btn,
                pause_btn;
        SeekBar
                seekBar;
        TextView
                audio_duration_txt;

        ImageButton
                callButton,
                audioPlayButton;
        ImageView
                full_map_event;


        private RecyclerView recyclerView;
        private Bundle bundle;


        private int  requestStatus;
        private AGENTS_TASKS modal;

        private MapView mapView;
        /*private LinearLayout lyt_profile;

        public void setPeople(People people) {
          this.people = people;
        }*/
        ImageView sound_icon;
        String AUDIO_PATH ;
        private MediaPlayer mediaPlayer;
        private int playbackPosition=0;
        TextView name_toolbar;


        public RequestDetailInternalBottomSheet(AGENTS_TASKS modal) {
            this.modal = modal;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
            final View view = View.inflate(getContext(), R.layout.request_detail_bottomsheet, null);

            dialog.setContentView(view);



            requestStatus = modal.getStatus();

            //mediaPlayer = new MediaPlayer();



            mBehavior = BottomSheetBehavior.from((View) view.getParent());
            mBehavior.setPeekHeight(BottomSheetBehavior.PEEK_HEIGHT_AUTO);



            app_bar_layout = (AppBarLayout) view.findViewById(R.id.app_bar_layout);


            name_toolbar = view.findViewById(R.id.name_toolbar);

            /*Request Section*/

            task_image = view.findViewById(R.id.task_image);
            serviceNameTxt  = view.findViewById(R.id.task_serviceName);
            serviceDetailTxt = view.findViewById(R.id.task_subDetail);
            task_image_full = view.findViewById(R.id.task_image_full);

            task_time = view.findViewById(R.id.task_time);
            request_address = view.findViewById(R.id.request_address);

            sound_icon = view.findViewById(R.id.sound_icon);

            /*Audio Configuration*/

            play_layout = view.findViewById(R.id.play_layout);
            play_btn = view.findViewById(R.id.play_btn);
            audio_duration_txt = view.findViewById(R.id.audio_time);
            seekBar = view.findViewById(R.id.audio_seekbar);
            pause_btn = view.findViewById(R.id.pause_btn);

            audioPlayButton = view.findViewById(R.id.audio_play_btn);
            callButton = view.findViewById(R.id.call_btn);


            full_map_event = view.findViewById(R.id.full_map_event);

            mapView = view.findViewById(R.id.request_location);
            mapView.onCreate(savedInstanceState);
            mapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {

                    try {
                        // Customise the styling of the base map using a JSON object defined
                        // in a raw resource file.
                        boolean success = googleMap.setMapStyle(
                                MapStyleOptions.loadRawResourceStyle(
                                        getContext(), R.raw.custom_map));

                        if (!success) {
                            Log.e("MapsActivityRaw", "Style parsing failed.");
                        }
                    } catch (Resources.NotFoundException e) {
                        Log.e("MapsActivityRaw", "Can't find style.", e);
                    }

                    googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                    // map.setMyLocationEnabled(true);

// Needs to call MapsInitializer before doing any CameraUpdateFactory calls
//                double lat =  Double.parseDouble(Paper.book().read(Constants.LATITUDE).toString());
                    //            double lng = Double.parseDouble(Paper.book().read(Constants.LONGITUDE).toString());
                    MapsInitializer.initialize(getContext());

                    googleMap.addMarker(new MarkerOptions().position(new LatLng(
                            modal.getLat(),
                            modal.getLon())).title("Your Location"));
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(
                            modal.getLat(),
                            modal.getLon()), 16);
                    googleMap.animateCamera(cameraUpdate);
                }
            });

            full_map_event.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, FullMapsActivity.class);
                    Bundle bundle  = new Bundle();
                    bundle.putDouble(Constants.REQ_LAT,modal.getLat());
                    bundle.putDouble(Constants.REQ_LNG,modal.getLon());
                    intent.putExtras(bundle);
                    startActivity(intent);
                    getActivity().overridePendingTransition(android.R.anim.fade_in,android.R.anim.slide_out_right);
                }
            });

            seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                        mediaPlayer.seekTo(seekBar.getProgress());
                    }
                }
            });


            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            try {
                mediaPlayer.setDataSource(Constants.REQ_Audio_BASE_URL+modal.getVoice());
                mediaPlayer.prepare();
                int dur =  mediaPlayer.getDuration()/1000;
                String duration = null;

                duration = "00:"+String.format("%02d", dur);


                audio_duration_txt.setText(duration);
            } catch (IOException e) {
                e.printStackTrace();
            }

            play_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    pause_btn.setVisibility(View.VISIBLE);
                    play_btn.setVisibility(View.GONE);
                    try {
                        playAudio(Constants.REQ_Audio_BASE_URL+modal.getVoice());
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(context,e.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }
            });

            pause_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (mediaPlayer != null && mediaPlayer.isPlaying()){
                        mediaPlayer.stop();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            seekBar.setProgress(0,true);
                        }
                        else{
                            seekBar.setProgress(0);
                        }
                        play_btn.setVisibility(View.VISIBLE);
                        pause_btn.setVisibility(View.GONE);
                    }
                }
            });

            callButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:"+modal.getCus_num()));
                    startActivity(intent);
                }
            });

            audioPlayButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    play_layout.setVisibility(View.VISIBLE);
                }
            });

            task_time.setVisibility(View.GONE);

            request_address.setText(modal.getAddress());
            try{

                /*String[] image_Array = modal.getAvatar().split("app/");
                String req_img = image_Array[1];
                Log.v("req_img",req_img);*/
                Glide.with(getContext()).
                        load(Constants.REQ_IMAGE_BASE_URL+modal.getCus_avatar()).
                        apply(new RequestOptions().placeholder(R.drawable.ic_placeholders_05).error(R.drawable.ic_placeholders_03).diskCacheStrategy(DiskCacheStrategy.RESOURCE))
                        .into(task_image);
                Glide.with(getContext()).
                        load(Constants.REQ_IMAGE_BASE_URL+modal.getAvatar()).
                        apply(new RequestOptions().placeholder(R.drawable.ic_placeholders_05).error(R.drawable.ic_placeholders_03).diskCacheStrategy(DiskCacheStrategy.RESOURCE))
                        .into(task_image_full);
            }catch (IllegalArgumentException ex){
                Toast.makeText(getContext(),ex.getMessage(),Toast.LENGTH_LONG).show();
            }
            catch (NullPointerException ex){
                Toast.makeText(getContext(),ex.getMessage(),Toast.LENGTH_LONG).show();
            }
            serviceNameTxt.setText(modal.getCus_name());
            serviceDetailTxt.setText(modal.getCus_num());
            serviceDetailTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:"+modal.getCus_num()));
                    startActivity(intent);
                }
            });

            task_image_full.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, Full_Image.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.FULL_SCREEN_IMAGE,Constants.REQ_IMAGE_BASE_URL+modal.getAvatar());
                    intent.putExtras(bundle);
                    startActivity(intent);
                    getActivity().overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                }
            });












            Log.v("dataBottom",modal.getStatus()+"");

            //hideView(recyclerView);

            mBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    if (BottomSheetBehavior.STATE_EXPANDED == newState) {
                      //  showView(app_bar_layout, getActionBarSize());
                        // showView(recyclerView,recyclerView.getScrollBarSize());
//                    hideView(lyt_profile);
                    }
                    if (BottomSheetBehavior.STATE_COLLAPSED == newState) {
                      //  hideView(app_bar_layout);
                        //  showView(lyt_profile, getActionBarSize());
                    }

                    if (BottomSheetBehavior.STATE_HIDDEN == newState) {
                        dismiss();
                    }
                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                }
            });

            ((ImageButton) view.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });

            return dialog;
        }

        @Override
        public void onStart() {
            super.onStart();
            mapView.onStart();
            mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

        }

        private void hideView(View view) {
            ViewGroup.LayoutParams params = view.getLayoutParams();
            params.height = 0;
            view.setLayoutParams(params);
        }

        private void showView(View view, int size) {
            ViewGroup.LayoutParams params = view.getLayoutParams();
            params.height = size;
            view.setLayoutParams(params);
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
            mapView.onDestroy();
        }

        @Override
        public void onLowMemory() {
            super.onLowMemory();
            mapView.onLowMemory();
        }

        private int getActionBarSize() {
            final TypedArray styledAttributes = getContext().getTheme().obtainStyledAttributes(new int[]{android.R.attr.actionBarSize});
            int size = (int) styledAttributes.getDimension(0, 0);
            return size;
        }

        private void playAudio(String url)
        {
            killMediaPlayer();


            try {
                mediaPlayer = new MediaPlayer();
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mediaPlayer.setDataSource(url);

                mediaPlayer.prepare(); // might take long! (for buffering, etc)
                mediaPlayer.start();
                seekBar.setMax(mediaPlayer.getDuration());
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        int currentPosition = mediaPlayer.getCurrentPosition();
                        int total = mediaPlayer.getDuration();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            seekBar.setProgress(0,true);
                        }
                        else{
                            seekBar.setProgress(0);
                        }

                        while (mediaPlayer != null && mediaPlayer.isPlaying() && currentPosition < total) {
                            try {
                                Thread.sleep(1000);
                                currentPosition = mediaPlayer.getCurrentPosition();
                            } catch (InterruptedException e) {
                                return;
                            } catch (Exception e) {
                                return;
                            }


                            if (!mediaPlayer.isPlaying()){
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    seekBar.setProgress(0,true);
                                }
                                else{
                                    seekBar.setProgress(0);
                                }
                            }
                            else{
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    seekBar.setProgress(currentPosition,true);
                                }
                                else{
                                    seekBar.setProgress(currentPosition);
                                }
                            }
                            if (seekBar.getProgress() == total){
                                play_btn.setVisibility(View.VISIBLE);
                                pause_btn.setVisibility(View.GONE);
                            }
                        }
                    }
                }).start();

            } catch (IOException e) {
                e.printStackTrace();
                Log.v("erro",e.getMessage());
            }

        }


        private void killMediaPlayer() {
            if(mediaPlayer!=null) {
                try {
                    mediaPlayer.release();
                    seekBar.setProgress(0);
                }
                catch(Exception e) {
                    e.printStackTrace();
                }
            }
        }


    }
    @SuppressLint("ValidFragment")
    public static  class QuoteSelection extends DialogFragment {
        int reqId;
        AGENTS_TASKS modal;

        public QuoteSelection(int reqId,AGENTS_TASKS modal) {
            this.reqId = reqId;
            this.modal  = modal;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the Builder class for convenient dialog construction
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Place Quote")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // FIRE ZE MISSILES!

                            Agent_Job_Interface agent_job_interface = ApiClient.getApiClient().create(Agent_Job_Interface.class);
                            Call<MSG_MODAL> call = agent_job_interface.update_agent_status(
                                    RequestBody.create(MultipartBody.FORM,modal.getId()+""),
                                    RequestBody.create(MultipartBody.FORM,4+""),

                                    RequestBody.create(MultipartBody.FORM,0+"")
                            );

                            progressDoalog.show();
                            call.enqueue(new Callback<MSG_MODAL>() {
                                @Override
                                public void onResponse(Call<MSG_MODAL> call, Response<MSG_MODAL> response) {
                                    if (response.isSuccessful()){
                                        progressDoalog.dismiss();
                                        Paper.book().write(Constants.REQUEST_MODAL_DATA,modal);

                                        getActivity().startActivity(new Intent(context, RequestOnMap.class));
                                        dismiss();

                                    }
                                    else{
                                        Toast.makeText(context,response.message(),Toast.LENGTH_LONG).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<MSG_MODAL> call, Throwable t) {
                                    progressDoalog.dismiss();
                                    Toast.makeText(context,t.getMessage(),Toast.LENGTH_LONG).show();

                                }
                            });

                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                            dismiss();
                        }
                    });
            // Create the AlertDialog object and return it
            return builder.create();
        }
    }
}
