package com.thakaa360.labeebpro.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.github.marlonlom.utilities.timeago.TimeAgo;
import com.thakaa360.labeebpro.R;
import com.thakaa360.labeebpro.helpers.AnimationItem;
import com.thakaa360.labeebpro.helpers.TimeConverter;
import com.thakaa360.labeebpro.server.client.ApiClient;
import com.thakaa360.labeebpro.server.interfaces.Requests_Interface;
import com.thakaa360.labeebpro.server.modals.AGENT_HISTORY;
import com.thakaa360.labeebpro.server.modals.AGENT_HISTORY;
import com.thakaa360.labeebpro.utilities.Constants;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AgentHistory_Fragment extends Fragment {
    View view;
    ArrayList<AGENT_HISTORY> modalArrayList = new ArrayList();
    RecyclerView recyclerView;

    private AlertDialog.Builder alertDialog;
    private EditText et_country;
    private int edit_position;
    private boolean add = false;
    private Paint p = new Paint();

    private SQLiteDatabase sqLiteDatabase;
    private Cursor cursor;
    private static int user_id;
    private static Context context;
    private TextView
            service_name_txt,
            service_subname_txt;
    ArrayList<AGENT_HISTORY> data = new ArrayList<>();

    private AnimationItem[] mAnimationItems;
    private AnimationItem mSelectedItem;
    private ProgressBar
                    progressBar;
    private SwipeRefreshLayout
                    swipeRefreshLayout;
    Requests_Interface requests_interface;
    Call<List<AGENT_HISTORY>>call;
    Call<List<AGENT_HISTORY>>call1;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.inprogress_tasks_fragment,container,false);

        recyclerView = view.findViewById(R.id.taskList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mAnimationItems = getAnimationItems();
        mSelectedItem = mAnimationItems[1];

        progressBar = view.findViewById(R.id.progressBar1);

        context = getContext();

        sqLiteDatabase = context.openOrCreateDatabase(Constants.DB_NAME,Context.MODE_PRIVATE,null);
        cursor = sqLiteDatabase.rawQuery(Constants.GET_PROFILE,null);
        cursor.moveToFirst();

        user_id = cursor.getInt(cursor.getColumnIndex(Constants.DB_USER_ID));
        Log.v("company_id",user_id+"");
        sqLiteDatabase.close();



         requests_interface = ApiClient.getApiClient().create(Requests_Interface.class);
         call = requests_interface.getAgentHistory(user_id);
        call.enqueue(new Callback<List<AGENT_HISTORY>>() {
            @Override
            public void onResponse(Call<List<AGENT_HISTORY>> call, Response<List<AGENT_HISTORY>> response) {

                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    modalArrayList.clear();
                    for (int i = 0;i < response.body().size();i++){
                        AGENT_HISTORY modal = response.body().get(i);
                        modalArrayList.add(modal);
                    }

                    SwipeAdapter adapter = new SwipeAdapter(R.layout.history_item,modalArrayList);
                    recyclerView.setAdapter(adapter);
                    runLayoutAnimation(recyclerView,mSelectedItem);
                }
                else{
                    Toast.makeText(getContext(), response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<AGENT_HISTORY>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


        swipeRefreshLayout = view.findViewById(R.id.refresh_layout);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                call1 = requests_interface.getAgentHistory(user_id);
                call1.enqueue(new Callback<List<AGENT_HISTORY>>() {
                    @Override
                    public void onResponse(Call<List<AGENT_HISTORY>> call, Response<List<AGENT_HISTORY>> response) {
                        swipeRefreshLayout.setRefreshing(false);
                        progressBar.setVisibility(View.GONE);
                        if (response.isSuccessful()){
                            modalArrayList.clear();
                            for (int i = 0;i < response.body().size();i++){
                                AGENT_HISTORY modal = response.body().get(i);
                                modalArrayList.add(modal);
                            }

                            SwipeAdapter adapter = new SwipeAdapter(R.layout.history_item,modalArrayList);
                            recyclerView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                            runLayoutAnimation(recyclerView,mSelectedItem);
                        }
                        else{
                            Toast.makeText(getContext(), response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<AGENT_HISTORY>> call, Throwable t) {
                        progressBar.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                        Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });

        return view;
    }

    public static Fragment newInstance() {
        return new AgentHistory_Fragment();
    }


    private void initDialog(){
        alertDialog = new AlertDialog.Builder(getContext());
       alertDialog.setTitle("Are you Sure want to delete ");
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                    //   adapter.addItem(et_country.getText().toString());
                  //  inProgressTaskAdapter.notifyDataSetChanged();
                    dialog.dismiss();

            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
               // inProgressTaskAdapter.notifyDataSetChanged();
                dialog.dismiss();
            }
        });

        alertDialog.setCancelable(false);
    }
    private void initCancelDialog(){
        alertDialog = new AlertDialog.Builder(getContext());
        alertDialog.setTitle("Are you Sure want to cancel ");
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                //   adapter.addItem(et_country.getText().toString());
          //      inProgressTaskAdapter.notifyDataSetChanged();
                dialog.dismiss();

            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
           //     inProgressTaskAdapter.notifyDataSetChanged();
                dialog.dismiss();
            }
        });

        alertDialog.setCancelable(false);
    }
    protected AnimationItem[] getAnimationItems() {
        return new AnimationItem[] {
                new AnimationItem("Fall down", R.anim.layout_animation_fall_down),
                new AnimationItem("Slide from right", R.anim.layout_animation_from_right),
                new AnimationItem("Slide from bottom", R.anim.layout_animation_from_bottom)
        };
    }
    private void runLayoutAnimation(final RecyclerView recyclerView, final AnimationItem item) {
        final Context context = recyclerView.getContext();

        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(context, item.getResourceId());

        recyclerView.setLayoutAnimation(controller);
        recyclerView.getAdapter().notifyDataSetChanged();
        recyclerView.scheduleLayoutAnimation();
    }


    private class SwipeAdapter extends BaseQuickAdapter<AGENT_HISTORY,BaseViewHolder>{
        View customView;
        CircleImageView serviceIcon;
        EditText bidAmount;
        TextView tasktime;
        public SwipeAdapter(int layoutResId, @Nullable List<AGENT_HISTORY> data) {
            super(layoutResId, data);
        }

        @Override
        protected void convert(final BaseViewHolder helper, final AGENT_HISTORY item) {
            serviceIcon  = helper.getView(R.id.service_icon);
            service_name_txt = helper.getView(R.id.task_serviceName);
            service_subname_txt = helper.getView(R.id.task_subDetail);

            tasktime = helper.getView(R.id.task_time);

            tasktime.setText(TimeAgo.using(TimeConverter.StringToMilliSecs(item.getUpdated_at())));
            try {
                Glide.with(mContext).
                        load(Constants.REQ_IMAGE_BASE_URL+item.getAvatar()).
                        apply(new RequestOptions().placeholder(R.drawable.profile_dummy).error(R.drawable.profile_dummy).diskCacheStrategy(DiskCacheStrategy.RESOURCE))
                        .into(serviceIcon);
            }catch (IllegalArgumentException ex){

            }
            catch (NullPointerException e){

            }

            service_name_txt.setText(item.getThiscat_title());
            service_subname_txt.setText(item.getSubcat_title());

        }
    }

}
