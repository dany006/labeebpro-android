package com.thakaa360.labeebpro.fragments;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.thakaa360.labeebpro.R;
import com.thakaa360.labeebpro.activities.CompanyDashboard;
import com.thakaa360.labeebpro.server.client.ApiClient;
import com.thakaa360.labeebpro.server.interfaces.Assign_Agent_Interface;
import com.thakaa360.labeebpro.server.interfaces.GetAllAgents;
import com.thakaa360.labeebpro.server.modals.AGENT_MODAL_FOR_ASSIGNING;
import com.thakaa360.labeebpro.server.modals.ALL_AGENTS_MODAL;
import com.thakaa360.labeebpro.server.modals.MSG_MODAL;
import com.thakaa360.labeebpro.utilities.Constants;
import com.thakaa360.labeebpro.utilities.Tools;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import io.paperdb.Paper;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentViewALLAgentsAsList extends Fragment{
    View view;
    Context context;
    int company_id;
    SQLiteDatabase sqLiteDatabase;
    RecyclerView recyclerView;
    ProgressBar
                progressBar;

    ArrayList<ALL_AGENTS_MODAL> modals = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_viewagentaslist,container,false);


        context = getContext();
        Paper.init(context);


        progressBar = view.findViewById(R.id.progressBar1);

        sqLiteDatabase = context.openOrCreateDatabase(Constants.DB_NAME,Context.MODE_PRIVATE,null);
        Cursor c = sqLiteDatabase.rawQuery(Constants.GET_PROFILE,null);
        c.moveToFirst();
        company_id =  c.getInt(c.getColumnIndex(Constants.DB_USER_ID));

        recyclerView = view.findViewById(R.id.taskList);

        GetAllAgents getAllAgents = ApiClient.getApiClient().create(GetAllAgents.class);
        Call<List<ALL_AGENTS_MODAL>> call = getAllAgents.getAgents(company_id);
        call.enqueue(new Callback<List<ALL_AGENTS_MODAL>>() {
            @Override
            public void onResponse(Call<List<ALL_AGENTS_MODAL>> call, Response<List<ALL_AGENTS_MODAL>> response) {

               progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    //   Toast.makeText(getApplicationContext(),response.body().get(0).getUser_name(),Toast.LENGTH_SHORT).show();
                    for (int i = 0; i < response.body().size(); i++) {
                            ALL_AGENTS_MODAL modal = response.body().get(i);
                            modals.add(modal);

                    }

                    recyclerView.setLayoutManager(new GridLayoutManager(context,2));
                    recyclerView.setAdapter(new MyAdapter(modals));

                }
                else {
                    Toast.makeText(context,response.message(),Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<ALL_AGENTS_MODAL>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(context,t.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });


        return view;
    }
    public static Fragment newInstance() {
        return new FragmentViewALLAgentsAsList();
    }

    public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder>{

        ArrayList<ALL_AGENTS_MODAL> data;

        public MyAdapter(ArrayList<ALL_AGENTS_MODAL> data) {
            this.data = data;
        }

        @NonNull
        @Override
        public MyAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view  = LayoutInflater.from(context).inflate(R.layout.agentaslist_item,parent,false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final MyAdapter.ViewHolder holder, int position) {
            try {
                Glide.with(context).
                        load(Constants.PROFILE_Url+data.get(position).getAvatar()).
                        apply(new RequestOptions().placeholder(R.drawable.ic_placeholders_05).error(R.drawable.ic_placeholders_03).diskCacheStrategy(DiskCacheStrategy.RESOURCE))
                        .into(holder.agent_profile_img);
            }catch (IllegalArgumentException ex){

            }
            catch (NullPointerException e){

            }

            holder.agent_profile_name.setText(data.get(position).getUser_name());
            holder.agent_rating.setRating(Float.parseFloat(data.get(position).getRating()+""));

           holder.agent_assign_btn.setVisibility(View.GONE);
            holder.agent_distance.setVisibility(View.GONE);
            holder.agent_distance.setVisibility(View.GONE);

        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder{

            CircleImageView
                    agent_profile_img;
            TextView
                    agent_profile_name,
                    agent_distance;
            RatingBar
                    agent_rating;
            Button
                    agent_assign_btn;

            public ViewHolder(View itemView) {
                super(itemView);

                agent_profile_img = itemView.findViewById(R.id.agent_profile_img);
                agent_distance = itemView.findViewById(R.id.agent_distance_txt);
                agent_profile_name = itemView.findViewById(R.id.agent_profile_name);
                agent_rating = itemView.findViewById(R.id.agent_profile_rating);
                agent_assign_btn = itemView.findViewById(R.id.agent_assign_btn);

            }
        }

    }

}
