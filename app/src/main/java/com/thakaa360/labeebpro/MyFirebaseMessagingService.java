package com.thakaa360.labeebpro;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.JsonObject;
import com.thakaa360.labeebpro.activities.AgentOnMap;
import com.thakaa360.labeebpro.activities.CompanyDashboard;
import com.thakaa360.labeebpro.modals.NotificationData;
import com.thakaa360.labeebpro.server.modals.INPROGRESS_TASKS_MODAL;
import com.thakaa360.labeebpro.utilities.Constants;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.Map;

import io.paperdb.Paper;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    int notStatus;
    String pic1 = null;
    NotificationData notData;
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.v("NotificationData",remoteMessage.getData().toString());

        notData = new NotificationData();


        JSONObject object = new JSONObject(remoteMessage.getData());
        try {
            JSONObject notiJSON = new JSONObject(object.getString("noti"));
            JSONObject requestJSON = new JSONObject(object.getString("request"));

            notData .setThiscat_id(requestJSON.getInt("thiscat_id"));
            notData.setAddress(requestJSON.getString("address"));
            notData.setStatus(requestJSON.getInt("status"));

            Log.v("JSONSTATUS",requestJSON.getInt("status")+"");
            int status = notData.getStatus();
            if (status > 2) {
                /*Agent Profile*/
                notData.setAgent_id(requestJSON.getInt("agent_id"));
                notData.setAgent_name(requestJSON.getString("agent_name"));
                notData.setAgent_avatar(requestJSON.getString("agent_avatar"));
                notData.setAgent_phone(requestJSON.getString("agent_number"));


            }
             notData.setAvatar(requestJSON.getString("avatar"));
            if (status > 1) {
                /*Company Profile*/
                notData.setSp_id(requestJSON.getInt("sp_id"));
                notData.setSp_name(requestJSON.getString("sp_name"));
                notData.setSp_avatar(requestJSON.getString("sp_avatar"));
                notData.setSp_number(requestJSON.getString("sp_number"));

                notData.setFinal_bid(requestJSON.getDouble("final_bid"));

            }
            notData.setThiscat_title(requestJSON.getString("thiscat_title"));
            notData.setSubcat_title(requestJSON.getString("subcat_title"));

            notData.setCatAvatar(requestJSON.getString("cat_avatar"));

            notData.setLon(requestJSON.getDouble("lon"));
            notData.setLat(requestJSON.getDouble("lat"));

            notData.setThisubcat_id(requestJSON.getInt("thisubcat_id"));
            notData.setVoice(requestJSON.getString("voice"));
            notData.setCreated_at(notiJSON.getString("created_at"));
            notData.setReq_id(requestJSON.getInt("id"));

            notData.setNote_status(notiJSON.getInt("note_status"));

            /*Customer Profile*/
            notData.setCus_name(requestJSON.getString("cus_name"));
            notData.setCus_avatar(requestJSON.getString("cus_avatar"));
            notData.setCus_number(requestJSON.getString("cus_number"));


        /*    Paper.init(MyFirebaseMessagingService.this);

            Paper.book().write(Constants.NOTIFICATION_STATUS_FCM,notData);
            Paper.book().write(Constants.NOTIFICATION_STATUS_ID,1);
*/

            Log.v("thisRequest",
                    notData.getThiscat_id()+"   "+
            notData.getAddress()+"  "+
          //  notData.getAgent_id()+"     "/*+
            notData.getAvatar()+"   "+
            notData.getThiscat_title()+"    "+
            notData.getSubcat_title()+"     "+
            notData.getLat()+"  "+
            notData.getLon()+"  "+
            notData.getStatus()+"   "+
            notData.getNote_status());




            pic1 =  notiJSON.getString("req_avatar").replaceAll("/","");

            Log.v("pcis",pic1);

            Log.v("notTrigger",notStatus+"");
        } catch (JSONException e) {
            e.printStackTrace();
            Log.v("sss",e.getMessage());
        }

        sendNotification(remoteMessage,remoteMessage.getData(),notData);


    }

    private void sendNotification(RemoteMessage messageBody, Map<String, String> data,NotificationData notificationData) {

        Intent intent = new Intent(MyFirebaseMessagingService.this,CompanyDashboard.class);

        int notification_status = notificationData.getNote_status();
        if (
                notification_status == 2005 ||
                notification_status == 2006 ||
                notification_status == 2007 ||
                notification_status == 2008 ||
                notification_status == 2009 ||
                notification_status == 2015 ||
                notification_status == 2017 ||
                notification_status == 2019 ||
                notification_status == 2021 ){
           intent = new Intent(this, CompanyDashboard.class);

        }
        if (
                notification_status == 2012 ||
                        notification_status == 2014){
            intent = new Intent(this, AgentDashboard.class);

        }

        Bundle bundle = new Bundle();


        bundle.putInt(Constants.REQUEST_ID,notificationData.getReq_id());
        bundle.putInt(Constants.REQUEST_CAT_ID,notificationData.getThiscat_id());
        bundle.putInt(Constants.REQUEST_SUBCAT_ID,notificationData.getThisubcat_id());
        bundle.putString(Constants.REQUEST_CAT_TITLE,notificationData.getThiscat_title());
        bundle.putString(Constants.REQUEST_SUBCAT_TITLE,notificationData.getSubcat_title());
        bundle.putDouble(Constants.REQUEST_LAT,notificationData.getLat());
        bundle.putDouble(Constants.REQUEST_LON,notificationData.getLon());
        bundle.putString(Constants.REQUEST_VOICE,notificationData.getVoice());
        bundle.putString(Constants.REQUEST_AVATAR,notificationData.getAvatar());
        bundle.putString(Constants.REQUEST_TIME,notificationData.getCreated_at());
        bundle.putInt(Constants.REQUEST_STATUS,notificationData.getStatus());
        bundle.putString(Constants.REQUEST_CUSNAME,notificationData.getCus_name());
        bundle.putString(Constants.REQUEST_CUSAVATAR,notificationData.getCus_avatar());
        bundle.putString(Constants.REQUEST_CUSNUMBER,notificationData.getCus_number());
        bundle.putString(Constants.REQUEST_CAT_AVATAR,notificationData.getCatAvatar());
        bundle.putInt(Constants.NOTSTATUS,notificationData.getNote_status());
        bundle.putString(Constants.REQUEST_ADDRESS,notificationData.getAddress());

        if (notificationData.getStatus() >1){
            bundle.putDouble(Constants.REQUEST_FINAL_BID,notificationData.getFinal_bid());
            bundle.putString(Constants.REQUEST_COMNAME,notificationData.getSp_name());
            bundle.putString(Constants.REQUEST_COMAVATAR,notificationData.getSp_avatar());
            bundle.putString(Constants.REQUEST_COMNUMBER,notificationData.getSp_number());
            bundle.putInt(Constants.SP_ID,notificationData.getSp_id());
        }

        if (notificationData.getStatus() > 2){
            bundle.putInt(Constants.AGENT_ID,notificationData.getAgent_id());
            bundle.putString(Constants.REQUEST_AGENTNAME,notificationData.getAgent_name());
            bundle.putString(Constants.REQUEST_AGENTAVATAR,notificationData.getAgent_avatar());
            bundle.putString(Constants.REQUEST_AGENTNUMBER,notificationData.getAgent_phone());

            if (notificationData.getNote_status() == 2017 ||
                    notificationData.getNote_status() == 2019 ||
                    notificationData.getNote_status() == 2015){
                bundle = new Bundle();
                bundle.putInt(Constants.AGENT_ID,notificationData.getAgent_id());
                bundle.putDouble(Constants.REQ_LAT,notificationData.getLat());
                bundle.putDouble(Constants.REQ_LNG,notificationData.getLon());

                intent = new Intent(MyFirebaseMessagingService.this, AgentOnMap.class);

            }

        }

       // bundle.putInt(Constants.REQ);

        intent.putExtras(bundle);


        Log.v("NotificationSStatus",notificationData.getStatus()+"");

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        int UNIQUE_ID = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, UNIQUE_ID /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);
    //    dataMap = data;


        String channelId = "Channel_02";
        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.ic_launcher_foreground)
                        .setContentTitle(messageBody.getNotification().getTitle())
                 //       .setLargeIcon(myBitmap)
                        .setContentText(messageBody.getNotification().getBody())
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }
        int m = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
        notificationManager.notify(m /* ID of notification */, notificationBuilder.build());
    }
}
