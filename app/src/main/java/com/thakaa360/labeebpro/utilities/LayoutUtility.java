package com.thakaa360.labeebpro.utilities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

public class LayoutUtility {
   private static LayoutInflater layoutInflater = null;

    public static View getLayout(Context context,int LayoutRes){
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return layoutInflater.inflate(LayoutRes, null, false);
    }

}
