package com.thakaa360.labeebpro.utilities;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.maps.MapView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.thakaa360.labeebpro.LocationUpdatesService;
import com.thakaa360.labeebpro.MapsActivity;
import com.thakaa360.labeebpro.R;
import com.thakaa360.labeebpro.activities.AgentRegistration;
import com.thakaa360.labeebpro.activities.CompanyDashboard;
import com.thakaa360.labeebpro.activities.Login;
import com.thakaa360.labeebpro.activities.TrackAllAgents;
import com.thakaa360.labeebpro.server.client.ApiClient;
import com.thakaa360.labeebpro.server.interfaces.FCM_TOKEN_INTERFACE;
import com.thakaa360.labeebpro.server.interfaces.Update_SessionCode;
import com.thakaa360.labeebpro.server.interfaces.UserData_Interface;
import com.thakaa360.labeebpro.server.modals.AGENT_DATA_MODAL;
import com.thakaa360.labeebpro.server.modals.COMPANY_DATA_MODAL;
import com.thakaa360.labeebpro.server.modals.MSG_MODAL;
import com.thakaa360.labeebpro.server.modals.UPDATE_SESSION_MSG;
import com.thakaa360.labeebpro.services.NotificationService;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import io.paperdb.Paper;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Major extends AppCompatActivity {

    protected NavigationView navigationView;
    protected  int user_role =1;
    protected SQLiteDatabase sqLiteDatabase;
    protected Cursor cursorData;
    protected DrawerLayout mDrawer;
    static String language = "en";

    ListView lvDetail;
    protected Context context = Major.this;
    ArrayList myList = new ArrayList();

    String[] desc ;
    int[] img ;
    protected Cursor getSessionData;
    int session_status = 0;
    String credientials = "not";
    Intent intent;

    protected  int customer_id;

    private LinearLayout header_layout;
    private CircleImageView profile_img;
    private TextView header_profile_name;
    private TextView header_profile_credientials;

    protected Toolbar toolbar;
    protected Resources resources;
    private Cursor cursor;
    private int user_id;
    private String user_crendiatials;

    ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //setLanguage(this, new Locale("ar"));
        super.onCreate(savedInstanceState);
       /// loadLocale();
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_major);


        context = Major.this;
        Paper.init(this);
           // language = Paper.book().read("language");
/*
        if (language == null)
            Paper.book().write("language","en");
        LocalHelper.setLocale(getApplicationContext(),language);
*/
      //  toolbar =  findViewById(R.id.toolbar);
        resources = getApplicationContext().getResources();

/*
        sqLiteDatabase = openOrCreateDatabase("labeeb", MODE_PRIVATE, null);
        getSessionData = sqLiteDatabase.rawQuery("SELECT * FROM profile", null);
        getSessionData.moveToFirst();
        if (getSessionData.getCount() > 0) {
            session_status = getSessionData.getInt(getSessionData.getColumnIndex("session_code"));
            credientials = getSessionData.getString(getSessionData.getColumnIndex("user_credientials"));
            customer_id = getSessionData.getInt(getSessionData.getColumnIndex("user_id"));
*/   if (getApplicationContext().getDatabasePath(Constants.DB_NAME).exists()) {

            sqLiteDatabase = openOrCreateDatabase(Constants.DB_NAME, MODE_PRIVATE, null);
            cursor = sqLiteDatabase.rawQuery(Constants.GET_PROFILE, null);
            cursor.moveToFirst();


            user_crendiatials = cursor.getString(cursor.getColumnIndex(Constants.DB_USER_CREDIENTIALS));
            user_id = cursor.getInt(cursor.getColumnIndex(Constants.DB_USER_ID));
            session_status = cursor.getInt(cursor.getColumnIndex(Constants.DB_SESSION_STAT));
            user_role  = cursor.getInt(cursor.getColumnIndex(Constants.DB_ROLE));
            sqLiteDatabase.close();

        }

        progressDialog = new ProgressDialog(context);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

            mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            navigationView = findViewById(R.id.navi);

                final View head_view = navigationView.getHeaderView(0);
                profile_img = head_view.findViewById(R.id.navi_profile_img);
                header_profile_name = head_view.findViewById(R.id.header_profile_name);
                header_profile_credientials = head_view.findViewById(R.id.header_profile_credientials);

                if (session_status == 1){
                    if (user_role == 2) {
                        navigationView.inflateMenu(R.menu.company_menu);
                        UserData_Interface userData_interface = ApiClient.getApiClient().create(UserData_Interface.class);
                        Call<List<COMPANY_DATA_MODAL>> call = userData_interface.getCompanyDataById(
                                RequestBody.create(MultipartBody.FORM, user_id + "")
                        );
                        call.enqueue(new Callback<List<COMPANY_DATA_MODAL>>() {
                            @Override
                            public void onResponse(Call<List<COMPANY_DATA_MODAL>> call, Response<List<COMPANY_DATA_MODAL>> response) {
                                if (response.isSuccessful()) {
                                    try {
                                        Glide.with(context).
                                                load(Constants.PROFILE_Url+response.body().get(0).getAvatar()).
                                                apply(new RequestOptions().placeholder(R.drawable.ic_placeholders_05).error(R.drawable.ic_placeholders_03).diskCacheStrategy(DiskCacheStrategy.RESOURCE))
                                                .into(profile_img);
                                    }catch (IllegalArgumentException ex){

                                    }
                                    catch (NullPointerException e){

                                    }
                                        header_profile_name.setText(response.body().get(0).getCompany_name());
                                        header_profile_credientials.setText(response.body().get(0).getNumber());
                                } else {
                                    Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<List<COMPANY_DATA_MODAL>> call, Throwable t) {
                                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                    else if(user_role == 3){
                        navigationView.inflateMenu(R.menu.agent_menu);
                        UserData_Interface userData_interface = ApiClient.getApiClient().create(UserData_Interface.class);
                        Call<List<AGENT_DATA_MODAL>> call = userData_interface.getAgentById(
                                RequestBody.create(MultipartBody.FORM,user_id+"")
                        );
                        call.enqueue(new Callback<List<AGENT_DATA_MODAL>>() {
                            @Override
                            public void onResponse(Call<List<AGENT_DATA_MODAL>> call, Response<List<AGENT_DATA_MODAL>> response) {
                                if (response.isSuccessful()){
                                    try {
                                        Glide.with(context).
                                                load(Constants.PROFILE_Url+response.body().get(0).getAvatar()).
                                                apply(new RequestOptions().placeholder(R.drawable.ic_placeholders_05).error(R.drawable.ic_placeholders_03).diskCacheStrategy(DiskCacheStrategy.RESOURCE))
                                                .into(profile_img);
                                    }catch (IllegalArgumentException ex){

                                    }
                                    catch (NullPointerException e){

                                    }
                                    header_profile_name.setText(response.body().get(0).getAgent_name());
                                    header_profile_credientials.setText(response.body().get(0).getNumber());
                                }
                                else{
                                    Toast.makeText(context,response.message(),Toast.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<List<AGENT_DATA_MODAL>> call, Throwable t) {

                                Toast.makeText(context,t.getMessage(),Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                }

            navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.dashboard_item:
                            startActivity(new Intent(context, CompanyDashboard.class));
                            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                            break;
                        case R.id.track_item:
                            startActivity(new Intent(context, TrackAllAgents.class));
                            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                            break;

                        case R.id.agent_register_item:
                            startActivity(new Intent(context, AgentRegistration.class));
                            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                            break;
                        case R.id.logout_item:

                            progressDialog.show();

                            sqLiteDatabase = openOrCreateDatabase(Constants.DB_NAME,MODE_PRIVATE,null);
                            ContentValues values = new ContentValues();

                            values.put(Constants.DB_SESSION_STAT,0);
                            sqLiteDatabase.update(Constants.DB_TABLE,values,"id="+1,null);
                            sqLiteDatabase.close();
                            int ROLE = 3;
                            if (user_role == 3){
                                    ROLE = 4;

                                    Paper.book().write(Constants.SERVICE_LOCATION_STATUS,0);
                                stopService(new Intent(Major.this, LocationUpdatesService.class));



                            }

                            FCM_TOKEN_INTERFACE fcm_token_interface = ApiClient.getApiClient().create(FCM_TOKEN_INTERFACE.class);
                            Call<MSG_MODAL> call1 = fcm_token_interface.updateToken(
                                    RequestBody.create(MultipartBody.FORM,user_crendiatials),
                                    RequestBody.create(MultipartBody.FORM,ROLE+""),
                                    RequestBody.create(MultipartBody.FORM, "AAA")
                            );
                            call1.enqueue(new Callback<MSG_MODAL>() {
                                @Override
                                public void onResponse(Call<MSG_MODAL> call, Response<MSG_MODAL> response) {
                                    if (response.isSuccessful()){

                                    }else{
                                        Toast.makeText(context,response.message(),Toast.LENGTH_LONG).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<MSG_MODAL> call, Throwable t) {
                                    Toast.makeText(context,t.getMessage(),Toast.LENGTH_LONG).show();
                                }
                            });

                            Update_SessionCode update_sessionCode = ApiClient.getApiClient().create(Update_SessionCode.class);

                            if (user_role == 2){
                                Call<UPDATE_SESSION_MSG> updateSessionCodeCall = update_sessionCode.updateSessionCodeByEmail(
                                        RequestBody.create(MultipartBody.FORM,user_crendiatials),
                                        RequestBody.create(MultipartBody.FORM,"1"),
                                        RequestBody.create(MultipartBody.FORM,"3")
                                );
                                updateSessionCodeCall.enqueue(new Callback<UPDATE_SESSION_MSG>() {
                                    @Override
                                    public void onResponse(Call<UPDATE_SESSION_MSG> call, Response<UPDATE_SESSION_MSG> response) {
                                        progressDialog.dismiss();
                                        if (response.isSuccessful()){
                                            startActivity(new Intent(context, Login.class));
                                            finishAffinity();
                                            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                                        }
                                        else{
                                            Toast.makeText(context,response.message(),Toast.LENGTH_LONG).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<UPDATE_SESSION_MSG> call, Throwable t) {
                                        progressDialog.dismiss();
                                        Toast.makeText(context,t.getMessage(),Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                            else if(user_role == 3){
                                Call<UPDATE_SESSION_MSG> updateSessionCodeCall = update_sessionCode.updateSessionCodeByEmail(
                                        RequestBody.create(MultipartBody.FORM,user_crendiatials),
                                        RequestBody.create(MultipartBody.FORM,"1"),
                                        RequestBody.create(MultipartBody.FORM,"4")
                                );
                                updateSessionCodeCall.enqueue(new Callback<UPDATE_SESSION_MSG>() {
                                    @Override
                                    public void onResponse(Call<UPDATE_SESSION_MSG> call, Response<UPDATE_SESSION_MSG> response) {
                                        progressDialog.dismiss();
                                        if (response.isSuccessful()){
                                            startActivity(new Intent(context, Login.class));
                                            finishAffinity();
                                            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                                        }
                                        else{
                                            Toast.makeText(context,response.message(),Toast.LENGTH_LONG).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<UPDATE_SESSION_MSG> call, Throwable t) {
                                        progressDialog.dismiss();
                                        Toast.makeText(context,t.getMessage(),Toast.LENGTH_LONG).show();
                                    }
                                });
                            }

                            // stopService(new Intent(Major.this,NotificationService.class));
                            break;

                    }

                    return false;
                }
            });

     //   }
    }




}