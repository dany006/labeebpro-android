package com.thakaa360.labeebpro.utilities;

public class Constants {

    public final static  String Base_Url_9000 ="http://52.221.159.42:9000/api/";
    public static final String IMAGE_BASE_URL = "http://52.221.159.42:9000/api/";
    public final static  String PROFILE_Url="http://52.221.159.42:5000/api/";
    public static final String REQ_IMAGE_BASE_URL = "http://52.221.159.42:9000/api";

        /* Login */
    public final static int CHECK_CREDIENTIALS_STATUS = 222;
    public final static int SUCCESSFUL_STATUS = 207;
    public final static int ROLE_CONFLICTION_STATUS = 249;

    /*Database Intializer*/
    public final static String DB_ID = "id";
    public final static String DB_USER_CREDIENTIALS = "user_credientials";
    public final static String DB_USER_ID = "user_id";
    public final static String DB_FCM_TOKEN = "token";
    public final static String DB_TABLE = "profile";
    public final static String DB_ROLE = "user_role";
    public final static String DB_NAME = "labeeb_pro.db";
    public final static String DB_SESSION_STAT = "session_code";
    public final static String GET_PROFILE = "SELECT * FROM "+DB_TABLE;

    public final static String  createTable = "CREATE TABLE IF NOT EXISTS " +
            DB_TABLE+" ( " +
            DB_ID+" INTEGER PRIMARY KEY AUTOINCREMENT," +
            DB_USER_CREDIENTIALS+" VARCHAR NOT NULL," +
            DB_USER_ID+" INTEGER NOT NULL," +
            DB_ROLE+" INTEGER NOT NULL," +
            DB_FCM_TOKEN+" VARCHAR NOT NULL," +
            DB_SESSION_STAT+" VARCHAR NOT NULL);";

    public final static String REQUEST_MODAL_DATA = "modal_DATA";
    public final static String REQ_Audio_BASE_URL = "http://52.221.159.42:9000/api";

    public final static String NOTIFICATION_MODAL = "notificationModal";
    public final static String NOTSTATUS = "notestatus";
    public final static String NOTIFICATION_TRIGGER = "NOTIFICATION_TRIGGER";

    public final static String AGENT_TRACKING_DATA = "AGENT_TRACKING_DATA";
    public final static String AGENT_TRACKING_STATUS = "AGENT_TRACKING_STATUS";

    public  final static String AGENT_ID = "AGENT_ID";

    public final static String REQ_LAT = "REQ_LAT";
    public final static String REQ_LNG = "REQ_LNG";

    public final static String INTO_STATUS = "INTO_STATUS";

    public final static String SERVICE_TEMP_MODAL= "SERVICE_TEMP_MODAL";
    public final static String SERVICE_TEMP_STATUS= "SERVICE_TEMP_STATUS";
    public final static String AUTHORIZATION_KEY = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjNkNWU2YWU4NTgxNGU2NTlmZDQ1NGY4ZWZkZTE5NGJhNWM2ZWE2YzFkMjU1ZmVmM2IzYmJhY2FhYjY0MjBiZTYwZjcxMDI5YTIwYmE1NGNhIn0.eyJhdWQiOiI0IiwianRpIjoiM2Q1ZTZhZTg1ODE0ZTY1OWZkNDU0ZjhlZmRlMTk0YmE1YzZlYTZjMWQyNTVmZWYzYjNiYmFjYWFiNjQyMGJlNjBmNzEwMjlhMjBiYTU0Y2EiLCJpYXQiOjE1MjYxOTg3NjIsIm5iZiI6MTUyNjE5ODc2MiwiZXhwIjoxNTU3NzM0NzYyLCJzdWIiOiIyMTYiLCJzY29wZXMiOlsiKiJdfQ.DbGR7goKtvZjlVeSOYcqse9FAox5sSJBBkU9J6QjuJNnsyaGHaRLgFGaEPMU-xoUBF22Vm30-zEIj5M7MXHe3juiYqhCRGJv0R03CJu3WHKF3zWs5UpBBO_-QsVH7yaj4dgM-BCcK_TKqwFwzJ9gmyh-rx-s8pbLItKv0QLYnNfd6ivCDAzHhT3Vt3KMmdvCR_jeGtp3KZEccgArMe3sWDv6U7CwSvSnkeeKLq-JUrAVJQJjCoeX8Ntepx9Xf3sQo2bmXpP_zZaR0Q-rWLiLK-EBD_3QDkvTdwio61uw8B4xn4t6z_HclfRK-lQzbWdeBFPBHWLIWHiwCG_vlhdnhQFtX1Fq3pb723xBLL8cE9xpjpULE0GZAo9YDPl0gaZCsnmemu3H-A_ZJGPxj2I0q2SL-dApbjzTyS5u6heUIORdqoAYBsDi4EdBbiII3JRjvTjRcbJpLj7fk8p-HxN8Dl6SX-H-F9EvdcVNr4ZPVxBB1OUME9nTPGRjesSI5lqx4c2iMzEHwE1HoEpVGNR4UopgOeuTWCTQ5e0u28J8jx-i7i257GuIcIS8yFK9ikWhebEvPS9vO8CixY6jCYgdds4UbMWW_RaR05n2DARf5XTSR8usf4O4ID5K2ZQeOn1auaQmJ3X1KNMdt05F2AvW9xOibzUj7AmV4_PlvMk2BdA";

    public final static String SERVICE_LOCATION_STATUS = "SERVICE_LOCATION_STATUS";

    public final static String NOTIFICATION_STATUS_FCM = "NOTIFICATION_STATUS_FCM";
    public final static String NOTIFICATION_STATUS_ID = "NOTIFICATION_STATUS_ID";

    /*Notification Details*/
    public final static String REQUEST_ID = "REQ_ID";
    public final static String REQUEST_CAT_ID = "REQ_CAT_ID";
    public final static String REQUEST_SUBCAT_ID = "REQ_SUBCAT_ID";
    public final static String REQUEST_CAT_TITLE = "REQUEST_CAT_TITLE";
    public final static String REQUEST_SUBCAT_TITLE = "REQUEST_SUBCAT_TITLE";
    public final static String REQUEST_LAT = "REQUEST_LAT";
    public final static String REQUEST_LON = "REQUEST_LON";
    public final static String REQUEST_AVATAR = "REQUEST_AVATAR";
    public final static String REQUEST_TIME = "REQUEST_TIME";
    public final static String REQUEST_VOICE = "REQUEST_VOICE";
    public final static String REQUEST_STATUS = "REQUEST_STATUS";
    public final static String REQUEST_CAT_AVATAR = "REQUEST_CAT_AVATAR";

    public final static String REQUEST_NOT_STATUS = "REQUEST_NOT_STATUS";

    public final static String REQUEST_CUSNAME = "REQUEST_CUSNAME";
    public final static String REQUEST_CUSAVATAR = "REQUEST_CUSAVATAR";
    public final static String REQUEST_CUSNUMBER = "REQUEST_CUSNUMBER";
    public final static String REQUEST_COMNAME = "REQUEST_COMNAME";
    public final static String REQUEST_COMAVATAR = "REQUEST_COMAVATAR";
    public final static String REQUEST_COMNUMBER = "REQUEST_COMNUMBER";
    public final static String REQUEST_AGENTNAME = "REQUEST_AGENTNAME";
    public final static String REQUEST_AGENTAVATAR = "REQUEST_AGENTAVATAR";
    public final static String REQUEST_AGENTNUMBER = "REQUEST_AGENTNUMBER";
    public final static String SP_ID = "SP_ID";
    public final static String REQUEST_ADDRESS ="REQUEST_ADDRESS";

    public final static String REQUEST_FINAL_BID = "REQUEST_FINAL_BID";

    public final static String FULL_SCREEN_IMAGE = "FULL_SCREEN_IMAGE";


}
