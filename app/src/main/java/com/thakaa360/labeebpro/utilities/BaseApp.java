package com.thakaa360.labeebpro.utilities;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;

import com.zplesac.connectionbuddy.ConnectionBuddy;
import com.zplesac.connectionbuddy.ConnectionBuddyConfiguration;

import java.util.Locale;

import io.paperdb.Paper;

public class BaseApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        ConnectionBuddyConfiguration networkInspectorConfiguration = new ConnectionBuddyConfiguration.Builder(this).build();
        ConnectionBuddy.getInstance().init(networkInspectorConfiguration);

    }




}
