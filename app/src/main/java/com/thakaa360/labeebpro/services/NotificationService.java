package com.thakaa360.labeebpro.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.google.gson.Gson;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.SubscriptionEventListener;
import com.thakaa360.labeebpro.AgentDashboard;
import com.thakaa360.labeebpro.R;
import com.thakaa360.labeebpro.activities.CompanyDashboard;
import com.thakaa360.labeebpro.modals.NOT_MODAL;
import com.thakaa360.labeebpro.utilities.Constants;

import java.util.ArrayList;
import java.util.Date;

public class NotificationService extends Service {
    SQLiteDatabase sqLiteDatabase;
    Pusher pusher;
    NOT_MODAL msg;
    String Customer_Name ;
    ArrayList<String> cus_name  = new ArrayList<>();
    NotificationManager mNotifyMgr;
    NotificationCompat.Builder mBuilder;
    Intent resultIntent;
    Bundle requestData;
    int customer_id = 0;
    int company_id = 0;
    int agent_id = 0;
    String customer_number = null;
    String customer_name = null;
    String cus_img = null;

    double agent_lat = 0.0;
    double agent_lon = 0.0;
    int role_type;
    int session;

    String final_bid = null;
    SharedPreferences sharedPreferences;
        Cursor cursor;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (getDatabasePath(Constants.DB_NAME).exists()) {
            sqLiteDatabase = openOrCreateDatabase(Constants.DB_NAME, MODE_PRIVATE, null);
            cursor = sqLiteDatabase.rawQuery(Constants.GET_PROFILE, null);
            cursor.moveToFirst();
            company_id = cursor.getInt(cursor.getColumnIndex(Constants.DB_USER_ID));
            role_type = cursor.getInt(cursor.getColumnIndex(Constants.DB_ROLE));
            session = cursor.getInt(cursor.getColumnIndex(Constants.DB_SESSION_STAT));

            sharedPreferences = this.getSharedPreferences(
                    "agent_work_status",
                    MODE_PRIVATE
            );


            PusherOptions options = new PusherOptions();
            options.setCluster("ap2");
            pusher = new Pusher("e5353a3054d6415263d0", options);
            pusher.connect();
            Channel channel = pusher.subscribe("note-channel");

            channel.bind("note-event", new SubscriptionEventListener() {
                @Override
                public void onEvent(String channelName, String eventName, final String data) {

                    if (session == 1) {
                        Gson gson = new Gson();
                        msg = gson.fromJson(data, NOT_MODAL.class);
                        if (msg.getRole() == 2  || msg.getRole() == 1 ) {
                            resultIntent = new Intent(getApplicationContext(), CompanyDashboard.class);
                            requestData = new Bundle();
                        } else if (msg.getRole() == 3) {
                            resultIntent = new Intent(getApplicationContext(), AgentDashboard.class);
                            requestData = new Bundle();
                            agent_id = msg.getNotification().getAgent_id();
                        }

                        if (msg.getRequest().getCus_number() != null) {
                            customer_number = msg.getRequest().getCus_number();
                        }
                        if (msg.getRequest().getCus_name() != null) {
                            customer_name = msg.getRequest().getCus_name();
                        }
                        if (msg.getRequest().getCus_avatar() != null) {
                            cus_img = msg.getRequest().getCus_avatar();
                        }

                        agent_lat = msg.getRequest().getAgent_lat();

                        agent_lon = msg.getRequest().getAgent_lon();


                        int status = msg.getNotification().getNote_status();

                        switch (status) {
                            case 2000:
                                //resultIntent = new Intent(getApplicationContext(),UserDashboard.class);
                                break;
                            case 2001:
                                //resultIntent = new Intent(getApplicationContext(),AgentDashboard.class);
                                break;
                            case 2002:
                        /*requestData = new Bundle();
                        requestData.putInt("agent_id",msg.getRequest().getAgent_id());
                        resultIntent = new Intent(getApplicationContext(), Customer_SPAgent_Profile.class);
                        resultIntent.putExtras(requestData);
                        */
                                break;
                            case 2003:
                        /*resultIntent = new Intent(getApplicationContext(), Selected_Task.class);
                        requestData = new Bundle();
                        requestData.putInt("request_id", msg.getRequest().getId());
                        requestData.putString("request_title", msg.getRequest().getTitle());
                        requestData.putInt("request_type", msg.getRequest().getRequest_type());
                        requestData.putString("request_category", msg.getRequest().getThiscat_title());
                        requestData.putString("request_subcategory", msg.getRequest().getSubcat_title());
                        requestData.putInt("request_sub_catID",msg.getRequest().getThisubcat_id());
                        requestData.putString("request_img", msg.getRequest().getAvatar());
                        requestData.putString("request_description", msg.getRequest().getDescription());
                        requestData.putDouble("request_lat", msg.getRequest().getLat());
                        requestData.putDouble("request_lon", msg.getRequest().getLon());
                        requestData.putString("request_date", msg.getRequest().getCreated_at());
                        requestData.putString("request_cutomer_name", customer_name);
                        requestData.putString("cus_num",customer_number);
                        requestData.putString("cus_img",cus_img);
                        requestData.putInt("company_id", company_id);

                        final_bid = msg.getRequest().getFinal_bid()+"";
                        requestData.putString("final_bid",final_bid);
                        requestData.putDouble("agent_lat",msg.getRequest().getAgent_lat());
                        requestData.putDouble("agent_lng",msg.getRequest().getAgent_lon());
//                        Toast.makeText(getApplicationContext(),msg.getRequest().getAgent_lat()+" "+msg.getRequest().getAgent_lon(),Toast.LENGTH_LONG).show();
                        requestData.putInt("agent_id",msg.getRequest().getAgent_id());
                        resultIntent.putExtras(requestData);


*/
                                break;
                            case 2004:
    /*                    resultIntent = new Intent(getApplicationContext(), Selected_Task.class);
                        requestData = new Bundle();
                        requestData.putInt("request_id", msg.getRequest().getId());
                        requestData.putString("request_title", msg.getRequest().getTitle());
                        requestData.putInt("request_type", msg.getRequest().getRequest_type());
                        requestData.putString("request_category", msg.getRequest().getThiscat_title());
                        requestData.putString("request_subcategory", msg.getRequest().getSubcat_title());
                        requestData.putInt("request_sub_catID",msg.getRequest().getThisubcat_id());
                        requestData.putString("request_img", msg.getRequest().getAvatar());
                        requestData.putString("request_description", msg.getRequest().getDescription());
                        requestData.putDouble("request_lat", msg.getRequest().getLat());
                        requestData.putDouble("request_lon", msg.getRequest().getLon());
                        requestData.putString("request_date", msg.getRequest().getCreated_at());
                        requestData.putString("request_cutomer_name", customer_name);
                        requestData.putString("cus_num",customer_number);
                        requestData.putString("cus_img",cus_img);
                        requestData.putInt("company_id", company_id);
                        resultIntent.putExtras(requestData);
    */
                                break;
                            case 2005:
        /*                resultIntent = new Intent(getApplicationContext(), Selected_Task.class);
                        requestData = new Bundle();
                        requestData.putInt("request_id", msg.getRequest().getId());
                        requestData.putString("request_title", msg.getRequest().getTitle());
                        requestData.putInt("request_type", msg.getRequest().getRequest_type());
                        requestData.putString("request_category", msg.getRequest().getThiscat_title());
                        requestData.putString("request_subcategory", msg.getRequest().getSubcat_title());
                        requestData.putInt("request_sub_catID",msg.getRequest().getThisubcat_id());
                        requestData.putString("request_img", msg.getRequest().getAvatar());
                        requestData.putString("request_description", msg.getRequest().getDescription());
                        requestData.putDouble("request_lat", msg.getRequest().getLat());
                        requestData.putDouble("request_lon", msg.getRequest().getLon());
                        requestData.putString("request_date", msg.getRequest().getCreated_at());
                        requestData.putString("request_cutomer_name", customer_name);
                        requestData.putString("cus_num",customer_number);
                        requestData.putString("cus_img",cus_img);
                        requestData.putInt("company_id", company_id);
                        resultIntent.putExtras(requestData);
        */
                                break;
                            case 2006:
            /*            resultIntent = new Intent(getApplicationContext(),ViewAgentsOnMap.class);
                        requestData = new Bundle();
                        requestData.putDouble("request_lat",msg.getRequest().getLat());
                        requestData.putDouble("request_lon",msg.getRequest().getLon());
                        requestData.putInt("company_id",company_id);
                        requestData.putInt("request_subcategory",msg.getRequest().getThisubcat_id());
                        requestData.putInt("status",1);
                        requestData.putInt("request_id",msg.getRequest().getId());
                        resultIntent.putExtras(requestData);
            */
                                break;
                            case 2007:
                /*        resultIntent = new Intent(getApplicationContext(), Customer_Company_Profile.class);
                        requestData = new Bundle();
                        requestData.putInt("company_id",msg.getRequest().getSp_id());
                        resultIntent.putExtras(requestData);
                */
                                break;
                            case 2008:
                    /*    resultIntent = new Intent(getApplicationContext(), Selected_Task.class);
                        requestData = new Bundle();
                        requestData.putInt("request_id", msg.getRequest().getId());
                        requestData.putString("request_title", msg.getRequest().getTitle());
                        requestData.putInt("request_type", msg.getRequest().getRequest_type());
                        requestData.putString("request_category", msg.getRequest().getThiscat_title());
                        requestData.putString("request_subcategory", msg.getRequest().getSubcat_title());
                        requestData.putInt("request_sub_catID",msg.getRequest().getThisubcat_id());
                        requestData.putString("request_img", msg.getRequest().getAvatar());
                        requestData.putString("request_description", msg.getRequest().getDescription());
                        requestData.putDouble("request_lat", msg.getRequest().getLat());
                        requestData.putDouble("request_lon", msg.getRequest().getLon());
                        requestData.putString("request_date", msg.getRequest().getCreated_at());
                        requestData.putString("request_cutomer_name", customer_name);
                        requestData.putString("cus_num",customer_number);
                        requestData.putString("cus_img",cus_img);
                        requestData.putInt("company_id", company_id);
                        resultIntent.putExtras(requestData);
                    */
                                break;
                            case 2009:
                                //resultIntent = new Intent(getApplicationContext(),UserDashboard.class);
                                break;
                            case 2010:
                        /*resultIntent = new Intent(getApplicationContext(), Selected_Task.class);
                        requestData = new Bundle();
                        requestData.putInt("request_id", msg.getRequest().getId());
                        requestData.putString("request_title", msg.getRequest().getTitle());
                        requestData.putInt("request_type", msg.getRequest().getRequest_type());
                        requestData.putString("request_category", msg.getRequest().getThiscat_title());
                        requestData.putString("request_subcategory", msg.getRequest().getSubcat_title());
                        requestData.putInt("total_bid", msg.getRequest().getBid_count());
                        requestData.putInt("request_sub_catID",msg.getRequest().getThisubcat_id());
                        requestData.putString("request_img", msg.getRequest().getAvatar());
                        requestData.putString("request_description", msg.getRequest().getDescription());
                        requestData.putDouble("request_lat", msg.getRequest().getLat());
                        requestData.putDouble("request_lon", msg.getRequest().getLon());
                        requestData.putString("request_date", msg.getRequest().getCreated_at());
                        resultIntent.putExtras(requestData);
                        */
                                break;
                            case 2011:
                        /*requestData = new Bundle();
                        requestData.putInt("agent_id",msg.getRequest().getAgent_id());
                        resultIntent = new Intent(getApplicationContext(), Customer_SPAgent_Profile.class);
                        resultIntent.putExtras(requestData);
                        */
                                break;
                            case 2012:
                                //resultIntent = new Intent(getApplicationContext(),AgentDashboard.class);
                                break;
                            case 2013:
                        /*requestData = new Bundle();
                        requestData.putInt("agent_id",msg.getRequest().getAgent_id());
                        resultIntent = new Intent(getApplicationContext(), Customer_SPAgent_Profile.class);
                        resultIntent.putExtras(requestData);
                        */
                                break;
                            case 2014:
                        /*resultIntent = new Intent(getApplicationContext(), Selected_Task.class);
                        requestData = new Bundle();
                        requestData.putInt("request_id", msg.getRequest().getId());
                        requestData.putString("request_title", msg.getRequest().getTitle());
                        requestData.putInt("request_type", msg.getRequest().getRequest_type());
                        requestData.putString("request_category", msg.getRequest().getThiscat_title());
                        requestData.putString("request_subcategory", msg.getRequest().getSubcat_title());
                        requestData.putInt("request_sub_catID",msg.getRequest().getThisubcat_id());
                        requestData.putString("request_img", msg.getRequest().getAvatar());
                        requestData.putString("request_description", msg.getRequest().getDescription());
                        requestData.putDouble("request_lat", msg.getRequest().getLat());
                        requestData.putDouble("request_lon", msg.getRequest().getLon());
                        requestData.putString("request_date", msg.getRequest().getCreated_at());
                        requestData.putString("request_cutomer_name", customer_name);
                        requestData.putString("cus_num",customer_number);
                        requestData.putString("cus_img",cus_img);
                        requestData.putInt("company_id", company_id);

                        final_bid = msg.getRequest().getFinal_bid()+"";
                        requestData.putString("final_bid",final_bid);
                        requestData.putDouble("agent_lat",agent_lat);
                        requestData.putDouble("agent_lng",agent_lon);
                        requestData.putInt("agent_id",msg.getRequest().getAgent_id());
                        resultIntent.putExtras(requestData);
                        */
                                break;
                            case 2015:
                        /*requestData = new Bundle();
                        requestData.putInt("agent_id",msg.getRequest().getAgent_id());
                        requestData.putDouble("request_lat",msg.getRequest().getLat());
                        requestData.putDouble("request_lng",msg.getRequest().getLon());
                        resultIntent = new Intent(getApplicationContext(),Requested_Agent_On_Map.class);
                        resultIntent.putExtras(requestData);
                        */
                                break;
                            case 2016:
                        /*requestData = new Bundle();
                        requestData.putInt("agent_id",msg.getRequest().getAgent_id());
                        requestData.putDouble("request_lat",msg.getRequest().getLat());
                        requestData.putDouble("request_lng",msg.getRequest().getLon());
                        resultIntent = new Intent(getApplicationContext(),Requested_Agent_On_Map.class);
                        resultIntent.putExtras(requestData);
                        */
                                break;
                            case 2017:
                        /*requestData = new Bundle();
                        requestData.putInt("agent_id",msg.getRequest().getAgent_id());
                        requestData.putDouble("request_lat",msg.getRequest().getLat());
                        requestData.putDouble("request_lng",msg.getRequest().getLon());
                        resultIntent = new Intent(getApplicationContext(),Requested_Agent_On_Map.class);
                        resultIntent.putExtras(requestData);
                        */
                                break;
                            case 2018:
                        /*requestData = new Bundle();
                        requestData.putInt("agent_id",msg.getRequest().getAgent_id());
                        requestData.putDouble("request_lat",msg.getRequest().getLat());
                        requestData.putDouble("request_lng",msg.getRequest().getLon());
                        resultIntent = new Intent(getApplicationContext(),Requested_Agent_On_Map.class);
                        resultIntent.putExtras(requestData);
                        */
                                break;
                            case 2019:
                        /*requestData = new Bundle();
                        requestData.putInt("agent_id",msg.getRequest().getAgent_id());
                        resultIntent = new Intent(getApplicationContext(), Customer_SPAgent_Profile.class);
                        resultIntent.putExtras(requestData);
                        */
                                break;
                            case 2020:
                        /*requestData = new Bundle();
                        requestData.putInt("agent_id",msg.getRequest().getAgent_id());
                        resultIntent = new Intent(getApplicationContext(), Customer_SPAgent_Profile.class);
                        resultIntent.putExtras(requestData);
                        */
                                break;
                            case 2021:
                        /*requestData = new Bundle();
                        requestData.putInt("agent_id",msg.getRequest().getAgent_id());
                        resultIntent = new Intent(getApplicationContext(), Customer_SPAgent_Profile.class);
                        resultIntent.putExtras(requestData);
                        */
                                break;
                            case 2022:
                        /*requestData = new Bundle();
                        requestData.putInt("agent_id",msg.getRequest().getAgent_id());
                        requestData.putInt("company_id",msg.getRequest().getSp_id());
                        requestData.putInt("customer_id",msg.getRequest().getCustomer_id());

                        resultIntent = new Intent(getApplicationContext(),UserDashboard.class);
                        resultIntent.putExtras(requestData);
                        */
                                break;
                            case 2023:
                                break;
                            case 2024:
                                break;
                            case 2025:
                                break;
                            case 2026:
                                break;
                            case 2027:
                                break;
                            case 2028:
                                break;
                            case 2029:
                                break;
                        }


                        PendingIntent resultPendingIntent =
                                PendingIntent.getActivity(
                                        getApplicationContext(),
                                        0,
                                        resultIntent,
                                        PendingIntent.FLAG_UPDATE_CURRENT
                                );

                        final Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
                        mBuilder =
                                new NotificationCompat.Builder(getApplicationContext())
                                        .setSmallIcon(R.mipmap.ic_launcher, 0)
                                        .setContentTitle("Resquest : " + msg.getRequest().getThiscat_title()).
                                        setVibrate(new long[]{1000, 1000})
                                        .setSound(sound).
                                        setDefaults(Notification.DEFAULT_ALL).
                                        setLargeIcon(icon)
                                        .setContentText(msg.getNotification().getContent()).setAutoCancel(true);

                        mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(msg.getNotification().getContent()));

                        mBuilder.setContentIntent(resultPendingIntent);

                        String CHANNEL_ID = "my_channel_02";// The id of the channel.
                        CharSequence name = "Example1";// The user-visible name of the channel.
                        int importance = NotificationManager.IMPORTANCE_HIGH;
                        NotificationChannel mChannel = null;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
                            mBuilder.setChannelId(CHANNEL_ID);
                        }

                        int m = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
                        final int mNotificationId = 001;
// Gets an instance of the NotificationManager service


                        mNotifyMgr =
                                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            mNotifyMgr.createNotificationChannel(mChannel);
                        }
// Builds the notification and issues it.
                        if (msg.getNotification().getCom_id() == company_id
                                &&
                                msg.getRole() == role_type) {
                            mNotifyMgr.notify(m, mBuilder.build());
                        }
                        if (msg.getNotification().getCus_id() == company_id
                                &&
                                msg.getRole() == role_type) {
                            mNotifyMgr.notify(m, mBuilder.build());
                        }
                        if (msg.getNotification().getAgent_id() == company_id
                                &&
                                msg.getRole() == role_type) {
                            mNotifyMgr.notify(m, mBuilder.build());
                        }
                    }
                }
            });
        }
        else{

        }

        return START_STICKY;


    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        sqLiteDatabase.close();
        pusher.disconnect();
        super.onDestroy();
    }

}
