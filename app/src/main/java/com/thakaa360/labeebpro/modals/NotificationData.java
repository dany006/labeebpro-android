package com.thakaa360.labeebpro.modals;

public class NotificationData {
            int req_id;
            String req_avatar;
            int agent_id;
            String updated_at;
            int com_id;
            String created_at;
            int id;
            String avatar;
            int note_status;
            String content;
            int cus_id;
            int status;


            String voice;
            int thisubcat_id;
            String address;
            double lon;
            String thiscat_title;
            String subcat_title;
            int sp_id;
            int thiscat_id;
            int customer_id;
            double lat;

            String cus_name;
            String cus_avatar;
            String cus_number;

            String sp_name;
            String sp_avatar;
            String sp_number;

            String agent_name;
            String agent_avatar;
            String agent_phone;

            String catAvatar;
            String subCatAvatar;

            double final_bid;
            int bid_count;
            float sp_rating;
            float agent_rating;

    public float getSp_rating() {
        return sp_rating;
    }

    public void setSp_rating(float sp_rating) {
        this.sp_rating = sp_rating;
    }

    public float getAgent_rating() {
        return agent_rating;
    }

    public void setAgent_rating(float agent_rating) {
        this.agent_rating = agent_rating;
    }

    public double getFinal_bid() {
        return final_bid;
    }

    public void setFinal_bid(double final_bid) {
        this.final_bid = final_bid;
    }

    public int getBid_count() {
        return bid_count;
    }

    public void setBid_count(int bid_count) {
        this.bid_count = bid_count;
    }

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getCus_avatar() {
        return cus_avatar;
    }

    public void setCus_avatar(String cus_avatar) {
        this.cus_avatar = cus_avatar;
    }

    public String getCus_number() {
        return cus_number;
    }

    public void setCus_number(String cus_number) {
        this.cus_number = cus_number;
    }

    public String getSp_name() {
        return sp_name;
    }

    public void setSp_name(String sp_name) {
        this.sp_name = sp_name;
    }

    public String getSp_avatar() {
        return sp_avatar;
    }

    public void setSp_avatar(String sp_avatar) {
        this.sp_avatar = sp_avatar;
    }

    public String getSp_number() {
        return sp_number;
    }

    public void setSp_number(String sp_number) {
        this.sp_number = sp_number;
    }

    public String getAgent_name() {
        return agent_name;
    }

    public void setAgent_name(String agent_name) {
        this.agent_name = agent_name;
    }

    public String getAgent_avatar() {
        return agent_avatar;
    }

    public void setAgent_avatar(String agent_avatar) {
        this.agent_avatar = agent_avatar;
    }

    public String getAgent_phone() {
        return agent_phone;
    }

    public void setAgent_phone(String agent_phone) {
        this.agent_phone = agent_phone;
    }

    public String getCatAvatar() {
        return catAvatar;
    }

    public void setCatAvatar(String catAvatar) {
        this.catAvatar = catAvatar;
    }

    public String getSubCatAvatar() {
        return subCatAvatar;
    }

    public void setSubCatAvatar(String subCatAvatar) {
        this.subCatAvatar = subCatAvatar;
    }

    public int getReq_id() {
        return req_id;
    }

    public void setReq_id(int req_id) {
        this.req_id = req_id;
    }

    public String getReq_avatar() {
        return req_avatar;
    }

    public void setReq_avatar(String req_avatar) {
        this.req_avatar = req_avatar;
    }

    public int getAgent_id() {
        return agent_id;
    }

    public void setAgent_id(int agent_id) {
        this.agent_id = agent_id;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public int getCom_id() {
        return com_id;
    }

    public void setCom_id(int com_id) {
        this.com_id = com_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getNote_status() {
        return note_status;
    }

    public void setNote_status(int note_status) {
        this.note_status = note_status;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getCus_id() {
        return cus_id;
    }

    public void setCus_id(int cus_id) {
        this.cus_id = cus_id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getVoice() {
        return voice;
    }

    public void setVoice(String voice) {
        this.voice = voice;
    }

    public int getThisubcat_id() {
        return thisubcat_id;
    }

    public void setThisubcat_id(int thisubcat_id) {
        this.thisubcat_id = thisubcat_id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public String getThiscat_title() {
        return thiscat_title;
    }

    public void setThiscat_title(String thiscat_title) {
        this.thiscat_title = thiscat_title;
    }

    public String getSubcat_title() {
        return subcat_title;
    }

    public void setSubcat_title(String subcat_title) {
        this.subcat_title = subcat_title;
    }

    public int getSp_id() {
        return sp_id;
    }

    public void setSp_id(int sp_id) {
        this.sp_id = sp_id;
    }

    public int getThiscat_id() {
        return thiscat_id;
    }

    public void setThiscat_id(int thiscat_id) {
        this.thiscat_id = thiscat_id;
    }

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }
}
