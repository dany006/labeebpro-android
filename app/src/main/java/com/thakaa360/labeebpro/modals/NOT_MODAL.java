package com.thakaa360.labeebpro.modals;

public class NOT_MODAL {
    public class Notification
    {
        public int req_id;
        public String content;
        public String avatar;

        public int getReq_id() {
            return req_id;
        }

        public void setReq_id(int req_id) {
            this.req_id = req_id;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getReq_avatar() {
            return req_avatar;
        }

        public void setReq_avatar(String req_avatar) {
            this.req_avatar = req_avatar;
        }

        public int getCom_id() {
            return com_id;
        }

        public void setCom_id(int com_id) {
            this.com_id = com_id;
        }

        public int getAgent_id() {
            return agent_id;
        }

        public void setAgent_id(int agent_id) {
            this.agent_id = agent_id;
        }

        public int getCus_id() {
            return cus_id;
        }

        public void setCus_id(int cus_id) {
            this.cus_id = cus_id;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public int getNote_status() {
            return note_status;
        }

        public void setNote_status(int note_status) {
            this.note_status = note_status;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String req_avatar;
        public int com_id;
        public int agent_id;
        public int cus_id;
        public int status;
        public int note_status;
        public String updated_at;
        public String created_at;
        public int id;
    }

    public class Request
    {
        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getCustomer_id() {
            return customer_id;
        }

        public void setCustomer_id(int customer_id) {
            this.customer_id = customer_id;
        }

        public int getAgent_id() {
            return agent_id;
        }

        public void setAgent_id(int agent_id) {
            this.agent_id = agent_id;
        }

        public int getSp_id() {
            return sp_id;
        }

        public void setSp_id(int sp_id) {
            this.sp_id = sp_id;
        }

        public int getThiscat_id() {
            return thiscat_id;
        }

        public void setThiscat_id(int thiscat_id) {
            this.thiscat_id = thiscat_id;
        }

        public String getThiscat_title() {
            return thiscat_title;
        }

        public void setThiscat_title(String thiscat_title) {
            this.thiscat_title = thiscat_title;
        }

        public int getThisubcat_id() {
            return thisubcat_id;
        }

        public void setThisubcat_id(int thisubcat_id) {
            this.thisubcat_id = thisubcat_id;
        }

        public String getSubcat_title() {
            return subcat_title;
        }

        public void setSubcat_title(String subcat_title) {
            this.subcat_title = subcat_title;
        }

        public String getSchedual_date() {
            return schedual_date;
        }

        public void setSchedual_date(String schedual_date) {
            this.schedual_date = schedual_date;
        }

        public String getSchedual_time() {
            return schedual_time;
        }

        public void setSchedual_time(String schedual_time) {
            this.schedual_time = schedual_time;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public double getFinal_bid() {
            return final_bid;
        }

        public void setFinal_bid(double final_bid) {
            this.final_bid = final_bid;
        }

        public int getBid_count() {
            return bid_count;
        }

        public void setBid_count(int bid_count) {
            this.bid_count = bid_count;
        }

        public double getLat() {
            return lat;
        }

        public void setLat(double lat) {
            this.lat = lat;
        }

        public double getLon() {
            return lon;
        }

        public void setLon(double lon) {
            this.lon = lon;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getVoice() {
            return voice;
        }

        public void setVoice(String voice) {
            this.voice = voice;
        }

        public String getCompleted_at() {
            return completed_at;
        }

        public void setCompleted_at(String completed_at) {
            this.completed_at = completed_at;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getCus_name() {
            return cus_name;
        }

        public void setCus_name(String cus_name) {
            this.cus_name = cus_name;
        }

        public String getCus_avatar() {
            return cus_avatar;
        }

        public void setCus_avatar(String cus_avatar) {
            this.cus_avatar = cus_avatar;
        }

        public String getCus_number() {
            return cus_number;
        }

        public void setCus_number(String cus_number) {
            this.cus_number = cus_number;
        }

        public double getAgent_lat() {
            return agent_lat;
        }

        public void setAgent_lat(double agent_lat) {
            this.agent_lat = agent_lat;
        }

        public double getAgent_lon() {
            return agent_lon;
        }

        public void setAgent_lon(double agent_lon) {
            this.agent_lon = agent_lon;
        }

        public int id;
        public int customer_id;
        public int agent_id;
        public int sp_id;
        public int thiscat_id;
        public String thiscat_title;
        public int thisubcat_id;
        public String subcat_title;
        public String schedual_date;
        public String schedual_time;
        public String avatar;
        public double final_bid;
        public int bid_count;
        public double lat;
        public double lon;
        public String address;
        public String voice;
        public String completed_at;
        public String created_at;
        public String updated_at;
        public int status;
        public String cus_name;
        public String cus_avatar;
        public String cus_number;
        public double agent_lat;
        public double agent_lon;
    }


        public Notification notification;
        public int role;

    public Notification getNotification() {
        return notification;
    }

    public void setNotification(Notification notification) {
        this.notification = notification;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public int counter;
        public Request request;

}
