package com.thakaa360.labeebpro.server.modals;

public class UPDATE_SESSION_MSG {
    public String msg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
