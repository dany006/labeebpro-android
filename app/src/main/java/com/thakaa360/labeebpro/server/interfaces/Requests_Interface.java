package com.thakaa360.labeebpro.server.interfaces;

import com.thakaa360.labeebpro.server.modals.AGENTS_TASKS;
import com.thakaa360.labeebpro.server.modals.AGENT_HISTORY;
import com.thakaa360.labeebpro.server.modals.COMPANY_REQUEST_HISTORY_MODAL;
import com.thakaa360.labeebpro.server.modals.INPROGRESS_TASKS_MODAL;
import com.thakaa360.labeebpro.server.modals.PENDING_TASKS_MODAL;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Requests_Interface {

    @GET("getpendingrequest/{id}")
    public Call<List<PENDING_TASKS_MODAL>> get_Companyimmediate_requests(@Path("id") int id);

    @GET("getinprogressrequest/{id}")
    public Call<List<INPROGRESS_TASKS_MODAL>> get_CompanyInProgressTasks(@Path("id") int id);

    @GET("getagentpendingrequest/{id}")
    public Call<List<AGENTS_TASKS>> get_AgentPendingTasks(@Path("id") int id);

    @GET("getagentinprogressrequest/{id}")
    public Call<List<AGENTS_TASKS>> get_AgentInProgressTasks(@Path("id") int id);


    @FormUrlEncoded
    @POST("comhistory")
    Call<List<COMPANY_REQUEST_HISTORY_MODAL>> getCompanyTaskHistory(
            @Field("com_id") int com_id
    );

    @GET("agenthistory")
    Call<List<AGENT_HISTORY>> getAgentHistory(
            @Query("agent_id") int agent_id
    );
}
