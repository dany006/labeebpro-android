package com.thakaa360.labeebpro.server.interfaces;

import com.thakaa360.labeebpro.server.modals.AGENT_DATA_MODAL;
import com.thakaa360.labeebpro.server.modals.COMPANY_DATA_MODAL;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface UserData_Interface {

    /*Company Section*/

    @Multipart
    @POST("getcomdata")
    Call<List<COMPANY_DATA_MODAL>> getCompanyDataById(
            @Part("id")RequestBody id
            );
    @Multipart
    @POST("getcomdata")
    Call<List<COMPANY_DATA_MODAL>> getCompanyDataByNumber(
            @Part("number")RequestBody number
    );
    @Multipart
    @POST("getcomdata")
    Call<List<COMPANY_DATA_MODAL>> getCompanyDataByEmail(
            @Part("email")RequestBody email
    );

    /*Agent Section*/

    @Multipart
    @POST("getagentdata")
    Call<List<AGENT_DATA_MODAL>> getAgentById(
            @Part("id")RequestBody id
    );
    @Multipart
    @POST("getagentdata")
    Call<List<AGENT_DATA_MODAL>> getAgentByNumber(
            @Part("number")RequestBody number
    );
    @Multipart
    @POST("getagentdata")
    Call<List<AGENT_DATA_MODAL>> getAgentByEmail(
            @Part("email")RequestBody email
    );

}
