package com.thakaa360.labeebpro.server.interfaces;

import com.thakaa360.labeebpro.server.modals.DirectionParser;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Dany on 11/14/2017.
 */

public interface InterfaceMap {

    @GET("maps/api/directions/json")
    Call<DirectionParser> getData(
            @Query("origin") String origin,
            @Query("destination") String destination,
            @Query("key") String key
    );
}
