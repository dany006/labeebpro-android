package com.thakaa360.labeebpro.server.modals;

public class MSG_MODAL {
    public String msg;
    public int status;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
