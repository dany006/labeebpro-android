package com.thakaa360.labeebpro.server.interfaces;

import com.thakaa360.labeebpro.server.modals.MSG_MODAL;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface Agent_Job_Interface {
    @Multipart
    @POST("updateagentstatus")
    Call<MSG_MODAL> update_agent_status(
            @Part("req_id") RequestBody req_id,
            @Part("req_status") RequestBody req_status,
            @Part("avail_status") RequestBody avail_status);
}
