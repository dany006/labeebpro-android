package com.thakaa360.labeebpro.server.interfaces;

import com.thakaa360.labeebpro.server.modals.MSG_MODAL;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by Dany on 12/6/2017.
 */

public interface LocationUpdateInterface {
    @Multipart
    @POST("pusheragent")
    Call<MSG_MODAL  > putAgentLocation(
            @Part("id") RequestBody id,
            @Part("lat") RequestBody lat,
            @Part("lon") RequestBody lon
    );
}
