package com.thakaa360.labeebpro.server.client;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.thakaa360.labeebpro.utilities.Constants.Base_Url_9000;


/**
 * Created by DanyRoyal on 9/25/2017.
 */

public class ApiClient {

    static Retrofit retrofit = null;


    public static Retrofit getApiClient(){

        if(retrofit == null){
            retrofit = new Retrofit.Builder().baseUrl(Base_Url_9000).
                    addConverterFactory(GsonConverterFactory.create()).build();
        }
        return  retrofit;

    }

}
