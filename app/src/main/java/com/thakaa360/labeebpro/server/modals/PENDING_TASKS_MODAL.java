package com.thakaa360.labeebpro.server.modals;

public class PENDING_TASKS_MODAL {
    public int id;
    public int customer_id;
    public int agent_id;
    public int sp_id;
    public int thiscat_id;
    public String thiscat_title;
    public int thisubcat_id;
    public String subcat_title;
    public String schedual_date;
    public String schedual_time;
    public String avatar;
    public double final_bid;
    public int bid_count;
    public double lat;
    public double lon;
    public String address;
    public String voice;
    public String completed_at;
    public String completed_time;
    public String created_at;
    public String updated_at;
    public int status;
    public String cus_avatar;
    public String cus_name;
    public String cus_number;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public int getAgent_id() {
        return agent_id;
    }

    public void setAgent_id(int agent_id) {
        this.agent_id = agent_id;
    }

    public int getSp_id() {
        return sp_id;
    }

    public void setSp_id(int sp_id) {
        this.sp_id = sp_id;
    }

    public int getThiscat_id() {
        return thiscat_id;
    }

    public void setThiscat_id(int thiscat_id) {
        this.thiscat_id = thiscat_id;
    }

    public String getThiscat_title() {
        return thiscat_title;
    }

    public void setThiscat_title(String thiscat_title) {
        this.thiscat_title = thiscat_title;
    }

    public int getThisubcat_id() {
        return thisubcat_id;
    }

    public void setThisubcat_id(int thisubcat_id) {
        this.thisubcat_id = thisubcat_id;
    }

    public String getSubcat_title() {
        return subcat_title;
    }

    public void setSubcat_title(String subcat_title) {
        this.subcat_title = subcat_title;
    }

    public String getSchedual_date() {
        return schedual_date;
    }

    public void setSchedual_date(String schedual_date) {
        this.schedual_date = schedual_date;
    }

    public String getSchedual_time() {
        return schedual_time;
    }

    public void setSchedual_time(String schedual_time) {
        this.schedual_time = schedual_time;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public double getFinal_bid() {
        return final_bid;
    }

    public void setFinal_bid(double final_bid) {
        this.final_bid = final_bid;
    }

    public int getBid_count() {
        return bid_count;
    }

    public void setBid_count(int bid_count) {
        this.bid_count = bid_count;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getVoice() {
        return voice;
    }

    public void setVoice(String voice) {
        this.voice = voice;
    }

    public String getCompleted_at() {
        return completed_at;
    }

    public void setCompleted_at(String completed_at) {
        this.completed_at = completed_at;
    }

    public String getCompleted_time() {
        return completed_time;
    }

    public void setCompleted_time(String completed_time) {
        this.completed_time = completed_time;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCus_avatar() {
        return cus_avatar;
    }

    public void setCus_avatar(String cus_avatar) {
        this.cus_avatar = cus_avatar;
    }

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getCus_number() {
        return cus_number;
    }

    public void setCus_number(String cus_number) {
        this.cus_number = cus_number;
    }

    public String getAgent_name() {
        return agent_name;
    }

    public void setAgent_name(String agent_name) {
        this.agent_name = agent_name;
    }

    public String getAgent_avatar() {
        return agent_avatar;
    }

    public void setAgent_avatar(String agent_avatar) {
        this.agent_avatar = agent_avatar;
    }

    public String getCat_avatar() {
        return cat_avatar;
    }

    public void setCat_avatar(String cat_avatar) {
        this.cat_avatar = cat_avatar;
    }

    public String getSubcat_avatar() {
        return subcat_avatar;
    }

    public void setSubcat_avatar(String subcat_avatar) {
        this.subcat_avatar = subcat_avatar;
    }

    public String agent_name;
    public String agent_avatar;
    public String cat_avatar;
    public String subcat_avatar;
}
