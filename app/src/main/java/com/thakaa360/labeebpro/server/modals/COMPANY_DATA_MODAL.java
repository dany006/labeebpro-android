package com.thakaa360.labeebpro.server.modals;

public class COMPANY_DATA_MODAL {
    public int id;
    public String email;
    public String number;
    public String avatar;
    public int session_code;
    public String remember_token;
    public String password;
    public String permissions;
    public String last_login;
    public String name;
    public String created_at;
    public String updated_at;
    public int device_id;
    public int device_type;
    public int company_id;
    public String company_name;
    public String company_address;
    public String company_bio;
    public double company_lat;
    public double company_lon;
    public float company_rating;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getSession_code() {
        return session_code;
    }

    public void setSession_code(int session_code) {
        this.session_code = session_code;
    }

    public String getRemember_token() {
        return remember_token;
    }

    public void setRemember_token(String remember_token) {
        this.remember_token = remember_token;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPermissions() {
        return permissions;
    }

    public void setPermissions(String permissions) {
        this.permissions = permissions;
    }

    public String getLast_login() {
        return last_login;
    }

    public void setLast_login(String last_login) {
        this.last_login = last_login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public int getDevice_id() {
        return device_id;
    }

    public void setDevice_id(int device_id) {
        this.device_id = device_id;
    }

    public int getDevice_type() {
        return device_type;
    }

    public void setDevice_type(int device_type) {
        this.device_type = device_type;
    }

    public int getCompany_id() {
        return company_id;
    }

    public void setCompany_id(int company_id) {
        this.company_id = company_id;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getCompany_address() {
        return company_address;
    }

    public void setCompany_address(String company_address) {
        this.company_address = company_address;
    }

    public String getCompany_bio() {
        return company_bio;
    }

    public void setCompany_bio(String company_bio) {
        this.company_bio = company_bio;
    }

    public double getCompany_lat() {
        return company_lat;
    }

    public void setCompany_lat(double company_lat) {
        this.company_lat = company_lat;
    }

    public double getCompany_lon() {
        return company_lon;
    }

    public void setCompany_lon(double company_lon) {
        this.company_lon = company_lon;
    }

    public float getCompany_rating() {
        return company_rating;
    }

    public void setCompany_rating(float company_rating) {
        this.company_rating = company_rating;
    }

    public int getCompany_completed_req() {
        return company_completed_req;
    }

    public void setCompany_completed_req(int company_completed_req) {
        this.company_completed_req = company_completed_req;
    }

    public int getCompany_status() {
        return company_status;
    }

    public void setCompany_status(int company_status) {
        this.company_status = company_status;
    }

    public int company_completed_req;
    public int company_status;
}
