package com.thakaa360.labeebpro.server.interfaces;

import com.thakaa360.labeebpro.server.modals.SESSION_CODE_MODAL;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface Get_Status {
    @Multipart
    @POST("getstatus")
    Call<SESSION_CODE_MODAL> getStatusByEmail(
            @Part("email")  RequestBody email,
            @Part("role_id")    RequestBody role_id
            );

    @Multipart
    @POST("getstatus")
    Call<SESSION_CODE_MODAL> getStatusByNumber(
            @Part("number")  RequestBody number,
            @Part("role_id")    RequestBody role_id
    );
}
