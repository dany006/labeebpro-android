package com.thakaa360.labeebpro.server.modals;

public class AGENT_DATA_MODAL {
    public int id;
    public String email;
    public String number;
    public String avatar;
    public int session_code;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getSession_code() {
        return session_code;
    }

    public void setSession_code(int session_code) {
        this.session_code = session_code;
    }

    public String getRemember_token() {
        return remember_token;
    }

    public void setRemember_token(String remember_token) {
        this.remember_token = remember_token;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPermissions() {
        return permissions;
    }

    public void setPermissions(String permissions) {
        this.permissions = permissions;
    }

    public String getLast_login() {
        return last_login;
    }

    public void setLast_login(String last_login) {
        this.last_login = last_login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public int getDevice_id() {
        return device_id;
    }

    public void setDevice_id(int device_id) {
        this.device_id = device_id;
    }

    public int getDevice_type() {
        return device_type;
    }

    public void setDevice_type(int device_type) {
        this.device_type = device_type;
    }

    public int getAgent_id() {
        return agent_id;
    }

    public void setAgent_id(int agent_id) {
        this.agent_id = agent_id;
    }

    public String getAgent_name() {
        return agent_name;
    }

    public void setAgent_name(String agent_name) {
        this.agent_name = agent_name;
    }

    public String getAgent_address() {
        return agent_address;
    }

    public void setAgent_address(String agent_address) {
        this.agent_address = agent_address;
    }

    public String getAgent_bio() {
        return agent_bio;
    }

    public void setAgent_bio(String agent_bio) {
        this.agent_bio = agent_bio;
    }

    public double getAgent_lat() {
        return agent_lat;
    }

    public void setAgent_lat(double agent_lat) {
        this.agent_lat = agent_lat;
    }

    public double getAgent_lon() {
        return agent_lon;
    }

    public void setAgent_lon(double agent_lon) {
        this.agent_lon = agent_lon;
    }

    public float getAgent_rating() {
        return agent_rating;
    }

    public void setAgent_rating(float agent_rating) {
        this.agent_rating = agent_rating;
    }

    public int getAgent_completed_req() {
        return agent_completed_req;
    }

    public void setAgent_completed_req(int agent_completed_req) {
        this.agent_completed_req = agent_completed_req;
    }

    public int getAgent_status() {
        return agent_status;
    }

    public void setAgent_status(int agent_status) {
        this.agent_status = agent_status;
    }

    public String remember_token;
    public String password;
    public String permissions;
    public String last_login;
    public String name;
    public String created_at;
    public String updated_at;
    public int device_id;
    public int device_type;
    public int agent_id;
    public String agent_name;
    public String agent_address;
    public String agent_bio;
    public double agent_lat;
    public double agent_lon;
    public float agent_rating;
    public int agent_completed_req;
    public int agent_status;
}
