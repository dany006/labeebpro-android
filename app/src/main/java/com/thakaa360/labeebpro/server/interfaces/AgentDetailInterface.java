package com.thakaa360.labeebpro.server.interfaces;


import com.thakaa360.labeebpro.server.modals.AGENT_DETAILS;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface AgentDetailInterface {
    @GET("agentprofile")
    Call<AGENT_DETAILS> getAgentDetail(
            @Header("Authorization") String auth,
            @Query("agentID") int agentID
    );
}
