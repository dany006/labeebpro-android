package com.thakaa360.labeebpro.server.interfaces;

import com.thakaa360.labeebpro.server.modals.MSG_MODAL;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface RespondCustomer {
    @FormUrlEncoded
    @POST("createbid")
    Call<MSG_MODAL> createBid(
           @Field("req_id") int req_id,
           @Field("company_id") int company_id,
           @Field("bid_amount") double  bid_amount,
           @Field("bid_type") int bid_type
    );

    @FormUrlEncoded
    @POST("createbid")
    Call<MSG_MODAL> update_Bid(
            @Field("req_id") int req_id,
            @Field("agent_id") int company_id,
            @Field("bid_amount") double  bid_amount,
            @Field("bid_type") int bid_type
    );
}
