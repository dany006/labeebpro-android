package com.thakaa360.labeebpro.server.interfaces;

import com.thakaa360.labeebpro.server.modals.MSG_MODAL;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface Assign_Agent_Interface {
    @Multipart
    @POST("asignagent")
    Call<MSG_MODAL> assignAgent(
                @Part("req_id") RequestBody req_id,
                @Part("status") RequestBody status,
                @Part("agent_id") RequestBody agent_id
            );
}
