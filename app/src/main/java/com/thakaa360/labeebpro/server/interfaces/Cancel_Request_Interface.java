package com.thakaa360.labeebpro.server.interfaces;

import com.thakaa360.labeebpro.server.modals.MSG_MODAL;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface Cancel_Request_Interface {
    @Multipart
    @POST("companycancelrequest")
    Call<MSG_MODAL> cancelReq(
            @Part("com_id")RequestBody com_id,
            @Part("req_id") RequestBody reqID
            );

}
