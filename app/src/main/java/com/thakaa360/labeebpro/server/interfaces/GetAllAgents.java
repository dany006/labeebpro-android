package com.thakaa360.labeebpro.server.interfaces;

import com.thakaa360.labeebpro.server.modals.AGENT_MODAL_FOR_ASSIGNING;
import com.thakaa360.labeebpro.server.modals.ALL_AGENTS_MODAL;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by DanyRoyal on 11/16/2017.
 */

public interface GetAllAgents {
    @GET("getallagents/{id}")
    Call<List<ALL_AGENTS_MODAL>> getAgents(@Path("id") int id);

    @Multipart
    @POST("getagentfor")
    Call<List<AGENT_MODAL_FOR_ASSIGNING>> getAgents(
            @Part("lat") RequestBody lat,
            @Part("lon") RequestBody lon,
            @Part("com_id") RequestBody com_id,
            @Part("subcat_id") RequestBody subcat_id);
}
