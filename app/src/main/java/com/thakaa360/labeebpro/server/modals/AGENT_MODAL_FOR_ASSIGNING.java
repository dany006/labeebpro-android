package com.thakaa360.labeebpro.server.modals;

public class AGENT_MODAL_FOR_ASSIGNING {
    public int id;
    public String user_name;
    public int user_id;
    public int company_id;
    public String avatar;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getCompany_id() {
        return company_id;
    }

    public void setCompany_id(int company_id) {
        this.company_id = company_id;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public int getCompleted_req() {
        return completed_req;
    }

    public void setCompleted_req(int completed_req) {
        this.completed_req = completed_req;
    }

    public String getAdded_by() {
        return added_by;
    }

    public void setAdded_by(String added_by) {
        this.added_by = added_by;
    }

    public String getAdded_by_name() {
        return added_by_name;
    }

    public void setAdded_by_name(String added_by_name) {
        this.added_by_name = added_by_name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getAvailble_status() {
        return availble_status;
    }

    public void setAvailble_status(int availble_status) {
        this.availble_status = availble_status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public int getCat_id() {
        return cat_id;
    }

    public void setCat_id(int cat_id) {
        this.cat_id = cat_id;
    }

    public int getComcat_id() {
        return comcat_id;
    }

    public void setComcat_id(int comcat_id) {
        this.comcat_id = comcat_id;
    }

    public int getComsubcat_id() {
        return comsubcat_id;
    }

    public void setComsubcat_id(int comsubcat_id) {
        this.comsubcat_id = comsubcat_id;
    }

    public int getAgent_id() {
        return agent_id;
    }

    public void setAgent_id(int agent_id) {
        this.agent_id = agent_id;
    }

    public int getParent_id() {
        return parent_id;
    }

    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }

    public int getThisubcat_id() {
        return thisubcat_id;
    }

    public void setThisubcat_id(int thisubcat_id) {
        this.thisubcat_id = thisubcat_id;
    }

    public String getSubcat_title() {
        return subcat_title;
    }

    public void setSubcat_title(String subcat_title) {
        this.subcat_title = subcat_title;
    }

    public String bio;
    public String number;
    public String address;
    public double lat;
    public double lon;
    public float rating;
    public int completed_req;
    public String added_by;
    public String added_by_name;
    public int status;
    public int availble_status;
    public String created_at;
    public String updated_at;
    public double distance;
    public int cat_id;
    public int comcat_id;
    public int comsubcat_id;
    public int agent_id;
    public int parent_id;
    public int thisubcat_id;
    public String subcat_title;
}
