package com.thakaa360.labeebpro.server.modals;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AGENT_DETAILS {
    public class AgentProfile
    {
        public double agentLat;
        public double agentLon;

        public double getAgentLat() {
            return agentLat;
        }

        public void setAgentLat(double agentLat) {
            this.agentLat = agentLat;
        }

        public double getAgentLon() {
            return agentLon;
        }

        public void setAgentLon(double agentLon) {
            this.agentLon = agentLon;
        }

        public int agentID;
        public String agentName;
        public String agentAvatarUrl;
        public String agentNumber;

        public int getAgentID() {
            return agentID;
        }

        public void setAgentID(int agentID) {
            this.agentID = agentID;
        }

        public String getAgentName() {
            return agentName;
        }

        public void setAgentName(String agentName) {
            this.agentName = agentName;
        }

        public String getAgentAvatarUrl() {
            return agentAvatarUrl;
        }

        public void setAgentAvatarUrl(String agentAvatarUrl) {
            this.agentAvatarUrl = agentAvatarUrl;
        }

        public String getAgentNumber() {
            return agentNumber;
        }

        public void setAgentNumber(String agentNumber) {
            this.agentNumber = agentNumber;
        }

        public float getAgentRating() {
            return agentRating;
        }

        public void setAgentRating(float agentRating) {
            this.agentRating = agentRating;
        }

        public int getAgentCompletedRequests() {
            return agentCompletedRequests;
        }

        public void setAgentCompletedRequests(int agentCompletedRequests) {
            this.agentCompletedRequests = agentCompletedRequests;
        }

        public float agentRating;
        public int agentCompletedRequests;
    }

    public class Review
    {
        public String reviews;

        public String getReviews() {
            return reviews;
        }

        public void setReviews(String reviews) {
            this.reviews = reviews;
        }

        public float getRating() {
            return rating;
        }

        public void setRating(float rating) {
            this.rating = rating;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getCustomerImageUrl() {
            return customerImageUrl;
        }

        public void setCustomerImageUrl(String customerImageUrl) {
            this.customerImageUrl = customerImageUrl;
        }

        public float rating;
        public String created_at;
        @SerializedName("CustomerImageUrl")
        public String customerImageUrl;
    }


    public List<AgentProfile> getAgentProfile() {
        return agentProfile;
    }

    public void setAgentProfile(List<AgentProfile> agentProfile) {
        this.agentProfile = agentProfile;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    @SerializedName("AgentProfile")
        public List<AgentProfile> agentProfile;
        @SerializedName("Reviews")
        public List<Review> reviews;
    
}
