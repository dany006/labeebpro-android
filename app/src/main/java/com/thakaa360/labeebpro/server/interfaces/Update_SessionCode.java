package com.thakaa360.labeebpro.server.interfaces;

import com.thakaa360.labeebpro.server.modals.UPDATE_SESSION_MSG;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface Update_SessionCode {
    @Multipart
    @POST("updatestatus")
    Call<UPDATE_SESSION_MSG> updateSessionCodeByEmail(
            @Part("email")  RequestBody email,
            @Part("session_code")   RequestBody session_code,
            @Part("role_id")    RequestBody role_id
            );

    @Multipart
    @POST("updatestatus")
    Call<UPDATE_SESSION_MSG> updateSessionCodeByNumber(
            @Part("number")  RequestBody number,
            @Part("session_code")   RequestBody session_code,
            @Part("role_id")    RequestBody role_id
    );
}
