package com.thakaa360.labeebpro.server.interfaces;

import com.thakaa360.labeebpro.server.modals.MSG_MODAL;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface LoginInterface {
    @Multipart
    @POST("comlogin")
    Call<MSG_MODAL> company_loginWithNumber(
            @Part("number") RequestBody number,
            @Part("password") RequestBody password
    );

    @Multipart
    @POST("comlogin")
    Call<MSG_MODAL> company_loginWithEmail(
            @Part("email") RequestBody number,
            @Part("password") RequestBody password
    );

    @Multipart
    @POST("agentlogin")
    Call<MSG_MODAL> agent_loginWithNumber(
            @Part("number") RequestBody number,
            @Part("password") RequestBody password
    );

    @Multipart
    @POST("agentlogin")
    Call<MSG_MODAL> agent_loginWithEmail(
            @Part("email") RequestBody number,
            @Part("password") RequestBody password
    );

}