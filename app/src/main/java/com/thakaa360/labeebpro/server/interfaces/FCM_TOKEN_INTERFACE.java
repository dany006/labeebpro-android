package com.thakaa360.labeebpro.server.interfaces;

import com.thakaa360.labeebpro.server.modals.MSG_MODAL;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface FCM_TOKEN_INTERFACE {
    @Multipart
    @POST("updatetoken")
    Call<MSG_MODAL> updateToken(
            @Part("email") RequestBody number,
            @Part("role_id") RequestBody role,
            @Part("token") RequestBody token
    );
}
