package com.thakaa360.labeebpro;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.github.marlonlom.utilities.timeago.TimeAgo;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.iid.FirebaseInstanceId;
import com.thakaa360.labeebpro.activities.CompanyDashboard;
import com.thakaa360.labeebpro.activities.FullMapsActivity;
import com.thakaa360.labeebpro.activities.RequestOnMap;
import com.thakaa360.labeebpro.activities.ViewAgentsOnMap;
import com.thakaa360.labeebpro.fragments.AgentHistory_Fragment;
import com.thakaa360.labeebpro.fragments.AgentInProgressTasks_Fragment;
import com.thakaa360.labeebpro.fragments.AgentPendingTasks_Fragment;
import com.thakaa360.labeebpro.helpers.BottomNavigationViewHelper;
import com.thakaa360.labeebpro.helpers.Full_Image;
import com.thakaa360.labeebpro.helpers.TimeConverter;
import com.thakaa360.labeebpro.modals.NotificationData;
import com.thakaa360.labeebpro.server.client.ApiClient;
import com.thakaa360.labeebpro.server.interfaces.Agent_Job_Interface;
import com.thakaa360.labeebpro.server.interfaces.LocationUpdateInterface;
import com.thakaa360.labeebpro.server.interfaces.RespondCustomer;
import com.thakaa360.labeebpro.server.modals.AGENTS_TASKS;
import com.thakaa360.labeebpro.server.modals.MSG_MODAL;
import com.thakaa360.labeebpro.utilities.Constants;
import com.thakaa360.labeebpro.utilities.LayoutUtility;
import com.thakaa360.labeebpro.utilities.Major;

import java.io.IOException;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;
import io.paperdb.Paper;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AgentDashboard extends Major {

    Context context;
    Fragment fragment;
    FragmentTransaction transaction;

    private static final String TAG = AgentDashboard.class.getSimpleName();

    // Used in checking for runtime permissions.
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

    // The BroadcastReceiver used to listen from broadcasts from the service.
    private MyReceiver myReceiver;

    // A reference to the service used to get location updates.
    private LocationUpdatesService mService = null;

    // Tracks the bound state of the service.
    private boolean mBound = false;


    SQLiteDatabase sqLiteDatabase;
    Cursor cursor;
    NotificationData notificationData;

    // Monitors the state of the connection to the service.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            LocationUpdatesService.LocalBinder binder = (LocationUpdatesService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            mBound = false;
        }
    };





    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_requests:
                    fragment = AgentPendingTasks_Fragment.newInstance();
                    transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.frame, fragment);
                    transaction.commit();
                    return true;
                case R.id.navigation_tasks:

                    fragment = AgentInProgressTasks_Fragment.newInstance();
                    transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.frame, fragment);
                    transaction.commit();
                    return true;
                case R.id.navigation_history:
                    fragment = AgentHistory_Fragment.newInstance();
                    transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.frame, fragment);
                    transaction.commit();
                    return true;
            }
            return false;
        }
    };
    private int user_id;
    private int service_status = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        notificationData = new NotificationData();
        myReceiver = new MyReceiver();

        context = AgentDashboard.this;
        Paper.init(context);

        service_status = Paper.book().read(Constants.SERVICE_LOCATION_STATUS,0);

        String Token = FirebaseInstanceId.getInstance().getToken();
        Log.v("AgentTok",Token);
        View contentView = LayoutUtility.getLayout(this, R.layout.activity_agent_dashboard);
        mDrawer.addView(contentView, 0);
        navigationView.setCheckedItem(R.id.dashboard_item);

        toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawer.openDrawer(GravityCompat.START);
            }
        });


        sqLiteDatabase = openOrCreateDatabase(Constants.DB_NAME,MODE_PRIVATE,null);
        cursor = sqLiteDatabase.rawQuery(Constants.GET_PROFILE,null);
        cursor.moveToFirst();
        user_id = cursor.getInt(cursor.getColumnIndex(Constants.DB_USER_ID));



        if (getIntent().getExtras() != null){

            notificationData.setAddress(getIntent().getExtras().getString(Constants.REQUEST_ADDRESS));
            notificationData.setNote_status(getIntent().getExtras().getInt(Constants.NOTSTATUS));
            notificationData.setReq_id(getIntent().getExtras().getInt(Constants.REQUEST_ID));

            notificationData.setThiscat_id(getIntent().getExtras().getInt(Constants.REQUEST_CAT_ID));
            notificationData.setThisubcat_id(getIntent().getExtras().getInt(Constants.REQUEST_SUBCAT_ID));

            notificationData.setThiscat_title(getIntent().getExtras().getString(Constants.REQUEST_CAT_TITLE));
            notificationData.setSubcat_title(getIntent().getExtras().getString(Constants.REQUEST_SUBCAT_TITLE));

            notificationData.setAvatar(getIntent().getExtras().getString(Constants.REQUEST_AVATAR));

            notificationData.setVoice(getIntent().getExtras().getString(Constants.REQUEST_VOICE));

            notificationData.setCreated_at(getIntent().getExtras().getString(Constants.REQUEST_TIME));

            notificationData.setStatus(getIntent().getExtras().getInt(Constants.REQUEST_STATUS));

            notificationData.setLat(getIntent().getExtras().getDouble(Constants.REQUEST_LAT));
            notificationData.setLon(getIntent().getExtras().getDouble(Constants.REQUEST_LON));

            notificationData.setCus_name(getIntent().getExtras().getString(Constants.REQUEST_CUSNAME));
            notificationData.setCus_number(getIntent().getExtras().getString(Constants.REQUEST_CUSNUMBER));
            notificationData.setCus_avatar(getIntent().getExtras().getString(Constants.REQUEST_CUSAVATAR));

            notificationData.setCatAvatar(getIntent().getExtras().getString(Constants.REQUEST_CAT_AVATAR));
            Log.v("Sp_IDTEST",notificationData.getStatus()+"");
            if (notificationData.getStatus() > 1){
                notificationData.setSp_id(getIntent().getExtras().getInt(Constants.SP_ID));
                notificationData.setSp_name(getIntent().getExtras().getString(Constants.REQUEST_COMNAME));
                notificationData.setSp_avatar(getIntent().getExtras().getString(Constants.REQUEST_COMAVATAR));
                notificationData.setSp_number(getIntent().getExtras().getString(Constants.REQUEST_COMNUMBER));


            }

            if (notificationData.getStatus() > 2){
                notificationData.setAgent_id(getIntent().getExtras().getInt(Constants.AGENT_ID));
                notificationData.setAgent_name(getIntent().getExtras().getString(Constants.REQUEST_AGENTNAME));
                notificationData.setAgent_avatar(getIntent().getExtras().getString(Constants.REQUEST_AGENTAVATAR));
                notificationData.setAgent_phone(getIntent().getExtras().getString(Constants.REQUEST_AGENTNUMBER));
            }



            RequestDetailInternalBottomSheet fragment = new RequestDetailInternalBottomSheet(notificationData);
            fragment.show(((FragmentActivity) context).getSupportFragmentManager(), "ServiceCategoriesBottomSheet" + notificationData.getId());

        }

        BottomNavigationView navigation =  findViewById(R.id.navigation);
        BottomNavigationViewHelper.removeShiftMode(navigation);

        if (savedInstanceState == null) {

            if (service_status == 0) {
                new MaterialStyledDialog.Builder(AgentDashboard.this)
                        .setTitle("Welcome Labeeb Agent")
                        .setIcon(R.mipmap.ic_launcher).setCancelable(false)
                        .withIconAnimation(true)
                        .setDescription("Press Start to Continue")
                        .setPositiveText("Start")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                if (!checkPermissions()) {
                                    requestPermissions();
                                } else {
                                    Paper.book().write(Constants.SERVICE_LOCATION_STATUS,1);
                                    mService.requestLocationUpdates();
                                }
                            }
                        }).show();

            }
            fragment = AgentPendingTasks_Fragment.newInstance();
            transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame, fragment);
            transaction.commit();
           /* Fragment fragment ;
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            fragment = PendingTasks_Fragment.newInstance();
            navigation.setSelectedItemId(R.id.navigation_requests);
            transaction.replace(R.id.frame, fragment);
            transaction.commit();*/

        }
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        // Check that the user hasn't revoked permissions by going to Settings.
        if (Utils.requestingLocationUpdates(this)) {
            if (!checkPermissions()) {
                requestPermissions();
            }
        }


    }
    @Override
    protected void onStart() {
        super.onStart();

        if (!checkPermissions()){
            requestPermissions();
        }else {
            bindService(new Intent(AgentDashboard.this, LocationUpdatesService.class), mServiceConnection,
                    Context.BIND_AUTO_CREATE);
        }





        // Bind to the service. If the service is in foreground mode, this signals to the service
        // that since this activity is in the foreground, the service can exit foreground mode.


    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver,
                new IntentFilter(LocationUpdatesService.ACTION_BROADCAST));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver);
        super.onPause();
    }

    @Override
    protected void onStop() {
        if (mBound) {
            // Unbind from the service. This signals to the service that this activity is no longer
            // in the foreground, and the service can respond by promoting itself to a foreground
            // service.
            unbindService(mServiceConnection);
            mBound = false;
        }
        super.onStop();
    }

    /**
     * Returns the current state of the permissions needed.
     */
    private boolean checkPermissions() {
        return  PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
            Snackbar.make(
                    findViewById(android.R.id.content),
                    "ok",
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction("ok", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            ActivityCompat.requestPermissions(AgentDashboard.this,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                    REQUEST_PERMISSIONS_REQUEST_CODE);
                        }
                    })
                    .show();
        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(AgentDashboard.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission was granted.
                bindService(new Intent(AgentDashboard.this, LocationUpdatesService.class), mServiceConnection,
                        Context.BIND_AUTO_CREATE);
//                mService.requestLocationUpdates();
            } else {
                // Permission denied.
                Snackbar.make(
                        findViewById(android.R.id.content),
                        "denied",
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction("settings", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        })
                        .show();
            }
        }
    }

    /**
     * Receiver for broadcasts sent by {@link LocationUpdatesService}.
     */
    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Location location = intent.getParcelableExtra(LocationUpdatesService.EXTRA_LOCATION);
            if (location != null) {


            }
        }
    }

    @SuppressLint("ValidFragment")
    public static   class RequestDetailInternalBottomSheet extends BottomSheetDialogFragment {

        private BottomSheetBehavior mBehavior;
        private AppBarLayout app_bar_layout;
        private View
                company_info_layout,
                agent_info_layout;
        /* Company Section */
        private CircleImageView
                company_info_thumbnail,
                company_dialler;
        private TextView
                company_info_name;
        private MaterialRatingBar
                company_info_rating;

        /*Agent Section*/
        private CircleImageView
                agent_info_thumbnail,
                agent_dialler,
                agent_map_launcher;
        private TextView
                agent_info_name;
        private MaterialRatingBar
                agent_info_rating;

        ImageView
                task_image,
                task_image_full;
        TextView
                serviceNameTxt,
                serviceDetailTxt,
                task_time,
                request_address;


        private RecyclerView recyclerView;
        private Bundle bundle;


        private int  requestStatus;
        private NotificationData modal;

        private MapView mapView;
        /*private LinearLayout lyt_profile;

        public void setPeople(People people) {
          this.people = people;
        }*/
        ImageView sound_icon;
        String AUDIO_PATH ;
        private MediaPlayer mediaPlayer;
        private int playbackPosition=0;
        TextView name_toolbar;

        CircleImageView
                customer_avatar;
        TextView
                customer_name,
                customer_phone;
        CircleImageView
                customer_dialler;

        Button
                place_bid_btn,
                place_quote_btn;
        MaterialDialog materialDialog;
        SQLiteDatabase sqLiteDatabase;
        int user_id;

        LinearLayout
                bid_layout;
        LinearLayout
                assign_agent_layout;
        Button
                assign_agent_btn;
        /*Audio*/

        ConstraintLayout
                play_layout;
        Button
                play_btn,
                pause_btn;
        SeekBar
                seekBar;
        TextView
                audio_duration_txt;
        ImageButton
                callButton,
                audioPlayButton;
        ImageView
                full_map_event;

        public RequestDetailInternalBottomSheet(NotificationData modal) {
            this.modal = modal;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
            final View view = View.inflate(getContext(), R.layout.request_detail_bottomsheet, null);

            dialog.setContentView(view);



            requestStatus = modal.getStatus();

            //mediaPlayer = new MediaPlayer();



            mBehavior = BottomSheetBehavior.from((View) view.getParent());
            mBehavior.setPeekHeight(BottomSheetBehavior.PEEK_HEIGHT_AUTO);



            app_bar_layout = (AppBarLayout) view.findViewById(R.id.app_bar_layout);


            name_toolbar = view.findViewById(R.id.name_toolbar);

            /*Request Section*/

            task_image = view.findViewById(R.id.task_image);
            serviceNameTxt  = view.findViewById(R.id.task_serviceName);
            serviceDetailTxt = view.findViewById(R.id.task_subDetail);
            task_image_full = view.findViewById(R.id.task_image_full);

            task_time = view.findViewById(R.id.task_time);
            request_address = view.findViewById(R.id.request_address);

            sound_icon = view.findViewById(R.id.sound_icon);

            mapView = view.findViewById(R.id.request_location);


            customer_name = view.findViewById(R.id.req_customer_name);
            customer_avatar = view.findViewById(R.id.req_customer_avatar);
            customer_phone  = view.findViewById(R.id.req_customer_phone);
            customer_dialler = view.findViewById(R.id.req_customer_dialler);

            place_bid_btn = view.findViewById(R.id.place_bid);
            place_quote_btn = view.findViewById(R.id.place_quote);


            bid_layout = view.findViewById(R.id.bid_layout_for_company);

            assign_agent_layout = view.findViewById(R.id.assign_agent_layout);

            assign_agent_btn = view.findViewById(R.id.assign_agent_btn);


            /*Audio Configuration*/

            play_layout = view.findViewById(R.id.play_layout);
            play_btn = view.findViewById(R.id.play_btn);
            audio_duration_txt = view.findViewById(R.id.audio_time);
            seekBar = view.findViewById(R.id.audio_seekbar);
            pause_btn = view.findViewById(R.id.pause_btn);


            audioPlayButton = view.findViewById(R.id.audio_play_btn);
            callButton = view.findViewById(R.id.call_btn);


            full_map_event = view.findViewById(R.id.full_map_event);

            sqLiteDatabase = getContext().openOrCreateDatabase(Constants.DB_NAME, Context.MODE_PRIVATE,null);
            Cursor cursor = sqLiteDatabase.rawQuery(Constants.GET_PROFILE,null);
            cursor.moveToFirst();

            user_id = cursor.getInt(cursor.getColumnIndex(Constants.DB_USER_ID));

Log.v("StatusForAgent",modal.getNote_status()+"");
            if (modal.getNote_status() == 2014){
                bid_layout.setVisibility(View.GONE);
                assign_agent_layout.setVisibility(View.VISIBLE);
                assign_agent_btn.setText("Start Work");
            }




            place_quote_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                 /*   new SweetAlertDialog(getContext())
                            .setTitleText("Here's a message!")
                            .show();*/
                    new SweetAlertDialog(getContext(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Are you sure?")
                            .setContentText("To Place Quote")
                            .setConfirmText("Yes")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(final SweetAlertDialog sDialog) {

                                    RespondCustomer respondCustomer = ApiClient.getApiClient().create(RespondCustomer.class);
                                    Call<MSG_MODAL> call = respondCustomer.createBid(
                                            modal.getReq_id(),
                                            user_id,
                                            0,
                                            2
                                    );
                                    call.enqueue(new Callback<MSG_MODAL>() {
                                        @Override
                                        public void onResponse(Call<MSG_MODAL> call, Response<MSG_MODAL> response) {
//                                    progressDoalog.dismiss();
                                            if (response.isSuccessful()){
                                                sDialog
                                                        .setTitleText("Success!")
                                                        .setContentText("Your Quote has been Placed")
                                                        .setConfirmText("OK")
                                                        .setConfirmClickListener(null)
                                                        .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                                                dismiss();
                                                Toast.makeText(getContext(),response.body().getMsg(),Toast.LENGTH_SHORT).show();
                                            }
                                            else{
                                                Toast.makeText(getContext(),response.message(),Toast.LENGTH_LONG).show();
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<MSG_MODAL> call, Throwable t) {
                                            //            progressDoalog.dismiss();
                                            Toast.makeText(getContext(),t.getMessage(),Toast.LENGTH_LONG).show();
                                        }
                                    });


                                }
                            })
                            .show();
                }
            });

            place_bid_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean wrapInScrollView = true;
                    materialDialog = new MaterialDialog.Builder(getContext())
                            .title("Enter Bid")
                            .customView(R.layout.custom_view, wrapInScrollView)
                            .positiveText("Submit").onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    View view1 = materialDialog.getView();

                                    EditText bidAmount = view1.findViewById(R.id.bid_amount_field);

                                    RespondCustomer respondCustomer = ApiClient.getApiClient().create(RespondCustomer.class);
                                    Call<MSG_MODAL> call = respondCustomer.createBid(
                                            modal.getReq_id(),
                                            user_id,
                                            Double.parseDouble(bidAmount.getText().toString()),
                                            1
                                    );
                                    call.enqueue(new Callback<MSG_MODAL>() {
                                        @Override
                                        public void onResponse(Call<MSG_MODAL> call, Response<MSG_MODAL> response) {
//                        progressDoalog.dismiss();
                                            if (response.isSuccessful()){
                                                new SweetAlertDialog(getContext(), SweetAlertDialog.SUCCESS_TYPE)
                                                        .setTitleText("Bid Successfully Placed!")
                                                        .setContentText("!")
                                                        .show();

                                                dismiss();
                                                Toast.makeText(getContext(),response.body().getMsg(),Toast.LENGTH_SHORT).show();
                                            }
                                            else{
                                                Toast.makeText(getContext(),response.message(),Toast.LENGTH_LONG).show();
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<MSG_MODAL> call, Throwable t) {
                                            //           progressDoalog.dismiss();
                                            Toast.makeText(getContext(),t.getMessage(),Toast.LENGTH_LONG).show();
                                        }
                                    });



                                }
                            }).build();
                    materialDialog.show();

                }
            });


            /*Agent Assigning Portion*/

            assign_agent_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Agent_Job_Interface agent_job_interface = ApiClient.getApiClient().create(Agent_Job_Interface.class);
                    Call<MSG_MODAL> call = agent_job_interface.update_agent_status(
                            RequestBody.create(MultipartBody.FORM,modal.getReq_id()+""),
                            RequestBody.create(MultipartBody.FORM,4+""),

                            RequestBody.create(MultipartBody.FORM,0+"")
                    );

//                    progressDoalog.show();
                    call.enqueue(new Callback<MSG_MODAL>() {
                        @Override
                        public void onResponse(Call<MSG_MODAL> call, Response<MSG_MODAL> response) {
                            if (response.isSuccessful()){
                                //       progressDoalog.dismiss();


                                AGENTS_TASKS dataModal = new AGENTS_TASKS();


                                dataModal.setId(modal.getReq_id());
                                dataModal.setAddress(modal.getAddress());
                                dataModal.setAgent_id(modal.getAgent_id());
                                dataModal.setAgent_avatar(modal.getAgent_avatar());

                                dataModal.setLat(modal.getLat());
                                dataModal.setLon(modal.getLon());
                                dataModal.setBid_count(modal.getBid_count());
                                dataModal.setFinal_bid(modal.getFinal_bid());
                                dataModal.setVoice(modal.getVoice());
                                dataModal.setCus_id(modal.getCus_id());
                                dataModal.setCus_name(modal.getCus_name());
                                dataModal.setCus_avatar(modal.getCus_avatar());
                                dataModal.setCus_num(modal.getCus_number());
                                dataModal.setCreated_at(modal.getCreated_at());
                                dataModal.setAvatar(modal.getAvatar());
                                dataModal.setThiscat_id(modal.getThiscat_id());
                                dataModal.setThiscat_title(modal.getThiscat_title());
                                dataModal.setSubcat_title(modal.getSubcat_title());
                                dataModal.setThisubcat_id(modal.getThisubcat_id());
                                dataModal.setStatus(modal.getStatus());


                                dataModal.setStatus(4);
                                Paper.init(getContext());
                                Paper.book().write(Constants.REQUEST_MODAL_DATA,dataModal);

                                getActivity().startActivity(new Intent(getContext(), RequestOnMap.class));


                            }
                            else{
                                Toast.makeText(getContext(),response.message(),Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<MSG_MODAL> call, Throwable t) {
                            //        progressDoalog.dismiss();
                            Toast.makeText(getContext(),t.getMessage(),Toast.LENGTH_LONG).show();

                        }
                    });
                }
            });



            mapView.onCreate(savedInstanceState);
            mapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {

                    googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                    // map.setMyLocationEnabled(true);

// Needs to call MapsInitializer before doing any CameraUpdateFactory calls
//                double lat =  Double.parseDouble(Paper.book().read(Constants.LATITUDE).toString());
                    //            double lng = Double.parseDouble(Paper.book().read(Constants.LONGITUDE).toString());
                    MapsInitializer.initialize(getContext());

                    googleMap.addMarker(new MarkerOptions().position(new LatLng(
                            modal.getLat(),
                            modal.getLon())).title("Your Location"));
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(
                            modal.getLat(),
                            modal.getLon()), 16);
                    googleMap.animateCamera(cameraUpdate);
                }
            });


            full_map_event.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), FullMapsActivity.class);
                    Bundle bundle  = new Bundle();
                    bundle.putDouble(Constants.REQ_LAT,modal.getLat());
                    bundle.putDouble(Constants.REQ_LNG,modal.getLon());
                    intent.putExtras(bundle);
                    startActivity(intent);
                    getActivity().overridePendingTransition(android.R.anim.fade_in,android.R.anim.slide_out_right);
                }
            });

            seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                        mediaPlayer.seekTo(seekBar.getProgress());
                    }
                }
            });

            play_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    pause_btn.setVisibility(View.VISIBLE);
                    play_btn.setVisibility(View.GONE);
                    try {
                        playAudio(Constants.REQ_Audio_BASE_URL+modal.getVoice());
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getContext(),e.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }
            });

            pause_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (mediaPlayer != null && mediaPlayer.isPlaying()){
                        mediaPlayer.stop();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            seekBar.setProgress(0,true);
                        }
                        else{
                            seekBar.setProgress(0);
                        }
                        play_btn.setVisibility(View.VISIBLE);
                        pause_btn.setVisibility(View.GONE);
                    }
                }
            });

            task_time.setVisibility(View.GONE);


            if (modal.getStatus() >1){
                try {
                    if (modal.getFinal_bid() == 0) {
                        task_time.setText("Quote");
                    } else {
                        task_time.setText(modal.getFinal_bid() + " AED");
                    }
                }
                catch (NullPointerException e){
                    task_time.setText("");
                }
            }
            else {
                task_time.setVisibility(View.GONE);
            }

            request_address.setText(modal.getAddress());


            /*Customer Profile*/

            customer_name.setText(modal.getCus_name());
            customer_phone.setText(modal.getCus_number());

            try{

                /*String[] image_Array = modal.getAvatar().split("app/");
                String req_img = image_Array[1];
                Log.v("req_img",req_img);*/

                //       Log.v("CusAvatar",modal.getCus_avatar());
                Log.v("Avatar",modal.getAvatar());
                Log.v("Voice",modal.getVoice());
                Glide.with(getContext()).
                        load(Constants.PROFILE_Url+modal.getCatAvatar()).
                        apply(new RequestOptions().placeholder(R.drawable.ic_placeholders_05).error(R.drawable.ic_placeholders_03).diskCacheStrategy(DiskCacheStrategy.RESOURCE))
                        .into(task_image);
                Glide.with(getContext()).
                        load(Constants.REQ_IMAGE_BASE_URL+modal.getCus_avatar()).
                        apply(new RequestOptions().placeholder(R.drawable.ic_placeholders_05).error(R.drawable.ic_placeholders_03).diskCacheStrategy(DiskCacheStrategy.RESOURCE))
                        .into(customer_avatar);
                Glide.with(getContext()).
                        load(Constants.REQ_IMAGE_BASE_URL+modal.getAvatar()).
                        apply(new RequestOptions().placeholder(R.drawable.ic_placeholders_05).error(R.drawable.ic_placeholders_03).diskCacheStrategy(DiskCacheStrategy.RESOURCE))
                        .into(task_image_full);
            }catch (IllegalArgumentException ex){
                Toast.makeText(getContext(),ex.getMessage(),Toast.LENGTH_LONG).show();
            }
            catch (NullPointerException ex){
                Toast.makeText(getContext(),ex.getMessage(),Toast.LENGTH_LONG).show();
            }
            serviceNameTxt.setText(modal.getThiscat_title());
            serviceDetailTxt.setText(modal.getSubcat_title());
            customer_dialler.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:"+modal.getCus_number()));
                    startActivity(intent);
                }
            });

            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            try {
                mediaPlayer.setDataSource(Constants.REQ_Audio_BASE_URL+modal.getVoice());
                mediaPlayer.prepare();
                int dur =  mediaPlayer.getDuration()/1000;
                String duration = null;

                duration = "00:"+String.format("%02d", dur);


                audio_duration_txt.setText(duration);
            } catch (IOException e) {
                e.printStackTrace();
            }

            task_image_full.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), Full_Image.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.FULL_SCREEN_IMAGE,Constants.REQ_IMAGE_BASE_URL+modal.getAvatar());
                    intent.putExtras(bundle);
                    startActivity(intent);
                    getActivity().overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                }
            });










            Log.v("dataBottom",modal.getStatus()+"");

            //hideView(recyclerView);

            mBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    if (BottomSheetBehavior.STATE_EXPANDED == newState) {
                        showView(app_bar_layout, getActionBarSize());
                        // showView(recyclerView,recyclerView.getScrollBarSize());
//                    hideView(lyt_profile);
                    }
                    if (BottomSheetBehavior.STATE_COLLAPSED == newState) {
                        hideView(app_bar_layout);
                        //  showView(lyt_profile, getActionBarSize());
                    }

                    if (BottomSheetBehavior.STATE_HIDDEN == newState) {
                        dismiss();
                    }
                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                }
            });

            ((ImageButton) view.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });

            return dialog;
        }

        @Override
        public void onStart() {
            super.onStart();
            mapView.onStart();
            mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

        }

        private void hideView(View view) {
            ViewGroup.LayoutParams params = view.getLayoutParams();
            params.height = 0;
            view.setLayoutParams(params);
        }

        private void showView(View view, int size) {
            ViewGroup.LayoutParams params = view.getLayoutParams();
            params.height = size;
            view.setLayoutParams(params);
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
            mapView.onDestroy();
        }

        @Override
        public void onLowMemory() {
            super.onLowMemory();
            mapView.onLowMemory();
        }

        private int getActionBarSize() {
            final TypedArray styledAttributes = getContext().getTheme().obtainStyledAttributes(new int[]{android.R.attr.actionBarSize});
            int size = (int) styledAttributes.getDimension(0, 0);
            return size;
        }

        private void playAudio(String url)
        {
            killMediaPlayer();



            try {
                mediaPlayer = new MediaPlayer();
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

                mediaPlayer.setDataSource(url);
                mediaPlayer.prepare();


                // might take long! (for buffering, etc)
                mediaPlayer.start();
                seekBar.setMax(mediaPlayer.getDuration());

                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        pause_btn.setVisibility(View.GONE);
                        play_btn.setVisibility(View.VISIBLE);
                    }
                });

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        int currentPosition = mediaPlayer.getCurrentPosition();
                        int total = mediaPlayer.getDuration();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            seekBar.setProgress(0, true);
                        } else {
                            seekBar.setProgress(0);
                        }

                        while (mediaPlayer != null && mediaPlayer.isPlaying() && currentPosition < total) {
                            try {
                                Thread.sleep(1000);
                                currentPosition = mediaPlayer.getCurrentPosition();
                            } catch (InterruptedException e) {
                                return;
                            } catch (Exception e) {
                                return;
                            }


                            if (!mediaPlayer.isPlaying()) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    seekBar.setProgress(0, true);
                                } else {
                                    seekBar.setProgress(0);
                                }

                            } else {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    seekBar.setProgress(currentPosition, true);
                                } else {
                                    seekBar.setProgress(currentPosition);
                                }
                            }
                            if (seekBar.getProgress() == total) {
                                play_btn.setVisibility(View.VISIBLE);
                                pause_btn.setVisibility(View.GONE);
                            }
                        }
                    }
                }).start();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }


        private void killMediaPlayer() {
            if(mediaPlayer!=null) {
                try {
                    mediaPlayer.release();
                    seekBar.setProgress(0);
                }
                catch(Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }




}
