
package com.thakaa360.labeebpro.helpers;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.ablanco.zoomy.Zoomy;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.thakaa360.labeebpro.R;
import com.thakaa360.labeebpro.utilities.Constants;

public class Full_Image extends AppCompatActivity {

    ImageView
                imageView;
    String
            imagePath;
    Context
            context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full__image);

        getWindow().setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.FILL_PARENT);

        context = Full_Image.this;

        imagePath = getIntent().getExtras().getString(Constants.FULL_SCREEN_IMAGE);

        imageView = findViewById(R.id.expanded_image);

        try{

            Glide.with(context).
                    load(imagePath).
                    apply(new RequestOptions().placeholder(R.drawable.ic_placeholders_05).error(R.drawable.ic_placeholders_03).diskCacheStrategy(DiskCacheStrategy.RESOURCE))
                    .into(imageView);
            Zoomy.Builder builder = new Zoomy.Builder(this).target(imageView);
            builder.register();


        }catch (IllegalArgumentException ex){
            Toast.makeText(context,ex.getMessage(),Toast.LENGTH_LONG).show();
        }
        catch (NullPointerException ex){
            Toast.makeText(context,ex.getMessage(),Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
    }
}
