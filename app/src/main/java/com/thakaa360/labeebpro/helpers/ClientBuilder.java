package com.thakaa360.labeebpro.helpers;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.thakaa360.labeebpro.helpers.HttpParsing.getUnsafeOkHttpClient;


public class ClientBuilder {
    public static Retrofit getClient(String url){
        return new Retrofit.Builder()
                .baseUrl(url).client(getUnsafeOkHttpClient()).addConverterFactory(GsonConverterFactory.create())
                .build();
    }
}
