package com.thakaa360.labeebpro.activities;

import android.Manifest;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.thakaa360.labeebpro.R;
import com.thakaa360.labeebpro.fragments.ShowCategoriesFullscreenFragment;
import com.thakaa360.labeebpro.modals.ChipModal;
import com.thakaa360.labeebpro.utilities.LayoutUtility;
import com.thakaa360.labeebpro.utilities.Major;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import io.paperdb.Paper;

public class AgentRegistration extends Major {

    Context context;
    MapView
            mapView;
    GoogleMap
            googleMap;
    Button
            pickLocation;
    private int PLACE_PICKER_REQUEST = 1;
    private static final int MY_PERMISSION_REQUEST_CODE = 7171;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 7172;

    private Uri outputFileUri;
    private Uri resultUri;
    private File fileCrop;
    private static String imageCode;
    private ImageView img_selector;
    private CircleImageView profile_img;
    private Snackbar
                    snackbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View contentView = LayoutUtility.getLayout(this, R.layout.activity_agent_registration);

        mDrawer.addView(contentView, 0);
        navigationView.setCheckedItem(R.id.agent_register_item);

        toolbar = findViewById(R.id.toolbar2);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawer.openDrawer(GravityCompat.START);
            }
        });

        snackbar = Snackbar.make(findViewById(android.R.id.content),"Register", BaseTransientBottomBar.LENGTH_INDEFINITE);

        //snackbar.show();

        context = this;

        mapView = contentView.findViewById(R.id.mapview);

        pickLocation = contentView.findViewById(R.id.pickLocation);

        img_selector = findViewById(R.id.image_selector);
        profile_img = contentView.findViewById(R.id.agent_profile_img);

        mapView.onCreate(savedInstanceState);

        if (ContextCompat.checkSelfPermission(AgentRegistration.this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(AgentRegistration.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(AgentRegistration.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSION_REQUEST_CODE);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
        }


        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap Map) {
                googleMap = Map;
                try {
                    // Customise the styling of the base map using a JSON object defined
                    // in a raw resource file.
                    boolean success = googleMap.setMapStyle(
                            MapStyleOptions.loadRawResourceStyle(
                                    AgentRegistration.this, R.raw.custom_map));

                    if (!success) {
                        Log.e("MapsActivityRaw", "Style parsing failed.");
                    }
                } catch (Resources.NotFoundException e) {
                    Log.e("MapsActivityRaw", "Can't find style.", e);
                }

                googleMap.setBuildingsEnabled(true);

                    //googleMap.setMyLocationEnabled(true);
            }
        });

        pickLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlacePicker.IntentBuilder placebuilder = new PlacePicker.IntentBuilder();
                try {
                    startActivityForResult(placebuilder.build(AgentRegistration.this), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });


            openImageIntent();



    }



    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    public void showDialogFullscreen(View view) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        ShowCategoriesFullscreenFragment newFragment = new ShowCategoriesFullscreenFragment();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(android.R.id.content, newFragment).addToBackStack(null).commit();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSION_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {
            if (requestCode == PLACE_PICKER_REQUEST) {
                Place place = PlacePicker.getPlace(data, this);
                StringBuilder stBuilder = new StringBuilder();
                String placename = String.format("%s", place.getName());
                String latitude = String.valueOf(place.getLatLng().latitude);
                //  this.latitude = place.getLatLng().latitude;
                // this.longitude = place.getLatLng().longitude;
                String longitude = String.valueOf(place.getLatLng().longitude);
                String address = String.format("%s", place.getAddress());
                stBuilder.append("Name: ");
                stBuilder.append(placename);
                stBuilder.append("\n");
                stBuilder.append("Latitude: ");
                stBuilder.append(latitude);
                stBuilder.append("\n");
                stBuilder.append("Logitude: ");
                stBuilder.append(longitude);
                stBuilder.append("\n");
                stBuilder.append("Address: ");
                stBuilder.append(address);
                //Toast.makeText(getApplicationContext(),placename+" "+address,Toast.LENGTH_SHORT).show();



                googleMap.addMarker(new MarkerOptions().position(place.getLatLng()));
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 16);
                googleMap.animateCamera(cameraUpdate);
                //textView.setText(stBuilder.toString());
                //  add_request_address.setText(address);
            }

            if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK){
                boolean isCamera ;
                if (data == null){
                    isCamera = true;
                }else{
                    String action = data.getAction();
                    if (action == null){
                        isCamera = false;

                    }else{
                        isCamera = action.equals(MediaStore.ACTION_IMAGE_CAPTURE);

                    }
                }
                Uri selectedImageUri;
                if (isCamera){
                    selectedImageUri = outputFileUri;

                }else{
                    selectedImageUri = data == null ? null : data.getData();
                }
                cropImageActivity(selectedImageUri,this);
            }
            else if (requestCode  == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE){
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK){
                    resultUri = result.getUri();

                    fileCrop = new File(getRealPathFromURI(resultUri));

                    Log.d("FILECROP",fileCrop.toString());
                    Log.d("URI",resultUri.toString());
//                Glide.with(getApplicationContext()).load(resultUri).into(ivUserImage);
                    // cropImageView.setImageUriAsync(resultUri);
                    File file = new File(resultUri.getPath());
                    Bitmap bitmap  = BitmapFactory.decodeFile(file.getPath());
                    //  imageStatus = true;

                    profile_img.setImageBitmap(bitmap);
                    //  verticalStepperForm.goToNextStep();
                    imageCode = encodeToBase64(bitmap, Bitmap.CompressFormat.JPEG, 100);

                    // textView.setText(myBase64Image);
                    //  Bitmap bitmap1 = decodeBase64(myBase64Image);
                    //  ivUserImage.setImageBitmap(bitmap1);
                    // Toast.makeText(getApplicationContext(),myBase64Image,Toast.LENGTH_SHORT).show();


                }else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE){
                    Exception error =result.getError();
                    Log.d("Error",error.toString());
                }
            }
        }

    }


    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    public static void cropImageActivity(Uri selectedImageUri,Activity activity){

        try {
            CropImage.activity(selectedImageUri).setScaleType(CropImageView.ScaleType.CENTER_INSIDE).setFixAspectRatio(true).setAspectRatio(1, 1).setGuidelines(CropImageView.Guidelines.ON).start(activity);
        }catch (IllegalArgumentException e){

        }
        catch (RuntimeException r){

        }
    }
    public static String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat, int quality)
    {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, quality, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }

    private void openImageIntent(){
        //Determine Uri of camera image to save
        File file = new File(Environment.getExternalStorageDirectory() + File.separator + "Labeeb" + File.separator);
        if(!file.exists())
            file.mkdirs();
        String fileName = "img_"+System.currentTimeMillis()+".jpg";
        File sdImageMainDirectory = new File(file,fileName);
        outputFileUri  = Uri.fromFile(sdImageMainDirectory);

        Log.d("URIOUTPUT",outputFileUri.toString());
        final PackageManager packageManager = getPackageManager();
        img_selector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Choose of gallery options
                Intent chooseIntent = Intent.createChooser(galleryIntents(packageManager),"Choose Source");
                //Add the camera options
                chooseIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, captureIntents(packageManager).toArray(new Parcelable[captureIntents(packageManager).size()]));

                startActivityForResult(chooseIntent, CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE);
            }
        });
    }

    private List<Intent> captureIntents(PackageManager packageManager){
        //Camera
        final List<Intent> cameraIntents = new ArrayList<>();
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

        List<ResolveInfo> list = packageManager.queryIntentActivities(captureIntent,0);
        for (ResolveInfo res : list){
            String packageName = res.activityInfo.packageName;
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            if (outputFileUri != null){
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            cameraIntents.add(intent);
        }
        return cameraIntents;
    }

    //Open gallery Intent
    private Intent galleryIntents(PackageManager packageManager){
        List<Intent> galleryIntents = new ArrayList<>();
        final Intent galleryIntent = new  Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery =  packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new  Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            galleryIntents.add(intent);
        }
        return galleryIntent;
    }

}
