package com.thakaa360.labeebpro.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.github.marlonlom.utilities.timeago.TimeAgo;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.annotations.SerializedName;
import com.karan.churi.PermissionManager.PermissionManager;
import com.thakaa360.labeebpro.R;
import com.thakaa360.labeebpro.fragments.History_Fragment;
import com.thakaa360.labeebpro.fragments.InProgressTasks_Fragment;
import com.thakaa360.labeebpro.fragments.PendingTasks_Fragment;
import com.thakaa360.labeebpro.fragments.View_Agents;
import com.thakaa360.labeebpro.helpers.BottomNavigationViewHelper;
import com.thakaa360.labeebpro.helpers.Full_Image;
import com.thakaa360.labeebpro.helpers.TimeConverter;
import com.thakaa360.labeebpro.modals.NotificationData;
import com.thakaa360.labeebpro.server.client.ApiClient;
import com.thakaa360.labeebpro.server.interfaces.RespondCustomer;
import com.thakaa360.labeebpro.server.modals.INPROGRESS_TASKS_MODAL;
import com.thakaa360.labeebpro.server.modals.MSG_MODAL;
import com.thakaa360.labeebpro.services.NotificationService;
import com.thakaa360.labeebpro.utilities.Constants;
import com.thakaa360.labeebpro.utilities.LayoutUtility;
import com.thakaa360.labeebpro.utilities.Major;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;
import io.paperdb.Paper;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CompanyDashboard extends Major {
    PermissionManager permission = null;
    Fragment fragment;
    FragmentTransaction transaction;
    ArrayList<String> granted = new ArrayList<>();
    ArrayList<String> denied = new ArrayList<>();

    int user_id;
    NotificationData notificationData;
    Context context;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_requests:
                    fragment = PendingTasks_Fragment.newInstance();
                    transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.frame, fragment);
                    transaction.commit();
                    return true;
                case R.id.navigation_tasks:
                    fragment = InProgressTasks_Fragment.newInstance();
                    transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.frame, fragment);
                    transaction.commit();
                    return true;
                case R.id.navigation_history:
                    fragment = History_Fragment.newInstance();
                    transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.frame, fragment);
                    transaction.commit();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View contentView = LayoutUtility.getLayout(this, R.layout.activity_dashboard);

        permission = new PermissionManager() {
            @Override
            public void ifCancelledAndCanRequest(Activity activity) {
                // Do Customized operation if permission is cancelled without checking "Don't ask again"
                super.ifCancelledAndCanRequest(activity);// or Don't override this method if not in use
                startActivity(new Intent(CompanyDashboard.this,CompanyDashboard.class));
                finish();
                overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);

            }

            @Override
            public void ifCancelledAndCannotRequest(Activity activity) {
                // Do Customized operation if permission is cancelled with checking "Don't ask again"
                super.ifCancelledAndCannotRequest(activity); //or Don't override this method if not in use
                //  Toast.makeText(getApplicationContext(),denied.get(i).toString(),Toast.LENGTH_SHORT).show();
                //finish();



            }


        };
        permission.checkAndRequestPermissions(this);

        context = CompanyDashboard.this;

        notificationData = new NotificationData();
            String Token = FirebaseInstanceId.getInstance().getToken();
            Log.v("Tok",Token);
        //
        //startService(new Intent(this,NotificationService.class));




        mDrawer.addView(contentView, 0);
        navigationView.setCheckedItem(R.id.dashboard_item);

        toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawer.openDrawer(GravityCompat.START);
            }
        });

        BottomNavigationView navigation =  findViewById(R.id.navigation);
        BottomNavigationViewHelper.removeShiftMode(navigation);
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) navigation.getChildAt(0);
        for (int i = 0; i < menuView.getChildCount(); i++) {
            final View iconView = menuView.getChildAt(i).findViewById(android.support.design.R.id.icon);
            final ViewGroup.LayoutParams layoutParams = iconView.getLayoutParams();
            final DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
            layoutParams.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, displayMetrics);
            layoutParams.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, displayMetrics);
            iconView.setLayoutParams(layoutParams);
        }

        if (savedInstanceState == null) {
            Fragment fragment ;
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

                    fragment = PendingTasks_Fragment.newInstance();
                    navigation.setSelectedItemId(R.id.navigation_requests);
                    transaction.replace(R.id.frame, fragment);
                    transaction.commit();

        }
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);







        if (getIntent().getExtras() != null){

            notificationData.setAddress(getIntent().getExtras().getString(Constants.REQUEST_ADDRESS));
            notificationData.setNote_status(getIntent().getExtras().getInt(Constants.NOTSTATUS));
            notificationData.setReq_id(getIntent().getExtras().getInt(Constants.REQUEST_ID));

            notificationData.setThiscat_id(getIntent().getExtras().getInt(Constants.REQUEST_CAT_ID));
            notificationData.setThisubcat_id(getIntent().getExtras().getInt(Constants.REQUEST_SUBCAT_ID));

            notificationData.setThiscat_title(getIntent().getExtras().getString(Constants.REQUEST_CAT_TITLE));
            notificationData.setSubcat_title(getIntent().getExtras().getString(Constants.REQUEST_SUBCAT_TITLE));

            notificationData.setAvatar(getIntent().getExtras().getString(Constants.REQUEST_AVATAR));

            notificationData.setVoice(getIntent().getExtras().getString(Constants.REQUEST_VOICE));

            notificationData.setCreated_at(getIntent().getExtras().getString(Constants.REQUEST_TIME));

            notificationData.setStatus(getIntent().getExtras().getInt(Constants.REQUEST_STATUS));

            notificationData.setLat(getIntent().getExtras().getDouble(Constants.REQUEST_LAT));
            notificationData.setLon(getIntent().getExtras().getDouble(Constants.REQUEST_LON));

            notificationData.setCus_name(getIntent().getExtras().getString(Constants.REQUEST_CUSNAME));
            notificationData.setCus_number(getIntent().getExtras().getString(Constants.REQUEST_CUSNUMBER));
            notificationData.setCus_avatar(getIntent().getExtras().getString(Constants.REQUEST_CUSAVATAR));

            notificationData.setCatAvatar(getIntent().getExtras().getString(Constants.REQUEST_CAT_AVATAR));
            Log.v("Sp_IDTEST",notificationData.getStatus()+"");
            if (notificationData.getStatus() > 1){
                notificationData.setSp_id(getIntent().getExtras().getInt(Constants.SP_ID));
                notificationData.setSp_name(getIntent().getExtras().getString(Constants.REQUEST_COMNAME));
                notificationData.setSp_avatar(getIntent().getExtras().getString(Constants.REQUEST_COMAVATAR));
                notificationData.setSp_number(getIntent().getExtras().getString(Constants.REQUEST_COMNUMBER));


            }

            if (notificationData.getStatus() > 2){
                notificationData.setAgent_id(getIntent().getExtras().getInt(Constants.AGENT_ID));
                notificationData.setAgent_name(getIntent().getExtras().getString(Constants.REQUEST_AGENTNAME));
                notificationData.setAgent_avatar(getIntent().getExtras().getString(Constants.REQUEST_AGENTAVATAR));
                notificationData.setAgent_phone(getIntent().getExtras().getString(Constants.REQUEST_AGENTNUMBER));
            }



            RequestDetailInternalBottomSheet fragment = new RequestDetailInternalBottomSheet(notificationData);
            fragment.show(((FragmentActivity) context).getSupportFragmentManager(), "ServiceCategoriesBottomSheet" + notificationData.getId());

        }
      //  Log.v("notTrigger",not_trigger+"");
/*        if (not_trigger == 1) {

            inprogress_tasks_modal = Paper.book().read(Constants.NOTIFICATION_MODAL);


            Paper.book().write(Constants.NOTIFICATION_TRIGGER,0);

        }*/

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        permission.checkResult(requestCode, permissions, grantResults);
        //To get Granted Permission and Denied Permission
        granted = permission.getStatus().get(0).granted;
        denied = permission.getStatus().get(0).denied;





    }
    @SuppressLint("ValidFragment")
    public static   class RequestDetailInternalBottomSheet extends BottomSheetDialogFragment {

        private BottomSheetBehavior mBehavior;
        private AppBarLayout app_bar_layout;
        private View
                company_info_layout,
                agent_info_layout;
        /* Company Section */
        private CircleImageView
                company_info_thumbnail,
                company_dialler;
        private TextView
                company_info_name;
        private MaterialRatingBar
                company_info_rating;

        /*Agent Section*/
        private CircleImageView
                agent_info_thumbnail,
                agent_dialler,
                agent_map_launcher;
        private TextView
                agent_info_name;
        private MaterialRatingBar
                agent_info_rating;

        ImageView
                task_image,
                task_image_full;
        TextView
                serviceNameTxt,
                serviceDetailTxt,
                task_time,
                request_address;


        private RecyclerView recyclerView;
        private Bundle bundle;


        private int  requestStatus;
        private NotificationData modal;

        private MapView mapView;
        /*private LinearLayout lyt_profile;

        public void setPeople(People people) {
          this.people = people;
        }*/
        ImageView sound_icon;
        String AUDIO_PATH ;
        private MediaPlayer mediaPlayer;
        private int playbackPosition=0;
        TextView name_toolbar;

        CircleImageView
                customer_avatar;
        TextView
                customer_name,
                customer_phone;
        CircleImageView
                customer_dialler;

        Button
                    place_bid_btn,
                    place_quote_btn;
        MaterialDialog materialDialog;
        SQLiteDatabase sqLiteDatabase;
        int user_id;

        LinearLayout
                            bid_layout;
        LinearLayout
                            assign_agent_layout;
        Button
                            assign_agent_btn;
        /*Audio*/

        ConstraintLayout
                play_layout;
        Button
                play_btn,
                pause_btn;
        SeekBar
                seekBar;
        TextView
                audio_duration_txt;

        ImageButton
                callButton,
                audioPlayButton;
        ImageView
                full_map_event;

        public RequestDetailInternalBottomSheet(NotificationData modal) {
            this.modal = modal;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
            final View view = View.inflate(getContext(), R.layout.request_detail_bottomsheet, null);

            dialog.setContentView(view);



            requestStatus = modal.getStatus();

            //mediaPlayer = new MediaPlayer();



            mBehavior = BottomSheetBehavior.from((View) view.getParent());
            mBehavior.setPeekHeight(BottomSheetBehavior.PEEK_HEIGHT_AUTO);



            app_bar_layout = (AppBarLayout) view.findViewById(R.id.app_bar_layout);


            name_toolbar = view.findViewById(R.id.name_toolbar);

            /*Request Section*/

            task_image = view.findViewById(R.id.task_image);
            serviceNameTxt  = view.findViewById(R.id.task_serviceName);
            serviceDetailTxt = view.findViewById(R.id.task_subDetail);
            task_image_full = view.findViewById(R.id.task_image_full);

            task_time = view.findViewById(R.id.task_time);
            request_address = view.findViewById(R.id.request_address);

            sound_icon = view.findViewById(R.id.sound_icon);

            mapView = view.findViewById(R.id.request_location);

            audioPlayButton = view.findViewById(R.id.audio_play_btn);
            callButton = view.findViewById(R.id.call_btn);


            full_map_event = view.findViewById(R.id.full_map_event);


            customer_name = view.findViewById(R.id.req_customer_name);
            customer_avatar = view.findViewById(R.id.req_customer_avatar);
            customer_phone  = view.findViewById(R.id.req_customer_phone);
            customer_dialler = view.findViewById(R.id.req_customer_dialler);

            place_bid_btn = view.findViewById(R.id.place_bid);
            place_quote_btn = view.findViewById(R.id.place_quote);


            bid_layout = view.findViewById(R.id.bid_layout_for_company);

            assign_agent_layout = view.findViewById(R.id.assign_agent_layout);

            assign_agent_btn = view.findViewById(R.id.assign_agent_btn);

            /*Audio Configuration*/

            play_layout = view.findViewById(R.id.play_layout);
            play_btn = view.findViewById(R.id.play_btn);
            audio_duration_txt = view.findViewById(R.id.audio_time);
            seekBar = view.findViewById(R.id.audio_seekbar);
            pause_btn = view.findViewById(R.id.pause_btn);


            sqLiteDatabase = getContext().openOrCreateDatabase(Constants.DB_NAME, Context.MODE_PRIVATE,null);
            Cursor cursor = sqLiteDatabase.rawQuery(Constants.GET_PROFILE,null);
            cursor.moveToFirst();

            user_id = cursor.getInt(cursor.getColumnIndex(Constants.DB_USER_ID));

            if (modal.getStatus() == 0){
                bid_layout.setVisibility(View.VISIBLE);
                assign_agent_layout.setVisibility(View.GONE);
            }

            if (modal.getNote_status() == 2005){
                bid_layout.setVisibility(View.VISIBLE);
                assign_agent_layout.setVisibility(View.GONE);
            }

            if (modal.getNote_status() == 2006){
                bid_layout.setVisibility(View.GONE);
                assign_agent_layout.setVisibility(View.VISIBLE);
            }


            place_quote_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                 /*   new SweetAlertDialog(getContext())
                            .setTitleText("Here's a message!")
                            .show();*/
                    new SweetAlertDialog(getContext(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Are you sure?")
                            .setContentText("To Place Quote")
                            .setConfirmText("Yes")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(final SweetAlertDialog sDialog) {

                                    RespondCustomer respondCustomer = ApiClient.getApiClient().create(RespondCustomer.class);
                                    Call<MSG_MODAL> call = respondCustomer.createBid(
                                            modal.getReq_id(),
                                            user_id,
                                            0,
                                            2
                                    );
                                    call.enqueue(new Callback<MSG_MODAL>() {
                                        @Override
                                        public void onResponse(Call<MSG_MODAL> call, Response<MSG_MODAL> response) {
//                                    progressDoalog.dismiss();
                                            if (response.isSuccessful()){
                                                sDialog
                                                        .setTitleText("Success!")
                                                        .setContentText("Your Quote has been Placed")
                                                        .setConfirmText("OK")
                                                        .setConfirmClickListener(null)
                                                        .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                                                dismiss();
                                                Toast.makeText(getContext(),response.body().getMsg(),Toast.LENGTH_SHORT).show();
                                            }
                                            else{
                                                Toast.makeText(getContext(),response.message(),Toast.LENGTH_LONG).show();
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<MSG_MODAL> call, Throwable t) {
                                            //            progressDoalog.dismiss();
                                            Toast.makeText(getContext(),t.getMessage(),Toast.LENGTH_LONG).show();
                                        }
                                    });


                                }
                            })
                            .show();
                }
            });

            place_bid_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean wrapInScrollView = true;
                   materialDialog = new MaterialDialog.Builder(getContext())
                            .title("Enter Bid")
                            .customView(R.layout.custom_view, wrapInScrollView)
                            .positiveText("Submit").onPositive(new MaterialDialog.SingleButtonCallback() {
                               @Override
                               public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                   View view1 = materialDialog.getView();

                                   EditText bidAmount = view1.findViewById(R.id.bid_amount_field);

                                   RespondCustomer respondCustomer = ApiClient.getApiClient().create(RespondCustomer.class);
                                   Call<MSG_MODAL> call = respondCustomer.createBid(
                                           modal.getReq_id(),
                                           user_id,
                                           Double.parseDouble(bidAmount.getText().toString()),
                                           1
                                   );
                                   call.enqueue(new Callback<MSG_MODAL>() {
                                       @Override
                                       public void onResponse(Call<MSG_MODAL> call, Response<MSG_MODAL> response) {
//                        progressDoalog.dismiss();
                                           if (response.isSuccessful()){
                                               new SweetAlertDialog(getContext(), SweetAlertDialog.SUCCESS_TYPE)
                                                       .setTitleText("Bid Successfully Placed!")
                                                       .setContentText("!")
                                                       .show();

                                               dismiss();
                                               Toast.makeText(getContext(),response.body().getMsg(),Toast.LENGTH_SHORT).show();
                                           }
                                           else{
                                               Toast.makeText(getContext(),response.message(),Toast.LENGTH_LONG).show();
                                           }
                                       }

                                       @Override
                                       public void onFailure(Call<MSG_MODAL> call, Throwable t) {
                                           //           progressDoalog.dismiss();
                                           Toast.makeText(getContext(),t.getMessage(),Toast.LENGTH_LONG).show();
                                       }
                                   });



                               }
                           }).build();
                   materialDialog.show();

                }
            });

            full_map_event.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), FullMapsActivity.class);
                    Bundle bundle  = new Bundle();
                    bundle.putDouble(Constants.REQ_LAT,modal.getLat());
                    bundle.putDouble(Constants.REQ_LNG,modal.getLon());
                    intent.putExtras(bundle);
                    startActivity(intent);
                    getActivity().overridePendingTransition(android.R.anim.fade_in,android.R.anim.slide_out_right);
                }
            });

            /*Agent Assigning Portion*/

            assign_agent_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle b =new Bundle();
                    b.putDouble("request_lat",modal.getLat());
                    b.putDouble("request_lon",modal.getLon());
                    b.putInt("request_id",modal.getReq_id());
                    b.putInt("company_id",modal.getSp_id());
                    b.putInt("request_subcategory",modal.getThisubcat_id());
                    b.putInt("status",3);

                    Log.v("AssignAgentLog",
                            modal.getLat()+" "+modal.getLon()+" "+modal.getReq_id()+" "+modal.getSp_id()+" "+modal.getThisubcat_id());
                    Intent i = new Intent(getContext(),ViewAgentsOnMap.class);
                    i.putExtras(b);
                    startActivity(i);
                }
            });



            mapView.onCreate(savedInstanceState);
            mapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {

                    googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                    // map.setMyLocationEnabled(true);

// Needs to call MapsInitializer before doing any CameraUpdateFactory calls
//                double lat =  Double.parseDouble(Paper.book().read(Constants.LATITUDE).toString());
                    //            double lng = Double.parseDouble(Paper.book().read(Constants.LONGITUDE).toString());
                    MapsInitializer.initialize(getContext());

                    googleMap.addMarker(new MarkerOptions().position(new LatLng(
                            modal.getLat(),
                            modal.getLon())).title("Your Location"));
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(
                            modal.getLat(),
                            modal.getLon()), 16);
                    googleMap.animateCamera(cameraUpdate);
                }
            });

            seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                        mediaPlayer.seekTo(seekBar.getProgress());
                    }
                }
            });

            play_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    pause_btn.setVisibility(View.VISIBLE);
                    play_btn.setVisibility(View.GONE);
                    try {
                        playAudio(Constants.REQ_Audio_BASE_URL+modal.getVoice());
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getContext(),e.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }
            });

            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            try {
                mediaPlayer.setDataSource(Constants.REQ_Audio_BASE_URL+modal.getVoice());
                mediaPlayer.prepare();
                int dur =  mediaPlayer.getDuration()/1000;
                String duration = null;

                duration = "00:"+String.format("%02d", dur);


                audio_duration_txt.setText(duration);
            } catch (IOException e) {
                e.printStackTrace();
            }

            pause_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (mediaPlayer != null && mediaPlayer.isPlaying()){
                        mediaPlayer.stop();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            seekBar.setProgress(0,true);
                        }
                        else{
                            seekBar.setProgress(0);
                        }
                        play_btn.setVisibility(View.VISIBLE);
                        pause_btn.setVisibility(View.GONE);
                    }
                }
            });

            callButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:"+modal.getCus_number()));
                    startActivity(intent);
                }
            });

            audioPlayButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    play_layout.setVisibility(View.VISIBLE);
                }
            });

            task_time.setVisibility(View.GONE);


            if (modal.getStatus() >1){
                try {
                    if (modal.getFinal_bid() == 0) {
                        task_time.setText("Quote");
                    } else {
                        task_time.setText(modal.getFinal_bid() + " AED");
                    }
                }
                catch (NullPointerException e){
                    task_time.setText("");
                }
            }
            else {
                task_time.setVisibility(View.GONE);
            }

            request_address.setText(modal.getAddress());


            /*Customer Profile*/

            customer_name.setText(modal.getCus_name());
            customer_phone.setText(modal.getCus_number());

            try{

                /*String[] image_Array = modal.getAvatar().split("app/");
                String req_img = image_Array[1];
                Log.v("req_img",req_img);*/

         //       Log.v("CusAvatar",modal.getCus_avatar());
                Log.v("Avatar",modal.getAvatar());
                Log.v("Voice",modal.getVoice());
                Glide.with(getContext()).
                        load(Constants.PROFILE_Url+modal.getCatAvatar()).
                        apply(new RequestOptions().placeholder(R.drawable.ic_placeholders_05).error(R.drawable.ic_placeholders_03).diskCacheStrategy(DiskCacheStrategy.RESOURCE))
                        .into(task_image);
                Glide.with(getContext()).
                        load(Constants.REQ_IMAGE_BASE_URL+modal.getCus_avatar()).
                        apply(new RequestOptions().placeholder(R.drawable.ic_placeholders_05).error(R.drawable.ic_placeholders_03).diskCacheStrategy(DiskCacheStrategy.RESOURCE))
                        .into(customer_avatar);
                Glide.with(getContext()).
                        load(Constants.REQ_IMAGE_BASE_URL+modal.getAvatar()).
                        apply(new RequestOptions().placeholder(R.drawable.ic_placeholders_05).error(R.drawable.ic_placeholders_03).diskCacheStrategy(DiskCacheStrategy.RESOURCE))
                        .into(task_image_full);
            }catch (IllegalArgumentException ex){
                Toast.makeText(getContext(),ex.getMessage(),Toast.LENGTH_LONG).show();
            }
            catch (NullPointerException ex){
                Toast.makeText(getContext(),ex.getMessage(),Toast.LENGTH_LONG).show();
            }
            serviceNameTxt.setText(modal.getThiscat_title());
            serviceDetailTxt.setText(modal.getSubcat_title());
            customer_dialler.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:"+modal.getCus_number()));
                    startActivity(intent);
                }
            });

            task_image_full.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), Full_Image.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.FULL_SCREEN_IMAGE,Constants.REQ_IMAGE_BASE_URL+modal.getAvatar());
                    intent.putExtras(bundle);
                    startActivity(intent);
                    getActivity().overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                }
            });










            Log.v("dataBottom",modal.getStatus()+"");

            //hideView(recyclerView);

            mBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    if (BottomSheetBehavior.STATE_EXPANDED == newState) {
                        showView(app_bar_layout, getActionBarSize());
                        // showView(recyclerView,recyclerView.getScrollBarSize());
//                    hideView(lyt_profile);
                    }
                    if (BottomSheetBehavior.STATE_COLLAPSED == newState) {
                        hideView(app_bar_layout);
                        //  showView(lyt_profile, getActionBarSize());
                    }

                    if (BottomSheetBehavior.STATE_HIDDEN == newState) {
                        dismiss();
                    }
                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                }
            });

            ((ImageButton) view.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });

            return dialog;
        }

        @Override
        public void onStart() {
            super.onStart();
            mapView.onStart();
            mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

        }

        private void hideView(View view) {
            ViewGroup.LayoutParams params = view.getLayoutParams();
            params.height = 0;
            view.setLayoutParams(params);
        }

        private void showView(View view, int size) {
            ViewGroup.LayoutParams params = view.getLayoutParams();
            params.height = size;
            view.setLayoutParams(params);
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
            mapView.onDestroy();
        }

        @Override
        public void onLowMemory() {
            super.onLowMemory();
            mapView.onLowMemory();
        }

        private int getActionBarSize() {
            final TypedArray styledAttributes = getContext().getTheme().obtainStyledAttributes(new int[]{android.R.attr.actionBarSize});
            int size = (int) styledAttributes.getDimension(0, 0);
            return size;
        }

        private void playAudio(String url)
        {
            killMediaPlayer();



            try {
                mediaPlayer = new MediaPlayer();
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

                mediaPlayer.setDataSource(url);
                mediaPlayer.prepare();


                // might take long! (for buffering, etc)
                mediaPlayer.start();
                seekBar.setMax(mediaPlayer.getDuration());

                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        pause_btn.setVisibility(View.GONE);
                        play_btn.setVisibility(View.VISIBLE);
                    }
                });

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        int currentPosition = mediaPlayer.getCurrentPosition();
                        int total = mediaPlayer.getDuration();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            seekBar.setProgress(0, true);
                        } else {
                            seekBar.setProgress(0);
                        }

                        while (mediaPlayer != null && mediaPlayer.isPlaying() && currentPosition < total) {
                            try {
                                Thread.sleep(1000);
                                currentPosition = mediaPlayer.getCurrentPosition();
                            } catch (InterruptedException e) {
                                return;
                            } catch (Exception e) {
                                return;
                            }


                            if (!mediaPlayer.isPlaying()) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    seekBar.setProgress(0, true);
                                } else {
                                    seekBar.setProgress(0);
                                }

                            } else {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    seekBar.setProgress(currentPosition, true);
                                } else {
                                    seekBar.setProgress(currentPosition);
                                }
                            }
                            if (seekBar.getProgress() == total) {
                                play_btn.setVisibility(View.VISIBLE);
                                pause_btn.setVisibility(View.GONE);
                            }
                        }
                    }
                }).start();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }


        private void killMediaPlayer() {
            if(mediaPlayer!=null) {
                try {
                    mediaPlayer.release();
                    seekBar.setProgress(0);
                }
                catch(Exception e) {
                    e.printStackTrace();
                }
            }
        }




    }


}
