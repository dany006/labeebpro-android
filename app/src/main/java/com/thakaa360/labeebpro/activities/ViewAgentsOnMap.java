package com.thakaa360.labeebpro.activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.Bundler;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import com.thakaa360.labeebpro.R;
import com.thakaa360.labeebpro.fragments.FragmentViewAgentsAsList;
import com.thakaa360.labeebpro.fragments.FragmentViewAgentsonMap;
import com.thakaa360.labeebpro.server.client.ApiClient;
import com.thakaa360.labeebpro.server.interfaces.Assign_Agent_Interface;
import com.thakaa360.labeebpro.server.interfaces.GetAllAgents;
import com.thakaa360.labeebpro.server.modals.AGENT_MODAL_FOR_ASSIGNING;
import com.thakaa360.labeebpro.server.modals.MSG_MODAL;
import com.thakaa360.labeebpro.utilities.Constants;
import com.thakaa360.labeebpro.utilities.LayoutUtility;
import com.thakaa360.labeebpro.utilities.Major;
import com.thakaa360.labeebpro.utilities.Tools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewAgentsOnMap extends Major{

    private Toolbar toolbar;
    static  Context context;
    private Double
            request_lat,
            request_lon;
    private int
            company_id,
            request_subcategory;
    private int status;
    private static int request_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View contentView = LayoutUtility.getLayout(this, R.layout.activity_view_agents_on_map);
        mDrawer.addView(contentView, 0);
        navigationView.setCheckedItem(R.id.dashboard_item);

        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Agents");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawer.openDrawer(GravityCompat.START);
            }
        });

        context = ViewAgentsOnMap.this;

        request_lat = getIntent().getExtras().getDouble("request_lat") ;
        request_lon = getIntent().getExtras().getDouble("request_lon") ;
        company_id = getIntent().getExtras().getInt("company_id") ;
        request_subcategory = getIntent().getExtras().getInt("request_subcategory");
        status = getIntent().getExtras().getInt("status");

        request_id = getIntent().getExtras().getInt("request_id");

        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
                getSupportFragmentManager(), FragmentPagerItems.with(this)
                .add("Map", FragmentViewAgentsonMap.class,
                        new Bundler().
                                putDouble("request_lat",request_lat).
                                putDouble("request_lon",request_lon).
                                putInt("company_id",company_id).
                putInt("request_subcategory",request_subcategory).putInt("status",status).putInt("request_id",request_id).get())
                .add("List", FragmentViewAgentsAsList.class,
                        new Bundler().
                                putDouble("request_lat",request_lat).
                                putDouble("request_lon",request_lon).
                                putInt("company_id",company_id).
                                putInt("request_subcategory",request_subcategory).putInt("status",status).putInt("request_id",request_id).get())
                .create());

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(adapter);

        SmartTabLayout viewPagerTab = (SmartTabLayout) findViewById(R.id.viewpagertab);
        viewPagerTab.setViewPager(viewPager);


    }



}
