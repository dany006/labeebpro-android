package com.thakaa360.labeebpro.activities;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.maps.android.PolyUtil;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.SubscriptionEventListener;
import com.thakaa360.labeebpro.R;
import com.thakaa360.labeebpro.Tracker.LatLngInterpolator;
import com.thakaa360.labeebpro.Tracker.MarkerAnimation;
import com.thakaa360.labeebpro.modals.PUSHER_MODAL;
import com.thakaa360.labeebpro.server.client.ApiClient;
import com.thakaa360.labeebpro.server.interfaces.AgentDetailInterface;
import com.thakaa360.labeebpro.server.interfaces.InterfaceMap;
import com.thakaa360.labeebpro.server.interfaces.UserData_Interface;
import com.thakaa360.labeebpro.server.modals.AGENT_DATA_MODAL;
import com.thakaa360.labeebpro.server.modals.AGENT_DETAILS;
import com.thakaa360.labeebpro.server.modals.DirectionParser;
import com.thakaa360.labeebpro.utilities.Constants;
import com.thakaa360.labeebpro.utilities.LayoutUtility;
import com.thakaa360.labeebpro.utilities.Major;
import com.thakaa360.labeebpro.utilities.Tools;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.thakaa360.labeebpro.helpers.HttpParsing.getUnsafeOkHttpClient;

public class AgentOnMap extends Major {

    private GoogleMap mMap;
    private BottomSheetBehavior bottomSheetBehavior;
    int agent_id;
    double request_lat,
            request_lng;
    PolylineOptions rectLine;
    Toolbar toolbar;
   // private List<Agent_Data_Model> agentData ;
    LatLng sydney1;
    final Marker[] marker = new Marker[1];
    CircleImageView fab;


    TextView
            agent_name,
            agent_rating_value,
            agent_number;
    AppCompatRatingBar
            ratingBar;
    LinearLayout
            agent_number_dialer;
    LatLng
            reqLatLng;
    View contentView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         contentView = LayoutUtility.getLayout(this, R.layout.activity_agent_on_map);

        mDrawer.addView(contentView, 0);

        toolbar = contentView.findViewById(R.id.toolbar2);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawer.openDrawer(GravityCompat.START);
            }
        });

        agent_id = getIntent().getExtras().getInt(Constants.AGENT_ID);

        reqLatLng = new LatLng(getIntent().getExtras().getDouble(Constants.REQ_LAT),getIntent().getExtras().getDouble(Constants.REQ_LNG));

        request_lat = getIntent().getExtras().getDouble(Constants.REQ_LAT);
        request_lng = getIntent().getExtras().getDouble(Constants.REQ_LNG);
        initMapFragment();
        initComponent();
     //   Toast.makeText(this, "Swipe up bottom sheet", Toast.LENGTH_SHORT).show();
    }


    private void initComponent() {
        // get the bottom sheet view
        LinearLayout llBottomSheet = (LinearLayout) contentView.findViewById(R.id.bottom_sheet);

        agent_name = llBottomSheet.findViewById(R.id.agent_name);
        agent_number = llBottomSheet.findViewById(R.id.agent_number);
        agent_number_dialer = llBottomSheet.findViewById(R.id.agent_number_dialler);

        agent_rating_value = llBottomSheet.findViewById(R.id.agent_rating_values);

        ratingBar = llBottomSheet.findViewById(R.id.agent_rating);

        // init the bottom sheet behavior
        bottomSheetBehavior = BottomSheetBehavior.from(llBottomSheet);

        // change the state of the bottom sheet
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

        // set callback for changes
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {

            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        fab =  contentView.findViewById(R.id.fab_directions);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                try {
                  //  mMap.animateCamera(zoomingLocation());
                } catch (Exception e) {
                }
            }
        });
    }

    private void initMapFragment() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = Tools.configActivityMaps(googleMap);
                try {
                    // Customise the styling of the base map using a JSON object defined
                    // in a raw resource file.
                    boolean success = googleMap.setMapStyle(
                            MapStyleOptions.loadRawResourceStyle(
                                    AgentOnMap.this, R.raw.custom_map));

                    if (!success) {
                        Log.e("MapsActivityRaw", "Style parsing failed.");
                    }
                } catch (Resources.NotFoundException e) {
                    Log.e("MapsActivityRaw", "Can't find style.", e);
                }
                AgentDetailInterface agentDetailInterface = ApiClient.getApiClient().create(AgentDetailInterface.class);
                Call<AGENT_DETAILS> call = agentDetailInterface.getAgentDetail(
                        Constants.AUTHORIZATION_KEY,
                        agent_id
                );

                mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.defaultMarker()).title("Your Request Location").position(reqLatLng));
                call.enqueue(new Callback<AGENT_DETAILS>() {
                    @Override
                    public void onResponse(Call<AGENT_DETAILS> call, final Response<AGENT_DETAILS> response) {
                        if (response.isSuccessful()){

                            try {
                                Glide.with(AgentOnMap.this).
                                        load(Constants.PROFILE_Url+response.body().getAgentProfile().get(0).getAgentAvatarUrl()).
                                        apply(new RequestOptions().placeholder(R.drawable.ic_placeholders_05).error(R.drawable.ic_placeholders_03).diskCacheStrategy(DiskCacheStrategy.RESOURCE))
                                        .into(fab);
                            }catch (IllegalArgumentException ex){

                            }
                            catch (NullPointerException e){

                            }

                            agent_name.setText(response.body().getAgentProfile().get(0).getAgentName());
                            agent_number.setText(response.body().getAgentProfile().get(0).getAgentNumber());
                            agent_number_dialer.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(Intent.ACTION_DIAL);
                                    intent.setData(Uri.parse("tel:"+response.body().getAgentProfile().get(0).getAgentNumber()));
                                    startActivity(intent);
                                }
                            });
                            agent_rating_value.setText(response.body().getAgentProfile().get(0).getAgentRating()+"");
                            ratingBar.setRating(response.body().getAgentProfile().get(0).getAgentRating());


                            MarkerOptions markerOptions = new MarkerOptions().position(new LatLng(response.body().getAgentProfile().get(0).getAgentLat(), response.body().getAgentProfile().get(0).getAgentLon())).icon(BitmapDescriptorFactory.fromBitmap(Tools.getBitmapFromVectorDrawable(AgentOnMap.this,R.drawable.marker_icon)));
                            marker[0]=  mMap.addMarker(markerOptions);
                             //= mMap.addMarker(new MarkerOptions().position(new LatLng(response.body().getAgentProfile().get(0).getAgentLat(), response.body().getAgentProfile().get(0).getAgentLon())).title(response.body().getAgentProfile().get(0).getAgentName()).icon(BitmapDescriptorFactory.defaultMarker()));

                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(response.body().getAgentProfile().get(0).getAgentLat(), response.body().getAgentProfile().get(0).getAgentLon()), 13));
                            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                                @Override
                                public boolean onMarkerClick(Marker marker) {
                                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                                    try {
                                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(response.body().getAgentProfile().get(0).getAgentLat(), response.body().getAgentProfile().get(0).getAgentLon()), 13));
                                    } catch (Exception e) {
                                    }
                                    return true;
                                }
                            });




                            rectLine = new PolylineOptions();
                            String base_url = "https://maps.googleapis.com/";

                            final Retrofit retrofit = new Retrofit.Builder()
                                    .baseUrl(base_url).client(getUnsafeOkHttpClient()).addConverterFactory(GsonConverterFactory.create())
                                    .build();


                            InterfaceMap reqinterface = retrofit.create(InterfaceMap.class);
                                String origin = request_lat + "," + request_lng;
                                String dest = response.body().getAgentProfile().get(0).getAgentLat() + "," +
                                        response.body().getAgentProfile().get(0).getAgentLon();
                                String key = "AIzaSyBKCT2rEx2atUwKSUa44TxVuo-5oxBeA8U";
                                Call<DirectionParser> call1 = reqinterface.getData(origin, dest, key);
                                //       Toast.makeText(getApplicationContext(), origin + " " + dest.toString() + " " + key, Toast.LENGTH_SHORT).show();
                                call1.enqueue(new Callback<DirectionParser>() {
                                    @Override
                                    public void onResponse(Call<DirectionParser> call, Response<DirectionParser> response) {
                                        if (response.isSuccessful()) {
                                            int s = response.body().getRoutes().get(0).getLegs().get(0).getSteps().size();
                                            String[] polylines = new String[s];
                                            for (int i = 0; i < s; i++) {
                                                String polyline = response.body().
                                                        getRoutes().
                                                        get(0).
                                                        getLegs().
                                                        get(0).
                                                        getSteps().
                                                        get(i).getPolyline().getPoints();
                                                polylines[i] = polyline;
                                            }
                                            for (int i = 0; i < s; i++) {

                                                rectLine.width(10);
                                                rectLine.color(Color.RED);
                                                rectLine.addAll(PolyUtil.decode(polylines[i]));
                                                // List<LatLng>
                                                //Toast.makeText()
                                                mMap.addPolyline(rectLine);

                                            }
                                        } else {
                                            Toast.makeText(getApplicationContext(), response.message(), Toast.LENGTH_SHORT).show();

                                        }


                                    }

                                    @Override
                                    public void onFailure(Call<DirectionParser> call, Throwable t) {
                                        Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                });




                        }
                        else{
                            Toast.makeText(AgentOnMap.this,response.message(),Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<AGENT_DETAILS> call, Throwable t) {
                        Toast.makeText(AgentOnMap.this,t.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });



                // Add a marker in Sydney and move the camera
                PusherOptions options = new PusherOptions();
                options.setCluster("ap2");
                Pusher pusher = new Pusher("e5353a3054d6415263d0", options);
                pusher.connect();
                Channel channel = pusher.subscribe("test-channel");

                channel.bind("test-event", new SubscriptionEventListener() {
                    @Override
                    public void onEvent(String channelName, String eventName, final String data) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                              Gson gson = new Gson();
                                PUSHER_MODAL msg = gson.fromJson(data,PUSHER_MODAL.class);
                                double lat = msg.getLat();
                                double lng = msg.getLon();
                                LatLngInterpolator latLngInterpolator = new LatLngInterpolator.Spherical();
                                MarkerAnimation.animateMarkerToGB(marker[0], new LatLng(lat,lng), latLngInterpolator);









                            }
                        });
                    }
                });
            }
        });
    }

    private CameraUpdate zoomingLocation() {
        return CameraUpdateFactory.newLatLngZoom(new LatLng(37.76496792, -122.42206407), 13);
    }

}
