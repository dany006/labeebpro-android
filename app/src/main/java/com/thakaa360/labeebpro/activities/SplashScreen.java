package com.thakaa360.labeebpro.activities;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.thakaa360.labeebpro.AgentDashboard;
import com.thakaa360.labeebpro.R;
import com.thakaa360.labeebpro.utilities.Constants;

public class SplashScreen extends AppCompatActivity {

    Thread thread;
    Handler handler;

    SQLiteDatabase sqLiteDatabase;
    Cursor cursor;
    int session_code;
    int user_role;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        // mcontext = getApplicationContext();

        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_LOW_PROFILE;
        decorView.setSystemUiVisibility(uiOptions);

        thread = new Thread(new mythread());

        handler = new Handler() {

            @Override
            public void handleMessage(Message msg) {


            }



        };
        thread.start();




    }

    class mythread implements Runnable{
        @Override
        public void run() {

            for (int i = 0;i<= 100;i++){
                Message message = Message.obtain();
                message.arg1 = i;

                handler.sendMessage(message);
                try {
                    thread.sleep(30);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }


            }

            if (!getApplicationContext().getDatabasePath(Constants.DB_NAME).exists()){
                startActivity(new Intent(SplashScreen.this,Login.class));
                overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                finishAffinity();
            }
            else{
                sqLiteDatabase = openOrCreateDatabase(Constants.DB_NAME,MODE_PRIVATE,null);
                cursor = sqLiteDatabase.rawQuery(Constants.GET_PROFILE,null);
                cursor.moveToFirst();
                user_role = cursor.getInt(cursor.getColumnIndex(Constants.DB_ROLE));
                session_code = cursor.getInt(cursor.getColumnIndex(Constants.DB_SESSION_STAT));
                if (session_code == 0){
                    startActivity(new Intent(SplashScreen.this,Login.class));
                    overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                    finishAffinity();
                    sqLiteDatabase.close();
                }
                else if(session_code ==1){
                    switch (user_role){
                        case 2:
                            startActivity(new Intent(SplashScreen.this,CompanyDashboard.class));
                            overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                            finishAffinity();
                            sqLiteDatabase.close();
                            break;
                        case 3:
                            startActivity(new Intent(SplashScreen.this,AgentDashboard.class));
                            overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                            finishAffinity();
                            sqLiteDatabase.close();
                            break;
                    }
                }
            }








        }
    }
}
