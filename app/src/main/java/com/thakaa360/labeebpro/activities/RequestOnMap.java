package com.thakaa360.labeebpro.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;
import com.thakaa360.labeebpro.AgentDashboard;
import com.thakaa360.labeebpro.R;
import com.thakaa360.labeebpro.server.client.ApiClient;
import com.thakaa360.labeebpro.server.interfaces.Agent_Job_Interface;
import com.thakaa360.labeebpro.server.interfaces.InterfaceMap;
import com.thakaa360.labeebpro.server.interfaces.RespondCustomer;
import com.thakaa360.labeebpro.server.interfaces.UserData_Interface;
import com.thakaa360.labeebpro.server.modals.AGENTS_TASKS;
import com.thakaa360.labeebpro.server.modals.AGENT_DATA_MODAL;
import com.thakaa360.labeebpro.server.modals.DirectionParser;
import com.thakaa360.labeebpro.server.modals.MSG_MODAL;
import com.thakaa360.labeebpro.utilities.Constants;
import com.thakaa360.labeebpro.utilities.LayoutUtility;
import com.thakaa360.labeebpro.utilities.Major;
import com.thakaa360.labeebpro.utilities.Tools;

import org.w3c.dom.Text;

import java.util.List;

import io.paperdb.Paper;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.thakaa360.labeebpro.helpers.HttpParsing.getUnsafeOkHttpClient;

public class RequestOnMap extends Major {

    private   Animator mCurrentAnimator;

    private AGENTS_TASKS modal;
    // The system "short" animation time duration, in milliseconds. This
    // duration is ideal for subtle animations or animations that occur
    // very frequently.
    private  int mShortAnimationDuration;

    static ProgressDialog progressDoalog;
    private GoogleMap mMap;
    private BottomSheetBehavior bottomSheetBehavior;
    private SQLiteDatabase sqLiteDatabase;
    private Cursor cursor;
    private static int user_id;
    EditText bidAmount;
    FloatingActionButton floatingActionButton;

    PolylineOptions rectLine;
    double Agent_lat,Agent_Lng,Request_lat,Request_Lng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final View contentView = LayoutUtility.getLayout(this, R.layout.activity_request_on_map);
        mDrawer.addView(contentView, 0);

        toolbar = findViewById(R.id.toolbar2);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawer.openDrawer(GravityCompat.START);
            }
        });

        sqLiteDatabase = context.openOrCreateDatabase(Constants.DB_NAME, Context.MODE_PRIVATE,null);
        cursor = sqLiteDatabase.rawQuery(Constants.GET_PROFILE,null);
        cursor.moveToFirst();

        user_id = cursor.getInt(cursor.getColumnIndex(Constants.DB_USER_ID));
        sqLiteDatabase.close();

        Paper.init(context);

        progressDoalog = new ProgressDialog(context);
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.setTitle("Updating");

        modal = Paper.book().read(Constants.REQUEST_MODAL_DATA);

        initMapFragment(modal);
        initComponent(contentView);



    }


    private void initComponent(final View contentView) {
        // get the bottom sheet view
        LinearLayout llBottomSheet = (LinearLayout) findViewById(R.id.bottom_sheet);

        TextView customer_name,customer_number,request_address;
        final LinearLayout dialer;
        LinearLayout update_bid;
        LinearLayout status_layout;
        TextView status_txt;

        final MaterialStyledDialog materialDialog;
        View customView  = LayoutUtility.getLayout(context,R.layout.create_bid_dialogue);
        bidAmount = customView.findViewById(R.id.bid_amount_field);
        materialDialog = new MaterialStyledDialog.Builder(context)
                .setTitle("Update Bid")
                .setIcon(R.drawable.profile_dummy)
                .withIconAnimation(true).setCustomView(customView)
                .setDescription("Update the bid amount ")
                .setPositiveText("Update").setCancelable(false)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        Log.d("MaterialStyledDialogs", "Do something!");
                        String datavalue = bidAmount.getText().toString();
                        progressDoalog.show();

                        if (!datavalue.equals("")) {
                            RespondCustomer respondCustomer = ApiClient.getApiClient().create(RespondCustomer.class);
                            Call<MSG_MODAL> call = respondCustomer.update_Bid(
                                    modal.getId(),
                                    user_id,
                                    Double.parseDouble(datavalue),
                                    1
                            );
                            call.enqueue(new Callback<MSG_MODAL>() {
                                @Override
                                public void onResponse(Call<MSG_MODAL> call, Response<MSG_MODAL> response) {
                                    progressDoalog.dismiss();
                                    if (response.isSuccessful()) {
                                        Toast.makeText(context, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(context, response.message(), Toast.LENGTH_LONG).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<MSG_MODAL> call, Throwable t) {
                                    progressDoalog.dismiss();
                                    Toast.makeText(context, t.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            });
                            dialog.dismiss();
                        }
                    }
                }).build();



        customer_name = llBottomSheet.findViewById(R.id.customer_name);
        customer_number = llBottomSheet.findViewById(R.id.customer_number);
        request_address = llBottomSheet.findViewById(R.id.request_address);

        status_layout = llBottomSheet.findViewById(R.id.status_layout);
        status_txt = llBottomSheet.findViewById(R.id.status_txt);

        dialer = llBottomSheet.findViewById(R.id.dialer);
        update_bid = llBottomSheet.findViewById(R.id.update_bid);

        final ImageView thumb1View = llBottomSheet.findViewById(R.id.req_thumb);

        try {
            Glide.with(context).
                    load(Constants.REQ_IMAGE_BASE_URL+modal.getAvatar()).
                    apply(new RequestOptions().placeholder(R.drawable.ic_placeholders_05).error(R.drawable.ic_placeholders_03).diskCacheStrategy(DiskCacheStrategy.RESOURCE))
                    .into(thumb1View);
        }catch (IllegalArgumentException ex){

        }
        catch (NullPointerException e){

        }

        thumb1View.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                zoomImageFromThumb(thumb1View, modal.getAvatar(),contentView);
            }
        });


        customer_name.setText(modal.getCus_name());
        request_address.setText(modal.getAddress());
        customer_number.setText(modal.getCus_num());

        floatingActionButton = findViewById(R.id.fab_directions);
        dialer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+modal.getCus_num()));
                startActivity(intent);
            }
        });

        if (modal.getStatus() == 4){

            floatingActionButton.setBackgroundTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.app_yellow)));

            status_txt.setText("Arrived");
            status_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    progressDoalog.show();
                    Agent_Job_Interface agent_job_interface = ApiClient.getApiClient().create(Agent_Job_Interface.class);
                    Call<MSG_MODAL> call = agent_job_interface.update_agent_status(
                            RequestBody.create(MultipartBody.FORM,modal.getId()+""),
                            RequestBody.create(MultipartBody.FORM,5+""),

                            RequestBody.create(MultipartBody.FORM,0+"")
                    );

                    progressDoalog.show();
                    call.enqueue(new Callback<MSG_MODAL>() {
                        @Override
                        public void onResponse(Call<MSG_MODAL> call, Response<MSG_MODAL> response) {
                            if (response.isSuccessful()){
                                progressDoalog.dismiss();

                                modal.setStatus(5);

                                Paper.book().write(Constants.REQUEST_MODAL_DATA,modal);
                                startActivity(new Intent(context, RequestOnMap.class));
                                finishAffinity();
                                overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);

                            }
                            else{
                                Toast.makeText(context,response.message(),Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<MSG_MODAL> call, Throwable t) {
                            progressDoalog.dismiss();
                            Toast.makeText(context,t.getMessage(),Toast.LENGTH_LONG).show();

                        }
                    });
                }
            });


        }
        else if(modal.getStatus() == 5){
            floatingActionButton.setBackgroundTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.app_green)));
            status_txt.setText("Start Work");
            status_layout.setBackgroundResource(R.drawable.call_btn_rounded);
            status_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    progressDoalog.show();
                    Agent_Job_Interface agent_job_interface = ApiClient.getApiClient().create(Agent_Job_Interface.class);
                    Call<MSG_MODAL> call = agent_job_interface.update_agent_status(
                            RequestBody.create(MultipartBody.FORM,modal.getId()+""),
                            RequestBody.create(MultipartBody.FORM,6+""),

                            RequestBody.create(MultipartBody.FORM,0+"")
                    );

                    progressDoalog.show();
                    call.enqueue(new Callback<MSG_MODAL>() {
                        @Override
                        public void onResponse(Call<MSG_MODAL> call, Response<MSG_MODAL> response) {
                            if (response.isSuccessful()){
                                progressDoalog.dismiss();

                                modal.setStatus(6);

                                Paper.book().write(Constants.REQUEST_MODAL_DATA,modal);
                                startActivity(new Intent(context, RequestOnMap.class));
                                finishAffinity();
                                overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);

                            }
                            else{
                                Toast.makeText(context,response.message(),Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<MSG_MODAL> call, Throwable t) {
                            progressDoalog.dismiss();
                            Toast.makeText(context,t.getMessage(),Toast.LENGTH_LONG).show();

                        }
                    });
                }
            });
            update_bid.setVisibility(View.VISIBLE);
            update_bid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    materialDialog.show();
                }
            });
        }
        else if(modal.getStatus() == 6){
            floatingActionButton.setBackgroundTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.light_blue_200)));
            status_txt.setText("Finish Work");
            update_bid.setVisibility(View.GONE);
            status_layout.setBackgroundResource(R.drawable.finish_btn_rounded);
            status_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    progressDoalog.show();
                    Agent_Job_Interface agent_job_interface = ApiClient.getApiClient().create(Agent_Job_Interface.class);
                    Call<MSG_MODAL> call = agent_job_interface.update_agent_status(
                            RequestBody.create(MultipartBody.FORM,modal.getId()+""),
                            RequestBody.create(MultipartBody.FORM,7+""),

                            RequestBody.create(MultipartBody.FORM,1+"")
                    );

                    progressDoalog.show();
                    call.enqueue(new Callback<MSG_MODAL>() {
                        @Override
                        public void onResponse(Call<MSG_MODAL> call, Response<MSG_MODAL> response) {
                            if (response.isSuccessful()){
                                progressDoalog.dismiss();

                                modal.setStatus(7);

                                Paper.book().write(Constants.REQUEST_MODAL_DATA,modal);
                                startActivity(new Intent(context, AgentDashboard.class));
                                finishAffinity();
                                overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);

                            }
                            else{
                                Toast.makeText(context,response.message(),Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<MSG_MODAL> call, Throwable t) {
                            progressDoalog.dismiss();
                            Toast.makeText(context,t.getMessage(),Toast.LENGTH_LONG).show();

                        }
                    });
                }
            });
            update_bid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    materialDialog.show();
                }
            });
        }


        // Retrieve and cache the system's default "short" animation time.
        mShortAnimationDuration = getResources().getInteger(
                android.R.integer.config_shortAnimTime);


        // init the bottom sheet behavior
        bottomSheetBehavior = BottomSheetBehavior.from(llBottomSheet);

        // change the state of the bottom sheet
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

        // set callback for changes
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {

            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });




        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);


                UserData_Interface userData_interface = ApiClient.getApiClient().create(UserData_Interface.class);

                RequestBody userID = RequestBody.create(MultipartBody.FORM,user_id+"");

                Call<List<AGENT_DATA_MODAL>> call = userData_interface.getAgentById(
                        userID
                );

                progressDoalog.show();

                call.enqueue(new Callback<List<AGENT_DATA_MODAL>>() {
                    @Override
                    public void onResponse(Call<List<AGENT_DATA_MODAL>> call, Response<List<AGENT_DATA_MODAL>> response) {
                        progressDoalog.dismiss();

                        if (response.isSuccessful()){

                            Uri gmmIntentUri = Uri.parse("http://maps.google.com/maps?saddr="+response.body().get(0).getAgent_lat()+","+ response.body().get(0).getAgent_lon()+"&daddr="+modal.getLat()+","+modal.getLon());
                            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                            mapIntent.setPackage("com.google.android.apps.maps");
                            if (mapIntent.resolveActivity(getPackageManager()) != null) {
                                startActivity(mapIntent);
                            }
                            else{
                                Toast.makeText(getApplicationContext(),"Please install google map",Toast.LENGTH_SHORT).show();
                            }
                            try {
                                mMap.animateCamera(zoomingLocation());
                            } catch (Exception e) {
                            }
                        }
                        else{
                            Toast.makeText(RequestOnMap.this,response.message(),Toast.LENGTH_LONG).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<List<AGENT_DATA_MODAL>> call, Throwable t) {
                        progressDoalog.dismiss();
                        Toast.makeText(RequestOnMap.this,t.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });

            }
        });
    }

    private void initMapFragment(final AGENTS_TASKS modal) {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {

                try {
                    // Customise the styling of the base map using a JSON object defined
                    // in a raw resource file.
                    boolean success = googleMap.setMapStyle(
                            MapStyleOptions.loadRawResourceStyle(
                                    RequestOnMap.this, R.raw.custom_map));

                    if (!success) {
                        Log.e("MapsActivityRaw", "Style parsing failed.");
                    }
                } catch (Resources.NotFoundException e) {
                    Log.e("MapsActivityRaw", "Can't find style.", e);
                }
                mMap = Tools.configActivityMaps(googleMap);
                rectLine = new PolylineOptions();
                String base_url = "https://maps.googleapis.com/";

                final Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(base_url).client(getUnsafeOkHttpClient()).addConverterFactory(GsonConverterFactory.create())
                        .build();

                UserData_Interface userData_interface = ApiClient.getApiClient().create(UserData_Interface.class);

                RequestBody userID = RequestBody.create(MultipartBody.FORM,user_id+"");

                Call<List<AGENT_DATA_MODAL>> call1 = userData_interface.getAgentById(
                        userID
                );

                progressDoalog.show();

                call1.enqueue(new Callback<List<AGENT_DATA_MODAL>>() {
                    @Override
                    public void onResponse(Call<List<AGENT_DATA_MODAL>> call1, Response<List<AGENT_DATA_MODAL>> response) {
                        progressDoalog.dismiss();

                        if (response.isSuccessful()){

                            Agent_lat = response.body().get(0).getAgent_lat();
                            Agent_Lng = response.body().get(0).getAgent_lon();

                            mMap.addMarker(new MarkerOptions().position(new LatLng(Agent_lat,Agent_Lng)).title("My Location").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

                            InterfaceMap reqinterface = retrofit.create(InterfaceMap.class);
                            if (modal.getLat()!=0.0 && modal.getLon()!=0.0 && Agent_lat !=0.0 && Agent_Lng!=0.0) {
                                String origin = modal.getLat() + "," + modal.getLon();
                                String dest = Agent_lat + "," + Agent_Lng;
                                String key = "AIzaSyBKCT2rEx2atUwKSUa44TxVuo-5oxBeA8U";
                                Call<DirectionParser> call = reqinterface.getData(origin, dest, key);
                                //       Toast.makeText(getApplicationContext(), origin + " " + dest.toString() + " " + key, Toast.LENGTH_SHORT).show();
                                call.enqueue(new Callback<DirectionParser>() {
                                    @Override
                                    public void onResponse(Call<DirectionParser> call, Response<DirectionParser> response) {
                                        if (response.isSuccessful()) {
                                            int s = response.body().getRoutes().get(0).getLegs().get(0).getSteps().size();
                                            String[] polylines = new String[s];
                                            for (int i = 0; i < s; i++) {
                                                String polyline = response.body().
                                                        getRoutes().
                                                        get(0).
                                                        getLegs().
                                                        get(0).
                                                        getSteps().
                                                        get(i).getPolyline().getPoints();
                                                polylines[i] = polyline;
                                            }
                                            for (int i = 0; i < s; i++) {

                                                rectLine.width(10);
                                                rectLine.color(Color.RED);
                                                rectLine.addAll(PolyUtil.decode(polylines[i]));
                                                // List<LatLng>
                                                //Toast.makeText()
                                                mMap.addPolyline(rectLine);

                                            }
                                        } else {
                                            Toast.makeText(getApplicationContext(), response.message(), Toast.LENGTH_SHORT).show();

                                        }


                                    }

                                    @Override
                                    public void onFailure(Call<DirectionParser> call, Throwable t) {
                                        Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }

                        }
                        else{
                            Toast.makeText(RequestOnMap.this,response.message(),Toast.LENGTH_LONG).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<List<AGENT_DATA_MODAL>> call1, Throwable t) {
                        progressDoalog.dismiss();
                        Toast.makeText(RequestOnMap.this,t.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });





                MarkerOptions markerOptions = new MarkerOptions().position(new LatLng(modal.getLat(), modal.getLon()));
                mMap.addMarker(markerOptions);
                mMap.moveCamera(zoomingLocation());
                mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        try {
                            mMap.animateCamera(zoomingLocation());
                        } catch (Exception e) {
                        }
                        return true;
                    }
                });
            }
        });
    }

    private CameraUpdate zoomingLocation() {
        return CameraUpdateFactory.newLatLngZoom(new LatLng(modal.getLat(), modal.getLon()), 13);
    }
    public  void zoomImageFromThumb(final View thumbView, String imageResId,View contentView) {
        // If there's an animation in progress, cancel it
        // immediately and proceed with this one.
        if (mCurrentAnimator != null) {
            mCurrentAnimator.cancel();
        }

        // Load the high-resolution "zoomed-in" image.
        final ImageView expandedImageView = (ImageView) contentView.findViewById(
                R.id.expanded_image);
        try {
            Glide.with(context).
                    load(Constants.REQ_IMAGE_BASE_URL+imageResId).
                    apply(new RequestOptions().placeholder(R.drawable.profile_dummy).error(R.drawable.profile_dummy).diskCacheStrategy(DiskCacheStrategy.RESOURCE))
                    .into(expandedImageView);
        }catch (IllegalArgumentException ex){

        }
        catch (NullPointerException e){

        }

        // Calculate the starting and ending bounds for the zoomed-in image.
        // This step involves lots of math. Yay, math.
        final Rect startBounds = new Rect();
        final Rect finalBounds = new Rect();
        final Point globalOffset = new Point();

        // The start bounds are the global visible rectangle of the thumbnail,
        // and the final bounds are the global visible rectangle of the container
        // view. Also set the container view's offset as the origin for the
        // bounds, since that's the origin for the positioning animation
        // properties (X, Y).
        thumbView.getGlobalVisibleRect(startBounds);
        contentView.findViewById(R.id.container)
                .getGlobalVisibleRect(finalBounds, globalOffset);
        startBounds.offset(-globalOffset.x, -globalOffset.y);
        finalBounds.offset(-globalOffset.x, -globalOffset.y);

        // Adjust the start bounds to be the same aspect ratio as the final
        // bounds using the "center crop" technique. This prevents undesirable
        // stretching during the animation. Also calculate the start scaling
        // factor (the end scaling factor is always 1.0).
        float startScale;
        if ((float) finalBounds.width() / finalBounds.height()
                > (float) startBounds.width() / startBounds.height()) {
            // Extend start bounds horizontally
            startScale = (float) startBounds.height() / finalBounds.height();
            float startWidth = startScale * finalBounds.width();
            float deltaWidth = (startWidth - startBounds.width()) / 2;
            startBounds.left -= deltaWidth;
            startBounds.right += deltaWidth;
        } else {
            // Extend start bounds vertically
            startScale = (float) startBounds.width() / finalBounds.width();
            float startHeight = startScale * finalBounds.height();
            float deltaHeight = (startHeight - startBounds.height()) / 2;
            startBounds.top -= deltaHeight;
            startBounds.bottom += deltaHeight;
        }

        // Hide the thumbnail and show the zoomed-in view. When the animation
        // begins, it will position the zoomed-in view in the place of the
        // thumbnail.
        thumbView.setAlpha(0f);
        expandedImageView.setVisibility(View.VISIBLE);

        // Set the pivot point for SCALE_X and SCALE_Y transformations
        // to the top-left corner of the zoomed-in view (the default
        // is the center of the view).
        expandedImageView.setPivotX(0f);
        expandedImageView.setPivotY(0f);

        // Construct and run the parallel animation of the four translation and
        // scale properties (X, Y, SCALE_X, and SCALE_Y).
        AnimatorSet set = new AnimatorSet();
        set
                .play(ObjectAnimator.ofFloat(expandedImageView, View.X,
                        startBounds.left, finalBounds.left))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
                        startBounds.top, finalBounds.top))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X,
                        startScale, 1f))
                .with(ObjectAnimator.ofFloat(expandedImageView,
                        View.SCALE_Y, startScale, 1f));
        set.setDuration(mShortAnimationDuration);
        set.setInterpolator(new DecelerateInterpolator());
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mCurrentAnimator = null;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                mCurrentAnimator = null;
            }
        });
        set.start();
        mCurrentAnimator = set;

        // Upon clicking the zoomed-in image, it should zoom back down
        // to the original bounds and show the thumbnail instead of
        // the expanded image.
        final float startScaleFinal = startScale;
        expandedImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCurrentAnimator != null) {
                    mCurrentAnimator.cancel();
                }

                // Animate the four positioning/sizing properties in parallel,
                // back to their original values.
                AnimatorSet set = new AnimatorSet();
                set.play(ObjectAnimator
                        .ofFloat(expandedImageView, View.X, startBounds.left))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.Y,startBounds.top))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.SCALE_X, startScaleFinal))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.SCALE_Y, startScaleFinal));
                set.setDuration(mShortAnimationDuration);
                set.setInterpolator(new DecelerateInterpolator());
                set.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        thumbView.setAlpha(1f);
                        expandedImageView.setVisibility(View.GONE);
                        mCurrentAnimator = null;
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        thumbView.setAlpha(1f);
                        expandedImageView.setVisibility(View.GONE);
                        mCurrentAnimator = null;
                    }
                });
                set.start();
                mCurrentAnimator = set;
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(context,AgentDashboard.class));
        finishAffinity();
        overridePendingTransition(android.R.anim.slide_in_left,android.R.anim.slide_out_right);
    }
}
