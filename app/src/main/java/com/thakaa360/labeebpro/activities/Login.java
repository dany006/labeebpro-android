package com.thakaa360.labeebpro.activities;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.SystemClock;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.thakaa360.labeebpro.AgentDashboard;
import com.thakaa360.labeebpro.R;
import com.thakaa360.labeebpro.server.client.ApiClient;
import com.thakaa360.labeebpro.server.interfaces.FCM_TOKEN_INTERFACE;
import com.thakaa360.labeebpro.server.interfaces.Get_Status;
import com.thakaa360.labeebpro.server.interfaces.LoginInterface;
import com.thakaa360.labeebpro.server.interfaces.Update_SessionCode;
import com.thakaa360.labeebpro.server.interfaces.UserData_Interface;
import com.thakaa360.labeebpro.server.modals.AGENT_DATA_MODAL;
import com.thakaa360.labeebpro.server.modals.COMPANY_DATA_MODAL;
import com.thakaa360.labeebpro.server.modals.MSG_MODAL;
import com.thakaa360.labeebpro.server.modals.SESSION_CODE_MODAL;
import com.thakaa360.labeebpro.server.modals.UPDATE_SESSION_MSG;
import com.thakaa360.labeebpro.utilities.Constants;
import com.thakaa360.labeebpro.utilities.Tools;

import java.io.IOException;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import eu.inmite.android.lib.validations.form.FormValidator;
import eu.inmite.android.lib.validations.form.annotations.NotEmpty;
import eu.inmite.android.lib.validations.form.callback.SimpleErrorPopupCallback;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Multipart;

public class Login extends AppCompatActivity implements View.OnClickListener{

   ImageView
                                company_selector,
                                agent_selector;
    @NotEmpty(messageId = R.string.validtion_empty,order = 1)
    EditText
                    username;
    @NotEmpty(messageId = R.string.validtion_empty,order = 1)
    EditText
                    password;
    Button
                login_button;
    String
            USER_NAME,
            PASSWORD;
    int
        user_role = 1;
    ProgressDialog
                            progressDialog;
    RequestBody
                        emailPart,
                        numberPart,
                        passwordPart;
    Context
                context;
    private SQLiteDatabase sqLiteDatabase;
    private UserData_Interface userData_interface;
    private Call<List<COMPANY_DATA_MODAL>> callCompany;
    private Call<List<AGENT_DATA_MODAL>> callAgent;

    private VideoView videoview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        createDatabase();

         videoview = (VideoView) findViewById(R.id.videoView);
        Uri uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.dubai);
        videoview.setVideoURI(uri);
        videoview.start();
        videoview.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.start();
            }
        });

        userData_interface = ApiClient.getApiClient().create(UserData_Interface.class);

        context = Login.this;
        progressDialog = new ProgressDialog(Login.this);
        progressDialog.setTitle("Login Please Wait");
        progressDialog.setCancelable(false);

        company_selector = findViewById(R.id.company_selector);
        agent_selector = findViewById(R.id.agent_selector);

        username = findViewById(R.id.username_field);
        password = findViewById(R.id.password_field);

        login_button = findViewById(R.id.login);


        company_selector.setOnClickListener(this);
        agent_selector.setOnClickListener(this);

        login_button.setOnClickListener(this);




    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.company_selector:
                user_role = 1;
                company_selector.setBackgroundResource(R.drawable.active_circle);
                agent_selector.setBackgroundResource(R.drawable.inactive_circle);
                break;
            case R.id.agent_selector:
                user_role = 2;
                company_selector.setBackgroundResource(R.drawable.inactive_circle);
                agent_selector.setBackgroundResource(R.drawable.active_circle);
                break;
            case R.id.login:
                long start = SystemClock.elapsedRealtime();
                final boolean isValid = FormValidator.validate(this, new SimpleErrorPopupCallback(this, true));
                long time = SystemClock.elapsedRealtime() - start;
                Log.d(getClass().getName(), "validation finished in [ms] " + time);

                if (isValid) {
                    progressDialog.show();
                    USER_NAME = username.getText().toString();
                    PASSWORD = password.getText().toString();

                    emailPart = RequestBody.create(MultipartBody.FORM, USER_NAME);
                    passwordPart = RequestBody.create(MultipartBody.FORM, PASSWORD);

                    LoginInterface loginInterface = ApiClient.getApiClient().create(LoginInterface.class);
                    Call<MSG_MODAL> call;
                    switch (user_role) {
                        case 1:
                            call = loginInterface.company_loginWithEmail(
                                    emailPart,
                                    passwordPart
                            );
                            call.enqueue(new Callback<MSG_MODAL>() {
                                @Override
                                public void onResponse(Call<MSG_MODAL> call, final Response<MSG_MODAL> response) {
                                    progressDialog.dismiss();
                                    if (response.isSuccessful()) {
                                        if (response.body().getStatus() == Constants.SUCCESSFUL_STATUS) {
                                            Get_Status get_status = ApiClient.getApiClient().create(Get_Status.class);
                                            Call<SESSION_CODE_MODAL> sessionCodeModalCall = get_status.getStatusByEmail(
                                                     emailPart,
                                                    RequestBody.create(MultipartBody.FORM,"3")
                                            );

                                            sessionCodeModalCall.enqueue(new Callback<SESSION_CODE_MODAL>() {
                                                @Override
                                                public void onResponse(Call<SESSION_CODE_MODAL> call, Response<SESSION_CODE_MODAL> response) {
                                                    if (response.isSuccessful()){
                                                        if (response.body().getSession_code()==1){
                                                            callCompany = userData_interface.getCompanyDataByEmail(emailPart);
                                                            callCompany.enqueue(new Callback<List<COMPANY_DATA_MODAL>>() {
                                                                @Override
                                                                public void onResponse(Call<List<COMPANY_DATA_MODAL>> call, Response<List<COMPANY_DATA_MODAL>> response) {
                                                                    if (response.isSuccessful() && response.body().size() != 0) {

                                                                        ContentValues contentValues = new ContentValues();
                                                                        contentValues.put(Constants.DB_USER_CREDIENTIALS, response.body().get(0).getEmail().toString());
                                                                        contentValues.put(Constants.DB_USER_ID, response.body().get(0).getCompany_id());
                                                                        contentValues.put(Constants.DB_ROLE, 2);
                                                                        contentValues.put(Constants.DB_FCM_TOKEN, "aaa");
                                                                        contentValues.put(Constants.DB_SESSION_STAT,1);

                                                                        sqLiteDatabase.update(Constants.DB_TABLE, contentValues, "id =" + 1, null);
                                                                        sqLiteDatabase.close();
                                                                        FCM_TOKEN_INTERFACE fcm_token_interface = ApiClient.getApiClient().create(FCM_TOKEN_INTERFACE.class);
                                                                        Call<MSG_MODAL> call1 = null;
                                                                        call1 = fcm_token_interface.updateToken(
                                                                                RequestBody.create(MultipartBody.FORM,response.body().get(0).getEmail()),
                                                                                RequestBody.create(MultipartBody.FORM,"3"),
                                                                                RequestBody.create(MultipartBody.FORM, FirebaseInstanceId.getInstance().getToken())
                                                                        );
                                                                        call1.enqueue(new Callback<MSG_MODAL>() {
                                                                            @Override
                                                                            public void onResponse(Call<MSG_MODAL> call, Response<MSG_MODAL> response) {
                                                                                if (response.isSuccessful()){

                                                                                }else{
                                                                                    Toast.makeText(context,response.message(),Toast.LENGTH_LONG).show();
                                                                                }
                                                                            }

                                                                            @Override
                                                                            public void onFailure(Call<MSG_MODAL> call, Throwable t) {
                                                                                Toast.makeText(context,t.getMessage(),Toast.LENGTH_LONG).show();
                                                                            }
                                                                        });

                                                                        Update_SessionCode update_sessionCode = ApiClient.getApiClient().create(Update_SessionCode.class);
                                                                        Call<UPDATE_SESSION_MSG> updateSessionMsgCall = update_sessionCode.updateSessionCodeByEmail(
                                                                                emailPart,
                                                                                RequestBody.create(MultipartBody.FORM,"0"),
                                                                                RequestBody.create(MultipartBody.FORM,"3")
                                                                        );
                                                                        updateSessionMsgCall.enqueue(new Callback<UPDATE_SESSION_MSG>() {
                                                                            @Override
                                                                            public void onResponse(Call<UPDATE_SESSION_MSG> call, Response<UPDATE_SESSION_MSG> response) {
                                                                                if (response.isSuccessful()){
                                                                                    Snackbar.make(findViewById(android.R.id.content),"Welcome ",Snackbar.LENGTH_LONG).show();
                                                                                    startActivity(new Intent(context, CompanyDashboard.class));
                                                                                    finishAffinity();
                                                                                    overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                                                                                }else{
                                                                                    Toast.makeText(context,response.message(),Toast.LENGTH_LONG).show();
                                                                                }
                                                                            }

                                                                            @Override
                                                                            public void onFailure(Call<UPDATE_SESSION_MSG> call, Throwable t) {
                                                                                Toast.makeText(context,t.getMessage(),Toast.LENGTH_LONG).show();
                                                                            }
                                                                        });

                                                                    } else {
                                                                        Toast.makeText(context, response.message(), Toast.LENGTH_LONG).show();
                                                                    }
                                                                }

                                                                @Override
                                                                public void onFailure(Call<List<COMPANY_DATA_MODAL>> call, Throwable t) {
                                                                    Toast.makeText(context, t.getMessage(), Toast.LENGTH_LONG).show();
                                                                }
                                                            });
                                                        }
                                                        else if(response.body().getSession_code() == 0){
                                                            Toast.makeText(context,"Sorry You are Logged to Another Device",Toast.LENGTH_LONG).show();
                                                        }
                                                    }else{
                                                        Toast.makeText(context,response.message(),Toast.LENGTH_LONG).show();
                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call<SESSION_CODE_MODAL> call, Throwable t) {
                                                        Toast.makeText(context,t.getMessage(),Toast.LENGTH_LONG).show();
                                                }
                                            });



                                        }

                                    } else {
                                        Toast.makeText(context, response.message(), Toast.LENGTH_LONG).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<MSG_MODAL> call, Throwable t) {
                                    progressDialog.dismiss();
                                    Toast.makeText(context, t.getMessage(), Toast.LENGTH_LONG).show();

                                }
                            });
                            break;
                        case 2:
                            call = loginInterface.agent_loginWithEmail(
                                    emailPart,
                                    passwordPart
                            );
                            call.enqueue(new Callback<MSG_MODAL>() {
                                @Override
                                public void onResponse(Call<MSG_MODAL> call, Response<MSG_MODAL> response) {
                                    progressDialog.dismiss();
                                    if (response.isSuccessful()) {
                                        if (response.body().getStatus() == Constants.SUCCESSFUL_STATUS) {
                                            Get_Status get_status = ApiClient.getApiClient().create(Get_Status.class);
                                            Call<SESSION_CODE_MODAL> sessionCodeModalCall = get_status.getStatusByEmail(
                                                    emailPart,
                                                    RequestBody.create(MultipartBody.FORM,"4")
                                            );
                                            sessionCodeModalCall.enqueue(new Callback<SESSION_CODE_MODAL>() {
                                                @Override
                                                public void onResponse(Call<SESSION_CODE_MODAL> call, Response<SESSION_CODE_MODAL> response) {
                                                    if (response.isSuccessful()){
                                                        if (response.body().getSession_code() == 1){
                                                            callAgent = userData_interface.getAgentByEmail(emailPart);
                                                            callAgent.enqueue(new Callback<List<AGENT_DATA_MODAL>>() {
                                                                @Override
                                                                public void onResponse(Call<List<AGENT_DATA_MODAL>> call, Response<List<AGENT_DATA_MODAL>> response) {
                                                                    if (response.isSuccessful() && response.body().size() != 0) {
                                                                        ContentValues contentValues = new ContentValues();
                                                                        contentValues.put(Constants.DB_USER_CREDIENTIALS, response.body().get(0).getEmail().toString());
                                                                        contentValues.put(Constants.DB_USER_ID, response.body().get(0).getAgent_id());
                                                                        contentValues.put(Constants.DB_ROLE, 3);
                                                                        contentValues.put(Constants.DB_FCM_TOKEN, "aaa");
                                                                        contentValues.put(Constants.DB_SESSION_STAT,1);
                                                                        sqLiteDatabase.update(Constants.DB_TABLE, contentValues, "id =" + 1, null);
                                                                        sqLiteDatabase.close();

                                                                        FCM_TOKEN_INTERFACE fcm_token_interface = ApiClient.getApiClient().create(FCM_TOKEN_INTERFACE.class);
                                                                        Call<MSG_MODAL> call1 = null;
                                                                        call1 = fcm_token_interface.updateToken(
                                                                                RequestBody.create(MultipartBody.FORM,response.body().get(0).getEmail()),
                                                                                RequestBody.create(MultipartBody.FORM,"4"),
                                                                                RequestBody.create(MultipartBody.FORM, FirebaseInstanceId.getInstance().getToken())
                                                                        );
                                                                        call1.enqueue(new Callback<MSG_MODAL>() {
                                                                            @Override
                                                                            public void onResponse(Call<MSG_MODAL> call, Response<MSG_MODAL> response) {
                                                                                if (response.isSuccessful()){

                                                                                }else{
                                                                                    Toast.makeText(context,response.message(),Toast.LENGTH_LONG).show();
                                                                                }
                                                                            }

                                                                            @Override
                                                                            public void onFailure(Call<MSG_MODAL> call, Throwable t) {
                                                                                Toast.makeText(context,t.getMessage(),Toast.LENGTH_LONG).show();
                                                                            }
                                                                        });

                                                                        Update_SessionCode update_sessionCode = ApiClient.getApiClient().create(Update_SessionCode.class);
                                                                        Call<UPDATE_SESSION_MSG> updateSessionMsgCall = update_sessionCode.updateSessionCodeByEmail(
                                                                                emailPart,
                                                                                RequestBody.create(MultipartBody.FORM,"0"),
                                                                                RequestBody.create(MultipartBody.FORM,"4")
                                                                        );
                                                                        updateSessionMsgCall.enqueue(new Callback<UPDATE_SESSION_MSG>() {
                                                                            @Override
                                                                            public void onResponse(Call<UPDATE_SESSION_MSG> call, Response<UPDATE_SESSION_MSG> response) {
                                                                                if (response.isSuccessful()){
                                                                                    Snackbar.make(findViewById(android.R.id.content),"Welcome ",Snackbar.LENGTH_LONG).show();
                                                                                    startActivity(new Intent(context, AgentDashboard.class));
                                                                                    finishAffinity();
                                                                                    overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                                                                                }
                                                                                else{
                                                                                    Toast.makeText(context,response.message(),Toast.LENGTH_LONG).show();
                                                                                }
                                                                            }

                                                                            @Override
                                                                            public void onFailure(Call<UPDATE_SESSION_MSG> call, Throwable t) {
                                                                                Toast.makeText(context,t.getMessage(),Toast.LENGTH_LONG).show();
                                                                            }
                                                                        });


                                                                    } else {
                                                                        Toast.makeText(context, response.message(), Toast.LENGTH_LONG).show();
                                                                    }
                                                                }

                                                                @Override
                                                                public void onFailure(Call<List<AGENT_DATA_MODAL>> call, Throwable t) {
                                                                    Toast.makeText(context, t.getMessage(), Toast.LENGTH_LONG).show();
                                                                }
                                                            });
                                                        }else if(response.body().getSession_code() == 0){
                                                            Toast.makeText(context,"Sorry You are Logged to Another Device",Toast.LENGTH_LONG).show();
                                                        }
                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call<SESSION_CODE_MODAL> call, Throwable t) {
                                                    Toast.makeText(context,t.getMessage(),Toast.LENGTH_LONG).show();
                                                }
                                            });



                                        }
                                    } else {
                                        Toast.makeText(context, response.message(), Toast.LENGTH_LONG).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<MSG_MODAL> call, Throwable t) {
                                    progressDialog.dismiss();
                                    Toast.makeText(context, t.getMessage(), Toast.LENGTH_LONG).show();

                                }
                            });
                            break;

                    }
                }
                break;
        }


    }

    private void createDatabase(){
        sqLiteDatabase = openOrCreateDatabase(Constants.DB_NAME,MODE_PRIVATE,null);

        sqLiteDatabase.execSQL(Constants.createTable);
        String insert_USER = "INSERT INTO "+Constants.DB_TABLE+"" +
                "(" +
                Constants.DB_USER_CREDIENTIALS+"," +
                Constants.DB_USER_ID+"," +
                Constants.DB_ROLE+"," +
                Constants.DB_FCM_TOKEN+"," +
                Constants.DB_SESSION_STAT+") " +
                "VALUES('" +
                "labeeb_pro'," +
                "0," +
                "0," +
                "'aaa'," +
                0+")";
        sqLiteDatabase.execSQL(insert_USER);
    }



    @Override
    protected void onResume() {
        super.onResume();
        videoview.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        videoview.pause();
    }

    @Override
    protected void onPause() {
        super.onPause();
        videoview.pause();
    }
}
