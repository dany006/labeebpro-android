package com.thakaa360.labeebpro.activities;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.Bundler;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.SubscriptionEventListener;
import com.thakaa360.labeebpro.R;
import com.thakaa360.labeebpro.Tracker.LatLngInterpolator;
import com.thakaa360.labeebpro.Tracker.MarkerAnimation;
import com.thakaa360.labeebpro.fragments.FragmentViewALLAgentsAsList;
import com.thakaa360.labeebpro.fragments.FragmentViewALLAgentsonMap;
import com.thakaa360.labeebpro.fragments.FragmentViewAgentsAsList;
import com.thakaa360.labeebpro.fragments.FragmentViewAgentsonMap;
import com.thakaa360.labeebpro.modals.PUSHER_MODAL;
import com.thakaa360.labeebpro.server.client.ApiClient;
import com.thakaa360.labeebpro.server.interfaces.GetAllAgents;
import com.thakaa360.labeebpro.server.modals.ALL_AGENTS_MODAL;
import com.thakaa360.labeebpro.utilities.Constants;
import com.thakaa360.labeebpro.utilities.LayoutUtility;
import com.thakaa360.labeebpro.utilities.Major;
import com.thakaa360.labeebpro.utilities.Tools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TrackAllAgents extends Major {
    View view;

    private GoogleMap mMap;
    Toolbar toolbar;
    SQLiteDatabase sqLiteDatabase;
    int company_id;
    static Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = TrackAllAgents.this;
        view = LayoutUtility.getLayout(context,R.layout.activity_track_all_agents);

        mDrawer.addView(view, 0);
        navigationView.setCheckedItem(R.id.track_item);

        toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawer.openDrawer(GravityCompat.START);
            }
        });

        sqLiteDatabase = context.openOrCreateDatabase(Constants.DB_NAME,Context.MODE_PRIVATE,null);
        Cursor c = sqLiteDatabase.rawQuery(Constants.GET_PROFILE,null);
        c.moveToFirst();
        company_id =  c.getInt(c.getColumnIndex(Constants.DB_USER_ID));

        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
                getSupportFragmentManager(), FragmentPagerItems.with(this)
                .add("Map", FragmentViewALLAgentsonMap.class)
                .add("List", FragmentViewALLAgentsAsList.class)
                .create());

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(adapter);

        SmartTabLayout viewPagerTab = (SmartTabLayout) findViewById(R.id.viewpagertab);
        viewPagerTab.setViewPager(viewPager);


    }


}
